# Project Description 
Dreams is a web communication tool for managing teams similar to Microsoft Teams. This application was created as a simple demonstration of basic features of a messaging application and can only run on a web browser. This application utilises FLASK (REST) API's to create and run a web server with Python. The backend of this project had to be constructed from scratch while the frontend was provided. The backend/src folder contains implementation of the application as well as a server file that runs the server using FLASK. 

- This project uses python and FLASK API's to create a workable web application. It has also taken into account design principles and provides testing for all the implementation. 


# Instructions to Run Program: 
NOTE: May or may not work on versions after Python 3.7


1. Clone the gitlab repository to a location locally 
2. Open a terminal or cmd window (depending on Mac or Windows) and change the current directory to the 'backend' folder
3. Once in the 'backend' folder, run the command...'python3.7 -m src.server'






<img src="step1.png">





4. Once the backend server is run in the terminal window, open a new terminal/cmd window alongside the old one and change directory to the 'frontend' directory. 
5. Once in the frontend direcotry, run the command...'python3.7 frontend.py 8080'





<img src="step2.png">







6. This should run the server and display a url in the current terminal window. Copy this url, paste into a web browser and run. 

7. The page should look like below. Then, follow the instructions given on the dreams app website and create an account to use the application. You're all set to use the application!








<img src="step3.png">




# Application Features 

> Regsitering a new account 
<img src="Dreams_-_register.png">












>  Creating a new channel 
<img src="create_channel.png">
























>  Inviting other users to a channel
<img src="channel.png">







>  Sending a message to other users in a channel 





<img src="DM.png">


