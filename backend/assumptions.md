Our Assumptions
Iteration 1:
- A valid email must have a singular top-level domain (i.e. .com not .com.au)
- The auth_user_id will start from 0 and go up in increasing order as more users are added (no negative auth_user_ids)
- The channel_id will start from 0 and go up in increasing order as more channels are created (no negative channel_ids)
- auth_user_id will be invalid if it is a data type other than an integer
- For the channels_create_v1 function, the is_public paramater takes in a value of True when the channel is public and False when it is private
- The auth_user_id value is a unique integer 
- The channel_id value is a unique integer 
- The auth_user_id value can be found as the "u_id" value in data.py
- Any member of an existing channel can invite someone with a valid u_id to the specific channel they are in
- A user that is a member of a channel can invite a non-existing member to the channel whether or not the is_public paramater of the channel is set to True or False
- A user which is an owner of a channel is part of both members_owner and members_all lists
- There is no limit on how many users, channels or messages can be created and as such each respective id has no limit
- When there are no messages, passing start index of 0 into channel_messages does not create InputError

Iteration 2:
- If the u_id in channel/addowner is not already in the channel, then adds them to the channel as a member and an owner
- Users can be invited multiple times (dm/invite, channel/invite)
- The dm_name includes the handle of the owner and all the members
- A dm can have just the owner