import pytest
from src.channels import channels_create_v2
from src.channel import channel_messages_v2, channel_invite_v2, channel_details_v2
from src.json_edit import access_json, clear_json
from src.error import InputError, AccessError
import src.auth
from src.channel import channel_messages_v2, channel_join_v2, channel_leave_v1, channel_addowner_v1, channel_removeowner_v1
from src.error import InputError, AccessError
from src.other import clear_v2
from src.auth import auth_register_v2
from src.json_edit import access_json
from src.message import message_send_v2
from datetime import datetime, timezone, timedelta
from src import config


@pytest.fixture
def clear_users():
    clear_json()

@pytest.fixture
def clear():
    clear_v2()

@pytest.fixture
def user(clear):
    return auth_register_v2("example@example.com", "password", "John", "Doe")

@pytest.fixture
def channel(user):
    return channels_create_v2(user["token"], "channel1", True)

#################################### LAWRENCE LEUNG ##############################
# Basic fixture to clear the users dictionary

def test_channel_invite(clear_users):
    owner_user = src.auth.auth_register_v2("channelowner@gmail.com", "PASSWORD", "Channel", "Owner")
    new_member = src.auth.auth_register_v2("new@member.com", "123456", "New", "Member")

    channel1 = channels_create_v2(owner_user["token"], "Best_Channel", False)

    channel_invite_v2(owner_user["token"], channel1["channel_id"], new_member["auth_user_id"])
    channel1_details = channel_details_v2(owner_user["token"], channel1["channel_id"])
    user_found = False
    for user in channel1_details["all_members"]:
        if user["u_id"] == new_member["auth_user_id"]:
            user_found = True
    
    assert user_found == True

def test_channel_invite_invalid_channel(clear_users):
    owner_user = src.auth.auth_register_v2("channelowner@gmail.com", "PASSWORD", "Channel", "Owner")
    new_user = src.auth.auth_register_v2("channelmember@gmail.com", "PASSWORD", "Channel", "Member")
    channels_create_v2(owner_user["token"], "The_Channel", False)
    channels_create_v2(owner_user["token"], "The_Channel_New", False)

    with pytest.raises(InputError):
        channel_invite_v2(owner_user["token"], 3, new_user["auth_user_id"])

def test_channel_invite_invalid_user(clear_users):
    owner_user = src.auth.auth_register_v2("channelowner@gmail.com", "PASSWORD", "Channel", "Owner")
    channel0 = channels_create_v2(owner_user["token"], "The_Channel", False)
    channels_create_v2(owner_user["token"], "The_Channel_New", False)

    with pytest.raises(InputError):
        channel_invite_v2(owner_user["token"], channel0["channel_id"], 2)

def test_channel_invite_invalid_auth_user(clear_users):
    owner_user = src.auth.auth_register_v2("channelowner@gmail.com", "PASSWORD", "Channel", "Owner")
    not_auth_user = src.auth.auth_register_v2("notmember@gmail.com", "PASSWORD", "Not", "Member")
    new_user = src.auth.auth_register_v2("channelmember@gmail.com", "PASSWORD", "Channel", "Member")
    channel0 = channels_create_v2(owner_user["token"], "The_Channel", False)
    channels_create_v2(owner_user["token"], "The_Channel_New", False)

    with pytest.raises(AccessError):
        channel_invite_v2(not_auth_user["token"], channel0["channel_id"], new_user["auth_user_id"])

# DETAILS TESTS
# Test 1, Check that a person is added to an existing channel that is not public
def test_channel_details_v2(clear_users):
    # Returns the id from a registered user (assume it starts from 0)
    result0 = src.auth.auth_register_v2("john.smith@gmail.com", "password", "John", "Smith") 
    # Hard coded the dictionary from channels 0 
    token0 = result0["token"]
    # Returns the dictionary that contains the channel
    channel0 = channels_create_v2(token0, "hehexd", False)
    # Variable for the integer of the channel id
    channel0_id = channel0["channel_id"]

    assert channel_details_v2(token0, channel0_id) == {
        'name': 'hehexd',
        'is_public' : 'False',
        'owner_members': [
            {
                'u_id': result0["auth_user_id"],
                'name_first' : 'John',
                'name_last' : 'Smith',
                'email' : 'john.smith@gmail.com',
                'handle_str' : 'johnsmith',
                'profile_img_url': f'{config.url}static/default_pic.jpg'
            },
        ],
        'all_members': [
            {
                'u_id': result0["auth_user_id"],
                'name_first' : 'John',
                'name_last' : 'Smith',
                'email' : 'john.smith@gmail.com',
                'handle_str' : 'johnsmith',
                'profile_img_url': f'{config.url}static/default_pic.jpg'
            },
        ],
    }

# Test 2, If channel Id is not valid then raise input error
def test_channel_details_v2_exceptInput(clear_users):
    result0 = src.auth.auth_register_v2("david.so@gmail.com", "GeniusBrain", "David", "So") 

    # Channel ID is not a valid channel Raise inputError 
    with pytest.raises(InputError):
        assert channel_details_v2(result0["token"], 1) 


# Test 3, If authorised user is not a member of a channel with channel_id then access error 
def test_channel_details_v2_exceptAccess(clear_users):
    # Is not a memeber id 0
    result0 = src.auth.auth_register_v2("diana.Prince@hotmail.com", "invisibleJet", "Diana", "Prince")
    token0 = result0["token"]
    # Is a member id 1
    result1 = src.auth.auth_register_v2("The.Vision@hotmail.com", "wanda1", "The", "Vision")
    token1 = result1["token"]
    # Is a member id 2 
    result2 = src.auth.auth_register_v2("Ultron.Six@yahoo.com", "Vibranium", "Ultron", "Six")
    result2_id = result2["auth_user_id"]
    
    channel0 = channels_create_v2(token1, "ReleaseSynderCut", False)
    channel0_id = channel0["channel_id"]
    
    channel_invite_v2(token1, channel0_id, result2_id)

    with pytest.raises(AccessError):
        assert channel_details_v2(token0, channel0_id)

# Test 4, if the authorised user has the same name but different email, duplicate handles
def test_channel_details_v2_duplicate_handle(clear_users):
    # Register users
    # Created a channel (owner)
    result0 = src.auth.auth_register_v2("durriemuncher@gmail.com", "ROFLLMAO", "Ralph", "Bohner")
    token0 = result0["token"]

    channel0 = channels_create_v2(token0, "eetswalads", False)
    channel0_id = channel0["channel_id"]
    
    # Added member (member)
    result1 = src.auth.auth_register_v2("eshayadlay@hotmail.com", "JBLspeaker", "Ralph", "Bohner")
    result1_id = result1["auth_user_id"]

    channel_invite_v2(token0, channel0_id, result1_id)

    assert channel_details_v2(token0, channel0_id) == {
        'name': 'eetswalads',
        'is_public' : 'False',
        'owner_members': [
            {
                'u_id': result0["auth_user_id"],
                'name_first' : 'Ralph',
                'name_last' : 'Bohner',
                'email' : 'durriemuncher@gmail.com',
                'handle_str' : 'ralphbohner',
                'profile_img_url': f'{config.url}static/default_pic.jpg',
            },
        ],
        'all_members': [
            {
                'u_id' : result0["auth_user_id"],
                'name_first' : 'Ralph',
                'name_last' : 'Bohner',
                'email' : 'durriemuncher@gmail.com',
                'handle_str' : 'ralphbohner',
                'profile_img_url': f'{config.url}static/default_pic.jpg',
            },
            {
                'u_id' : result1["auth_user_id"],
                'name_first' : 'Ralph',
                'name_last' : 'Bohner',
                'email' : 'eshayadlay@hotmail.com',
                'handle_str' : 'ralphbohner0',
                'profile_img_url': f'{config.url}static/default_pic.jpg',
            },
            
        ],
    }

# ADDOWNER TESTS
# Test 1 Testing the token is valid
def test_channel_addowner_v1_not_valid_token(clear_users):
    owner_user1 = src.auth.auth_register_v2("nickfury123@hotmail.com", "shield", "Nick", "Fury")
    new_user = src.auth.auth_register_v2("zacksynder@hotmail.com", "justiceleague", "Zack", "Synder")
    channel0 = channels_create_v2(owner_user1["token"], "Hydra", False)

    with pytest.raises(AccessError):
        channel_addowner_v1(-6161610, channel0["channel_id"], new_user["auth_user_id"])

# Test 2 If the u_id is already an owner of the channel
def test_channel_addowner_v1_not_valid_u_id(clear_users):
    owner_user = src.auth.auth_register_v2("henrylau@gmail.com", "reallove", "Henry", "Lau")
    channel0 = channels_create_v2(owner_user["token"], "Strings", False)
    
    with pytest.raises(InputError):
        channel_addowner_v1(owner_user["token"], channel0["channel_id"], owner_user["auth_user_id"])

# Test 3 If the u_id is not a valid channel
def test_channel_addowner_v1_not_valid_channel_id(clear_users):
    owner_user = src.auth.auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    new_user = src.auth.auth_register_v2("zacksynder@hotmail.com", "justiceleague", "Zack", "Synder")

    with pytest.raises(InputError):
        channel_addowner_v1(owner_user["token"], 0, new_user["auth_user_id"])

# Test 4 If the auth user is not an owner of the channel access error 
def test_channel_addowner_v1_not_valid_owner(clear_users):
    owner_user = src.auth.auth_register_v2("kanyewest@hotmail.com", "kimKaye", "Kanye", "West")
    channel0 = channels_create_v2(owner_user["token"], "Sleep", False)
    not_owner_user = src.auth.auth_register_v2("suzybae@gmail.com", "Suzy1234", "Suzy", "Bae")
    random_user = src.auth.auth_register_v2("hectorDiaz@gmail.com", "lawnmower", "Hector", "Diaz")

    with pytest.raises(AccessError):
        channel_addowner_v1(not_owner_user["token"], channel0["channel_id"], random_user["auth_user_id"]) 

# Test 5 If auth user is not an owner of the **Dreams** access error 
def test_channel_addowner_v1_not_owner_of_dreams(clear_users):
    # Permission id 1 
    owner_user = src.auth.auth_register_v2("DarthVader@gmail.com", "padme23", "Darth", "Vader")
    # Permission id 2 
    member_user1 = src.auth.auth_register_v2("ObiwanKenobi@hotmail.com", "theduchess", "Obiwan", "Kenobi" )
    # Permission id 2
    member_user2 = src.auth.auth_register_v2("AhsokaTano@gmail.com", "wrongjedi", "Ahsoka", "Tano")
    channel0 = channels_create_v2(owner_user["token"], "CloneWars", False)

    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user1["auth_user_id"])
    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user2["auth_user_id"])
    
    with pytest.raises(AccessError):
        channel_addowner_v1(member_user1["token"], channel0["channel_id"], member_user2["auth_user_id"])

# Test 6 Testing adding a member that is not a owner
def test_channel_addowner_v1_member(clear_users):
    owner_user = src.auth.auth_register_v2("AnakinSkywalker@gmail.com", "padme23", "Anakin", "Skywalker")
    member_user = src.auth.auth_register_v2("ObiwanKenobi@hotmail.com", "theduchess", "Obiwan", "Kenobi")

    
    channel0 = channels_create_v2(owner_user["token"], "CloneWars", False)
    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user["auth_user_id"])

    channel_addowner_v1(owner_user["token"], channel0["channel_id"], member_user["auth_user_id"])
    channel_details = channel_details_v2(owner_user["token"], channel0["channel_id"])

    user_found = False
    for user in channel_details["owner_members"]:
        if user["u_id"] == member_user["auth_user_id"]:
            user_found = True
    
    assert user_found == True

# Test 7 Testing if the u_id is not apart of the channel add them to both owner_members and members_all lists
def test_channel_addowner_v1_not_member(clear_users):
    owner_user = src.auth.auth_register_v2("DarthSidious@gmail.com", "iamthesenate", "Sheev", "Palpatine")
    non_member = src.auth.auth_register_v2("DarthPlageius@hotmail.com", "Thetragedy", "Moon", "Face")

    channel0 = channels_create_v2(owner_user["token"], "Corusant", False)
    channel_addowner_v1(owner_user["token"], channel0["channel_id"], non_member["auth_user_id"])
    channel_details = channel_details_v2(owner_user["token"], channel0["channel_id"])

    user_found_owner = False
    user_found_member = False
    
    for user in channel_details["owner_members"]:
        if user["u_id"] == non_member["auth_user_id"]:
            user_found_owner = True

    for user in channel_details["all_members"]:
        if user["u_id"] == non_member["auth_user_id"]:
            user_found_member = True 

    user_found = False
    if user_found_member and user_found_owner:
        user_found = True

    assert user_found == True



# REMOVE OWNER TESTS
# Test 1 Testing the token is valid
def test_channel_removeowner_v1_not_valid_token(clear_users):
    owner_user1 = src.auth.auth_register_v2("nickfury123@hotmail.com", "shield", "Nick", "Fury")
    owner_user2 = src.auth.auth_register_v2("WinterSoldier@hotmail.com", "steverogers", "Winter", "Soldier")
    channel0 = channels_create_v2(owner_user1["token"], "Hydra", False)

    with pytest.raises(AccessError):
        channel_removeowner_v1(-123123123, channel0["channel_id"], owner_user2["auth_user_id"])

# Test 2 Test user with u_id is not an owner of the channel
def test_channel_removeowner_v1_not_and_owner(clear_users):
    owner_user1 = src.auth.auth_register_v2("nickfury123@hotmail.com", "shield", "Nick", "Fury")
    owner_user2 = src.auth.auth_register_v2("WinterSoldier@hotmail.com", "steverogers", "Winter", "Soldier")
    
    channel0 = channels_create_v2(owner_user1["token"], "Hydra", False)

    with pytest.raises(InputError):
        channel_removeowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])

# Test 3 Test if channel Id is valid
def test_channel_removeowner_v1_not_valid_channel_id(clear_users):
    owner_user1 = src.auth.auth_register_v2("mikehunt420@gmail.com", "eshay231", "Mike", "Hunt")
    owner_user2 = src.auth.auth_register_v2("candicesithers69@gmail.com", "CanIshithere", "Candice", "Sithers")
    channel0 = channels_create_v2(owner_user1["token"], "Adlay", False)
    channel_addowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])

    with pytest.raises(InputError):
        channel_removeowner_v1(owner_user1["token"], 123, owner_user2["auth_user_id"])
    
    with pytest.raises(InputError):
        channel_removeowner_v1(owner_user1["token"], -3, owner_user2["auth_user_id"])
    
    with pytest.raises(InputError):
        channel_removeowner_v1(owner_user1["token"], 5/7, owner_user2["auth_user_id"])
        

# Test 4 Test if the user is the only owner of the channel
def test_channel_removeowner_v1_only_owner_in_channel(clear_users):
    owner_user1 = src.auth.auth_register_v2("jesussandal@gmail.com", "godisGreat", "Jesus", "Christ")
    channel0 = channels_create_v2(owner_user1["token"], "Heaven", False)

    with pytest.raises(InputError):
        channel_removeowner_v1(owner_user1["token"], channel0["channel_id"], owner_user1["auth_user_id"])


# Test 5 If auth user is not an owner of the **Dreams** access error
def test_channel_removeowner_v1_not_owner_of_dreams(clear_users): 
    # Permission id 1 
    owner_user = src.auth.auth_register_v2("DarthVader@gmail.com", "padme23", "Darth", "Vader")
    # Permission id 2 
    member_user1 = src.auth.auth_register_v2("ObiwanKenobi@hotmail.com", "theduchess", "Obiwan", "Kenobi" )
    # Permission id 2
    member_user2 = src.auth.auth_register_v2("AhsokaTano@gmail.com", "wrongjedi", "Ahsoka", "Tano")
    channel0 = channels_create_v2(owner_user["token"], "CloneWars", False)

    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user1["auth_user_id"])
    channel_addowner_v1(owner_user["token"], channel0["channel_id"], member_user2["auth_user_id"])
    
    with pytest.raises(AccessError):
        channel_removeowner_v1(member_user1["token"], channel0["channel_id"], member_user2["auth_user_id"])

# Test 6 Test general functionality of removing an owner
def test_channel_removeowner_v1_removing_an_owner(clear_users):
    owner_user1 = src.auth.auth_register_v2("jesussandal@gmail.com", "godisGreat", "Jesus", "Christ")
    owner_user2 = src.auth.auth_register_v2("lucifermorning@gmail.com", "iamThedevil", "The", "Devil")
    owner_user3 = src.auth.auth_register_v2("NORMALhuman@gmail.com", "iatetheapple", "Adam", "Eve")

    channel0 = channels_create_v2(owner_user1["token"], "Earth", False)

    channel_addowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])
    channel_addowner_v1(owner_user2["token"], channel0["channel_id"], owner_user3["auth_user_id"])
    
    channel_removeowner_v1(owner_user2["token"], channel0["channel_id"], owner_user3["auth_user_id"])
    channel_details = channel_details_v2(owner_user1["token"], channel0["channel_id"])

    user_found = False
    for user in channel_details["owner_members"]:
        if user["u_id"] == owner_user3["auth_user_id"]:
            user_found = True

    assert user_found == False

# Test 7 Test that it removed the u_id from the owners list and not the members all list
def test_channel_removeowner_v1_check_u_id_is_not_removed_from_members_all(clear_users):
    owner_user1 = src.auth.auth_register_v2("IamIronman@gmail.com", "IronMan", "Tony", "Stark")
    owner_user2 = src.auth.auth_register_v2("thanopurple@gmail.com", "Grimace", "Thanos", "Stone")
    channel0 = channels_create_v2(owner_user1["token"], "Marvel", False)
    channel_addowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])
    
    channel_removeowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])
    channel_details = channel_details_v2(owner_user1["token"], channel0["channel_id"])
    print(channel_details)
    
    user_found = False
    
    for user in channel_details["all_members"]:
        if user["u_id"] == owner_user2["auth_user_id"]:        
            user_found = True

    assert user_found == True

# CHANNEL LEAVE
# Test 1 Test If the channel is not a valid channel_id
def test_channel_leave_v1_not_valid_channel_id(clear_users):
    owner_user1 = src.auth.auth_register_v2("deezNuts@gmail.com", "ligma7899", "Diaz", "Nut")
    
    
    with pytest.raises(InputError):
        assert channel_leave_v1(owner_user1["token"], 23212321)

# Test 2 Test if the token is valid
def test_channel_leave_v1_not_valid_token(clear_users):
    owner_user1 = src.auth.auth_register_v2("nickfury123@hotmail.com", "shield", "Nick", "Fury")
    
    channel0 = channels_create_v2(owner_user1["token"], "Hydra", False)

    with pytest.raises(AccessError):
        channel_leave_v1(3431231232131, channel0["channel_id"])

# Test 3 Test If a authorised user is not a member of the channel
def test_channel_leave_v1_not_auth_user_of_channel(clear_users):
    owner_user1 = src.auth.auth_register_v2("OptimusPrime@hotmail.com", "prime2654", "Orion", "Pax")
    owner_user2 = src.auth.auth_register_v2("megatron@hotmail.com", "Thefallen", "Megatronous", "prime")
    
    channel0 = channels_create_v2(owner_user1["token"], "IamThemachine", False)

    with pytest.raises(AccessError):
        assert channel_leave_v1(owner_user2["token"], channel0["channel_id"])

# Test 4 Test if a member in members_all leaves the channel is removed coorectly 
def test_channel_leave_v1_member_leaves_channel(clear_users):
    owner_user1 = src.auth.auth_register_v2("OptimusPrime@hotmail.com", "prime2654", "Orion", "Pax")
    member_user1 = src.auth.auth_register_v2("namjoohyuk@gmail.com", "Start-up", "Joo", "Hyuk")   

    channel0 = channels_create_v2(owner_user1["token"], "SamsanTek", False)
    
    channel_invite_v2(owner_user1["token"], channel0["channel_id"], member_user1["auth_user_id"])
    channel_leave_v1(member_user1["token"], channel0["channel_id"])

    channel_details = channel_details_v2(owner_user1["token"], channel0["channel_id"])
 
    user_found = False
    for user in channel_details["owner_members"]:
        if user["u_id"] == member_user1["auth_user_id"]:
            user_found = True
    # if the user is in the "members_all then user_found is true and fails test"
    assert user_found == False

# Test 5 Test when a user in owner_members and members_all leave the channel is removed correctly 
def test_channel_leave_v1_owner_leaves_channel(clear_users):
    owner_user1 = src.auth.auth_register_v2("namdosan@hotmail.com", "dalmi232", "DoSan", "Nam")
    owner_user2 = src.auth.auth_register_v2("Seodalmi@hotmail.com", "dosan2323", "DalMi", "Seo")

    channel0 = channels_create_v2(owner_user1["token"], "SamsanTek", False)

    channel_addowner_v1(owner_user1["token"], channel0["channel_id"], owner_user2["auth_user_id"])
    channel_leave_v1(owner_user2["token"], channel0["channel_id"])
    channel_details = channel_details_v2(owner_user1["token"], channel0["channel_id"])

    owner_user_found = False
    member_user_found = False
    # none of these statements should return true if it does then it means the user is still in
    # the channel
    for user in channel_details["owner_members"]:
        if user["u_id"] == owner_user2["auth_user_id"]:
            owner_user_found = True

    for user in channel_details["all_members"]:
        if user["u_id"] == owner_user2["auth_user_id"]:
            member_user_found = True

    assert not owner_user_found and not member_user_found
    
# Test 5 Test when a owner wants to leave their channel being the only user in the channel?






################################## LAWSON OATES ################################################

# channel_join tests

# check channel id is a valid channel
def test_channel_join_channel_id(clear, user, channel):

    with pytest.raises(InputError):
        assert channel_join_v2(user["token"], "1A")

    with pytest.raises(InputError):
        assert channel_join_v2(user["token"], "/A")

    with pytest.raises(InputError):
        assert channel_join_v2(user["token"], "2.4")

    with pytest.raises(InputError):
        assert channel_join_v2(user["token"], -1)

    with pytest.raises(InputError):
        assert channel_join_v2(user["token"], 2.7)

    assert channel_join_v2(user["token"], channel["channel_id"]) == {}

# check user id is a valid user
def test_channel_join_user_id(clear, user, channel):

    with pytest.raises(AccessError):
        assert channel_join_v2("1A", channel["channel_id"])

    with pytest.raises(AccessError):
        assert channel_join_v2("/A", channel["channel_id"])

    with pytest.raises(AccessError):
        assert channel_join_v2("2.4", channel["channel_id"])

    with pytest.raises(AccessError):
        assert channel_join_v2(-1, channel["channel_id"])

    with pytest.raises(AccessError):
        assert channel_join_v2(2.7, channel["channel_id"])

    assert channel_join_v2(user["token"], channel["channel_id"]) == {}

# check user has perms to join channel
def test_channel_join_perms(clear, user):

    # check for error when trying to join a private channel
    channel = channels_create_v2(user["token"], "channel1", False)

    # register user that is not global owner
    user2 = auth_register_v2("example2@example.com", "password", "Jane", "Doe")

    with pytest.raises(AccessError):
        channel_join_v2(user2["token"], channel["channel_id"])

# test return type
def test_channel_join_return(clear, user, channel):
    assert channel_join_v2(user["token"], channel["channel_id"]) == {}

# channel_messages tests

# test start is greater than total number of messages
def test_channel_messages_start_message(clear, user, channel):
    messages = access_json("messages")
    number_messages = len(messages)

    with pytest.raises(InputError):
        assert channel_messages_v2(user["token"], channel["channel_id"], number_messages + 1)

# test return type
def test_channel_messages_return(clear, user, channel):
    # try when channel is created by user
    result = channel_messages_v2(user["token"], channel["channel_id"], 0)

    assert type(result) is dict

# user is not a member of the channel
def test_channel_messages_auth_user_perms(clear, user):
    # try to access channel created by another user
    channel = channels_create_v2(user["token"], "channel1", False)

    with pytest.raises(AccessError):
        assert channel_messages_v2(2, channel["channel_id"], 0)

# test channel id is valid
def test_channel_messages_channel_id(clear, user):

    with pytest.raises(InputError):
        assert channel_messages_v2(user["token"], 0, 0)

# test no messages are returned when messages is empty
def test_channel_messages_empty_list(clear, user, channel):

    assert channel_messages_v2(user["token"], channel["channel_id"], 0) == {
        "messages": [],
        "start": 0,
        "end": -1
    }

# test message is returned when single message is sent to channel
def test_channel_messages_single_message(clear, user, channel):
    message = 'welcome to comp'
    message_id = message_send_v2(user['token'], channel['channel_id'], message)["message_id"]

    return_result = channel_messages_v2(user['token'], channel['channel_id'], 0)

    return_expected = {
        "messages": [{
            "message_id": message_id,
            "u_id": user['auth_user_id'],
            "message": message,
            "time_created": 0,
            "reacts": [{"react_id": 1, "u_ids": [], "is_this_user_reacted": False}],
            "is_pinned": False
        }],
        "start": 0,
        "end": -1
    }

    for msg in return_result["messages"]:
        msg["time_created"] = 0

    assert return_expected == return_result

def test_channel_messages_end_many_messages(clear, user, channel):
    message = 'welcome to comp'

    x = 0
    while x < 51:
        message_send_v2(user['token'], channel['channel_id'], message)["message_id"]
        x += 1

    assert channel_messages_v2(user['token'], channel['channel_id'], 0)["end"] == 50