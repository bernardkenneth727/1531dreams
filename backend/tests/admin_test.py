from src.admin import admin_user_permission_change_v1, admin_user_remove_v1
from src.auth import auth_register_v2
from src.other import clear_v2, search_v2
from src.dm import dm_create_v1, message_send_dm_v1
import pytest

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2()

@pytest.fixture
def SetPermission(Clearing):
    """
    creates a valid permission
    """
    permission = 1
    return permission

#tests for admin_user_permission_change_v1:
#takes in token, u_id, permission_id
#outputs nothing but sets permissions to new permission - MEMBER or OWNER

def test_admin_user_permission_change_v1_token_check(SetPermission):
    output = auth_register_v2("jim.smith@gmail.com", "password", "Jimmy", "Smith")
    u_id = output["auth_user_id"]
    permission_id = SetPermission
    with pytest.raises(Exception):
        admin_user_permission_change_v1(-1, u_id, permission_id)

def test_admin_user_permission_change_valid(SetPermission):
    output = auth_register_v2("jim.smith@gmail.com", "password", "Jimmy", "Smith")
    u_id = output["auth_user_id"]
    permission_id = 2
    
    returns = admin_user_permission_change_v1(output['token'], u_id, permission_id)
    assert returns == {}

def test_admin_user_permission_change_v1_invalid_u_id(SetPermission):
    output = auth_register_v2("jim.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output["token"]
    permission = SetPermission
   #assert  is InputError
    with pytest.raises(Exception):
        admin_user_permission_change_v1(token, -1, permission)

def test_admin_user_permission_change_v1_invalid_permission_id(Clearing):
    output = auth_register_v2("bob.smith@gmail.com", "password", "Jimmy", "Smith")
    u_id = output["auth_user_id"]
    token = output["token"]
    with pytest.raises(Exception):
        admin_user_permission_change_v1(token, u_id, -1)

def test_admin_user_permission_change_v1_user_not_owner():
    output = auth_register_v2("jonno.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output["token"]
    u_id = output["auth_user_id"]
    with pytest.raises(Exception):
        admin_user_permission_change_v1(token, u_id, 2)

#tests for admin_user_remove_v1:
#takes in token, u_id
#outputs nothing but removes user, sets contents of messages to 'Removed user', replaces name with 'Removed user'

def test_admin_user_remove_v1_token_check(Clearing):
    output = auth_register_v2("jim.smith@gmail.com", "password", "Jimmy", "Smith")
    u_id = output["auth_user_id"]
    with pytest.raises(Exception):
        admin_user_remove_v1("invalid token", u_id)

def test_admin_user_remove_v1_valid(Clearing):
    output2 = auth_register_v2("jangyo.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output2["token"]
    output3 = auth_register_v2("tango.smithy@gmail.com", "password", "Jimmy", "Smith")
    admin_user_permission_change_v1(output2['token'], output3["auth_user_id"], 1)
    
    returns = admin_user_remove_v1(token, output3['auth_user_id'])
    assert returns == {}

def test_admin_user_remove_v1_invalid_u_id(Clearing):
    output2 = auth_register_v2("jangyo.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output2["token"]
    output3 = auth_register_v2("tango.smithy@gmail.com", "password", "Jimmy", "Smith")
    admin_user_permission_change_v1(output2['token'], output3["auth_user_id"], 1)

    with pytest.raises(Exception):
        admin_user_remove_v1(token, -1)

def test_admin_user_remove_v1_user_is_only_owner(Clearing):
    output = auth_register_v2("jack.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output["token"]
    u_id = output["auth_user_id"]
    #making a user who is the only owner
    with pytest.raises(Exception):
        admin_user_remove_v1(token, u_id)

def test_admin_user_remove_v1_user_not_owner(Clearing):
    #create user that is not owner
    #call fn and see if access error if user is not owner
    auth_register_v2("jeremiah.smith@gmail.com", "password", "Jimmy", "Smith")
    output = auth_register_v2("baxter.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output["token"]

    u_id = output["auth_user_id"]
    with pytest.raises(Exception):
        admin_user_remove_v1(token, u_id)
    clear_v2()

def test_admin_user_remove_v1_message_sent(Clearing):
    output2 = auth_register_v2("jangyo.smith@gmail.com", "password", "Jimmy", "Smith")
    token = output2["token"]
    output3 = auth_register_v2("tango.smithy@gmail.com", "password", "Jimmy", "Smith")
    admin_user_permission_change_v1(output2['token'], output3["auth_user_id"], 1)

    message = 'Removed user'
    dm_id = dm_create_v1(output2['token'], [output3['auth_user_id']])['dm_id']
    message_id = message_send_dm_v1(output3['token'], dm_id, message)['message_id']
    
    admin_user_remove_v1(token, output3['auth_user_id'])

    result = search_v2(output3['token'], message)['messages']
    for msg in result:
        msg['time_created'] = 0

    result_expected = [{
        'message_id': message_id,
        'u_id': output3['auth_user_id'],
        'message': 'Removed user',
        'reacts': [{'react_id': 1, 'u_ids': [], 'is_this_user_reacted': False}],
        'time_created': 0,
        'is_pinned': False
    }]

    assert result == result_expected