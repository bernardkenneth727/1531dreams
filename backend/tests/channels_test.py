import pytest
from src.channels import channels_create_v2, channels_list_v2, channels_listall_v2
from src.auth import auth_register_v2
from src.other import clear_v2
import src.user
from src.error import AccessError, InputError
from src.json_edit import access_json, clear_json, add_to_json
from src.secret import token_encode, token_decode
import uuid

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2()
    clear_json()

@pytest.fixture
def token_setup(Clearing):
    """
    creates a valid token via the auth_register_v2 function to be used for channels create
    also clears any ids that have been previously created
    """
    identifiers = auth_register_v2("john.smith@gmail.com", "password", "Jimmy", "Smith")
    return identifiers

@pytest.fixture
def SetName(Clearing):
    """
    creates a valid name to be used for testing clear via create channels
    and clears previous names
    """
    name = "Cacti"
    return name

@pytest.fixture
def Set_is_public(Clearing):
    """
    creates a valid boolean for the is_public parameter for channels create
    and clears previous booleans created
    """
    is_public = True
    return is_public

@pytest.fixture
def RegisterUser():

    return auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")


@pytest.fixture 
def RegisterUser2():

    return auth_register_v2("jack.swoosh@gmail.com", "password2", "Jack", "Swoosh")

@pytest.fixture
def channelsCreate(RegisterUser):

    return channels_create_v2(RegisterUser['token'], 'Teams1', 'True')

@pytest.fixture
def channelsCreate2(RegisterUser2):
    
    return channels_create_v2(RegisterUser2['token'], 'Teams2', 'True')

@pytest.fixture
def channelsCreate3(RegisterUser):
    
    return channels_create_v2(RegisterUser['token'], 'Teams4', 'True')

@pytest.fixture
def channelsCreatePvt(RegisterUser):
    
    return channels_create_v2(RegisterUser['token'], 'Teams3', 'False')


##channels_create_v2 tests below 

def test_channels_create_v2_invalid_token(Set_is_public, token_setup):

    with pytest.raises(Exception):
        channels_create_v2(-1, "validname", Set_is_public)


def test_channels_create_v2LargeName(token_setup, Set_is_public):
    """tests if exception is thrown with names > 20 characters 
    """    
    with pytest.raises(Exception):
        channels_create_v2(token_setup['token'], "anamethatiswayyyyyyyyyyyyyyyyyyytoooolong", Set_is_public)       
      

def test_channels_create_v2ReturnType(token_setup, SetName, Set_is_public):
    """
    Tests that the function returns a dictionary
    """
    is_dictionary = channels_create_v2(token_setup['token'], SetName, Set_is_public)
    assert type(is_dictionary) is dict



def test__channels_create_v2Check_Nonexistant_Token(SetName, Set_is_public):
    """
    checks that a non existent token - throws AccessError
    """
    with pytest.raises(Exception):
        channels_create_v2(3, SetName, Set_is_public)

def test__channels_create_v2CheckIfIdRepeated(token_setup, SetName, Set_is_public):
    """
    testing if channel_create repeats channel id (channel id must be unique), if it does this fails the test
    """
    id1 = channels_create_v2(token_setup['token'], SetName, Set_is_public)
    id2 = channels_create_v2(token_setup['token'], SetName, Set_is_public)

    assert id1 != id2
    

def test_channels_create_v2CheckIdIsInt(token_setup, SetName, Set_is_public):
    
    #testing if channel id which is returned by channels create is an integer 
     
    channel_id = channels_create_v2(token_setup['token'], SetName, Set_is_public)
    assert type(channel_id["channel_id"]) is int 


#channels_list_v2 tests below 

#testing channels_list with invalid token - should return AccessError
def test_channels_list_invalidToken(Clearing, RegisterUser, channelsCreate):
    invalidToken = RegisterUser["token"] + "invalid" #invalid auth_user_id

    with pytest.raises(AccessError):
        assert channels_list_v2(invalidToken)

#testing channels_list with user registered but no channels created - should return empty list 
def test_channels_list_nochannels(Clearing, RegisterUser):
    assert channels_list_v2(RegisterUser["token"]) == {"channels": []}

#testing channels_list with only 1 user registered and 1 public channel created - should return 1 channel
def test_channels_list_channel_publicCreate(Clearing, RegisterUser, channelsCreate):
    assert channels_list_v2(RegisterUser["token"]) == {"channels": [
        {
            "channel_id": channelsCreate["channel_id"],
            "name": "Teams1",
        },
    ]}

#testng channels_list with only 1 user registered and 1 private channel created - should return 1 channel 
def test_channels_list_channel_privateCreate(Clearing, RegisterUser, channelsCreatePvt):
    assert channels_list_v2(RegisterUser["token"]) == {"channels": [
        {
            "channel_id": channelsCreatePvt["channel_id"],
            "name": "Teams3"
        }
    ]}

#tests channels_list after single user creates multiple public channels  - should return multiple channels 
def test_channels_list_multiplePubChannelsCreate(Clearing, RegisterUser, channelsCreate, channelsCreate3):
    assert channels_list_v2(RegisterUser["token"]) == {"channels": [
        {
            'channel_id': channelsCreate["channel_id"], 
            'name': 'Teams1', 
        },
        {
            "channel_id": channelsCreate3["channel_id"],
            "name": "Teams4"
        }
    ]}

#tests channels_list after single user creates 1 pvt and 1 public channel  - should return both channels
def test_channels_list_PubandPvtChannel(Clearing, RegisterUser, channelsCreate, channelsCreatePvt):
    assert channels_list_v2(RegisterUser["token"]) == {"channels": [
        {
            'channel_id': channelsCreate["channel_id"], 
            'name': 'Teams1', 
        },
        {
            "channel_id": channelsCreatePvt["channel_id"],
            "name": "Teams3"
        }
    ]}


#channel_listall_v2 tests below

#testing channels_listall with invalid token - should return AccessError
def test_channels_listall_invalidToken(Clearing, RegisterUser, channelsCreate):
    invalidToken = RegisterUser["token"] + "invalid"

    with pytest.raises(AccessError):
        assert channels_listall_v2(invalidToken) 

#testing channels_listall with user registered but no channels created - should return empty list 
def test_channels_listall_nochannels(Clearing, RegisterUser):
    assert channels_listall_v2(RegisterUser["token"]) == {"channels": []}

#testing channels_listall with 1 public channel - should return 1 channel 
def test_channels_listall_singlePublic(Clearing, RegisterUser, channelsCreate):
    assert channels_listall_v2(RegisterUser["token"]) == {"channels": [
        {
            'channel_id': channelsCreate["channel_id"], #channel id
            'name': 'Teams1', #channel name 
        },
    ]}


#testing channels_listall with 1 private channel
def test_channels_listall_singlePrivate(Clearing, RegisterUser, channelsCreatePvt):
    assert channels_listall_v2(RegisterUser["token"]) == {"channels": [
        {
            'channel_id': channelsCreatePvt["channel_id"], #channel id
            'name': 'Teams3', #channel name 
        },
    ]}


#channels_listall with multiple users in multiple channels (pubic and private) - return all channels 
def test_channels_listall_multiple(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelsCreate2, channelsCreatePvt, channelsCreate3):
    assert channels_listall_v2(RegisterUser["token"]) == {"channels": [
        {
            'channel_id': channelsCreate["channel_id"], #channel id
            'name': 'Teams1', #channel name 
        },
        {
            'channel_id': channelsCreate2["channel_id"], #channel id
            'name': 'Teams2', #channel name 
        },
        {
            'channel_id': channelsCreatePvt["channel_id"], #channel id
            'name': 'Teams3', #channel name 
        },
        {
            'channel_id': channelsCreate3["channel_id"], #channel id
            'name': 'Teams4', #channel name 
        },
    ]}

