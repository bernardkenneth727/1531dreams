from src.message import message_send_v2, message_remove_v1, message_edit_v2, message_share_v1, message_react_v1, message_unreact_v1, message_pin_v1, message_unpin_v1
from src.channels import channels_create_v2
from src.channel import channel_invite_v2, channel_addowner_v1, channel_messages_v2
from src.other import clear_v2, search_v2
from src.auth import auth_register_v2
from src.json_edit import access_json, clear_json
from src.dm import dm_create_v1, message_send_dm_v1, dm_messages_v1
from src.message import message_send_v2, message_share_v1
from src.error import InputError, AccessError
import pytest

@pytest.fixture
def clear_users():
    clear_json()

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2()

@pytest.fixture
def SetName(Clearing):
    """
    creates a valid name to be used for testing clear via create channels
    and clears previous names
    """
    name = "Cacti"
    return name

@pytest.fixture
def Set_is_public(Clearing):
    """
    creates a valid boolean for the is_public parameter for channels create
    and clears previous booleans created
    """
    is_public = True
    return is_public

@pytest.fixture
def create_valid_message(Clearing):
    message = "This is a valid message"
    return message
#############################################################################################
#tests for message_send_v2:
#takes in token, channel_id, message
#returns message_id and sends a message from authorised_user to the channel specified by channel_id
#no messges should share an ID with another message

#testing error where message > 1000 characters

def test_message_send_invalid_token(Set_is_public, SetName):
    name = SetName
    is_public = Set_is_public
    with pytest.raises(Exception):
        message_send_v2(-1, name, is_public)

def test_message_send_valid(Set_is_public, SetName):
    clear_v2()
    #setup person using channel
    user = auth_register_v2("babylon.smith@gmail.com", "password", "five", "rolly")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)

    message_id = message_send_v2(token, channel_id["channel_id"], "arvalid" )
    assert type(message_id["message_id"]) == int

def test_message_send_v2_message_too_long(Set_is_public, SetName):
    clear_v2()
    #setup person using channel
    user = auth_register_v2("babylon.smith@gmail.com", "password", "five", "rolly")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    
    with pytest.raises(Exception):
        message_send_v2(token, channel_id["channel_id"], "ar" * 501)
    

#access error when user has not joined channel they are posting to 
#--check if authorised user not in channels

def test_message_send_v2_user_not_joined_channel(Set_is_public, SetName):
    #setup person who owns channel
    user1 = auth_register_v2("rolly.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user1["token"]
    #must have a channel to add to
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    
    #creating user2 who is not part of the channel
    user2 = auth_register_v2("jojomes.smith@gmail.com", "password", "Jimmy", "Smith")
    token2 = user2["token"]
    with pytest.raises(Exception):
        message_send_v2(token2, channel_id["channel_id"], "validmessage")
    clear_v2()


############################################################################################
#tests for message_remove_v1:
#takes in token, message_id
#outputs nothing but removes message given the id

def test_message_remove_invalid_token(Set_is_public, SetName):
    user = auth_register_v2("rabbit.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    
    with pytest.raises(Exception):
        message_remove_v1(-1, message_id['message_id'])

def test_message_remove_valid(Set_is_public, SetName):
    clear_v2()
    #setup person using channel
    user = auth_register_v2("babylon.smith@gmail.com", "password", "five", "rolly")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")

    value = message_remove_v1(token, message_id['message_id'])
    assert value == {}

#message no longer exists
def test_message_remove_v1_message_no_longer_exists(Set_is_public, SetName):
    user = auth_register_v2("rabbit.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    message_remove_v1(token, message_id["message_id"])
    #since message has already been removed in above line, calling for removal again should
    #raise an error
    with pytest.raises(Exception):
        message_remove_v1(token, message_id["message_id"])
    clear_v2()

#whitebox elements: user permissions,
def test_message_remove_v1_user_not_owner_of_channel_or_dreams(Set_is_public, SetName):
    user = auth_register_v2("blokefromeverywehere.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    
    #creating user2 who does not own channel and did not send the message
    user2 = auth_register_v2("django.smith@gmail.com", "password", "Jimmy", "Smith")
    token2 = user2["token"]
    
    with pytest.raises(Exception):
        message_remove_v1(token2, message_id["message_id"])
    clear_v2()


############################################################################################
#tests for message_edit_v2:
#inputs token, message_id, message
# returns nothing but updates message text with new text. If new message is empty the message is 
#deleted

def test_message_edit_invalid_token(Set_is_public, SetName):
    user = auth_register_v2("rabbit.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    
    with pytest.raises(Exception):
        message_edit_v2(-1, message_id['message_id'], "validMessage")

def test_message_edit_valid(Set_is_public, SetName):
    user = auth_register_v2("rabbit.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")

    value = message_edit_v2(token, message_id['message_id'], "message edited")
    assert value == {}

def test_message_edit_v2_over_1000_chars(Set_is_public, SetName):
    #user is part of channel and has sent the message
    user = auth_register_v2("minnesota.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")

    with pytest.raises(Exception):
        message_edit_v2(token, message_id["message_id"], "invalid"*500)
    clear_v2()

def test_message_edit_v2_deleted_message(Set_is_public, SetName):
    user = auth_register_v2("boolean.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    message_remove_v1(token, message_id["message_id"])
    
    #since message has already been removed in above line, the message cannot be edited
    with pytest.raises(Exception):
        message_edit_v2(token, message_id["message_id"], "valid")
    clear_v2()

def test_message_edit_v2_user_not_owner_of_channel_or_dreams(Set_is_public, SetName):
    user = auth_register_v2("raditz.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    
    #creating user2 who does not own channel and did not send the message
    user2 = auth_register_v2("jumbo.smith@gmail.com", "newpass", "John", "Smith")
    token2 = user2["token"]
    
    with pytest.raises(Exception):
        message_edit_v2(token2, message_id["message_id"], "valid")
    clear_v2()

def test_message_edit_v2_empty_message(Set_is_public, SetName):
    #user is part of channel and has sent the message
    user = auth_register_v2("jpegohn.smith@gmail.com", "password", "Jimmy", "Smith")
    token = user["token"]
    name = SetName
    is_public = Set_is_public
    #channel is created with the user as owner
    channel_id = channels_create_v2(token, name, is_public)
    #sending message and getting message id with message send
    message_id = message_send_v2(token, channel_id["channel_id"], "valid message")
    #set new message to empty string
    message_edit_v2(token, message_id["message_id"], "")

    with pytest.raises(Exception):
        message_edit_v2(token, message_id["message_id"], "valid")

    clear_v2()

# Test 1 Test if token is valid
def test_message_share_v1_invalid_token(clear_users):
    owner_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")

    channel0 = channels_create_v2(owner_user["token"], "WARR", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
 
    with pytest.raises(AccessError):
        message_share_v1(-12312124123, message0["message_id"], '', channel0["channel_id"], -1)
    

# Test 2 Test if auth user the authorised user has joined the channel or DM they are trying to share the message to.
def test_message_share_v1_auth_user_join_channel_or_dm(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    member_user1 = auth_register_v2("mrjohncena@gmail.com", "ucantcME", "John", "Cena")

    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    dm0 = dm_create_v1(owner_user["token"],[member_user1["auth_user_id"]])

    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    with pytest.raises(AccessError):
        message_share_v1(member_user["token"], message0["message_id"], '', channel0["channel_id"], -1)
    
    with pytest.raises(AccessError):
        message_share_v1(member_user["token"], message0["message_id"], '', -1, dm0["dm_id"])

# Test 3 Test that  message is being shared to a channel
def test_message_share_v1_to_channel(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    
    shared_message_id = message_share_v1(owner_user["token"], message0["message_id"], "schlong", channel0["channel_id"], -1)
    
    messages = access_json("messages")
    message_found = False
    for message in messages:
        if message["message_id"] == shared_message_id["shared_message_id"] and message["dm_id"] == -1:  
            message_found = True
    
    assert message_found == True

# Test 4 Test that message is the being shared to a dm 
def test_message_share_v1_to_dm(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])

    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")

    shared_message_id = message_share_v1(owner_user["token"], message0["message_id"], "schlong", -1, dm0["dm_id"])

    message_found = False
    messages = access_json("messages")

    for message in messages:
        if message["message_id"] == shared_message_id["shared_message_id"] and message["channel_id"] == -1:  
            message_found = True
   
    assert message_found == True

# message_react_v1 tests
# Test 1 The person reacting is already in the channel or dm
def test_message_react_v1_invalid_user_access(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    member_user2 = auth_register_v2("running@gmail.com", "wooridul", "Seo", "You")

    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])
    thumbs_up = 1
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    message1 = message_send_dm_v1(member_user["token"], dm0["dm_id"], "all day yes")
    # Access error when member that isnt in channel is accessing channel message
    with pytest.raises(AccessError):
       message_react_v1(member_user["token"], message0["message_id"], thumbs_up)
    
    # Access error when a member that isnt in the dm is accessing dm message
    with pytest.raises(AccessError):
        message_react_v1(member_user2["token"], message1["message_id"], thumbs_up)

# Test 2 The message_id is valid
def test_message_react_v1_invalid_message_id(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    thumbs_up = 1
    with pytest.raises(InputError):
        message_react_v1(owner_user["token"], 123123, thumbs_up)

# Test 3 React_id is one
def test_message_react_v1_invalid_react_id(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    invalid_react = 232323232
    with pytest.raises(InputError):
        message_react_v1(owner_user["token"], message0["message_id"], invalid_react)

# Test 4 Reacting the same message raises input error 
def test_message_react_v1_contains_react(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "samemessage")
    thumbs_up = 1
    message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)

    with pytest.raises(InputError):
        message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)

# Test 5 Reacting to a dm message 
def test_message_react_v1_dm(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")

    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])
    message1 = message_send_dm_v1(member_user["token"], dm0["dm_id"], "all day yes")
    thumbs_up = 1
    message_react_v1(owner_user["token"], message1["message_id"], thumbs_up)
    messages = access_json("messages")
    react_found = False

    for message in messages:
        if message1["message_id"] == message["message_id"]:
            tmp_message = message

    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
           if owner_user["auth_user_id"] in react["u_ids"]:
               react_found = True

    assert react_found == True

# Test 6 Reacting to a channel message 
def test_message_react_v1_channel(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "samemessage")
    thumbs_up = 1
    message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)

    messages = access_json("messages")
    react_found = False

    for message in messages:
        if message0["message_id"] == message["message_id"]:
            tmp_message = message

    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
           if owner_user["auth_user_id"] in react["u_ids"]:
               react_found = True

    assert react_found == True

# Test 7 Reacting to a spam dm message
def test_message_react_v1_spam_dm(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    
    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])
    thumbs_up = 1

    i = 0
    while i < 20:
        message0 = message_send_dm_v1(member_user["token"], dm0["dm_id"], "all day yes")
        message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)
        i = i + 1
    messages = access_json("messages")

    for message in messages:
        assert message["reacts"][0]["u_ids"][0] == owner_user["auth_user_id"]


# message_unreact_v1 tests
# Test 1 The person unreacting isnt in the channel or dm
def test_message_unreact_v1_invalid_user_access(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    member_user2 = auth_register_v2("running@gmail.com", "wooridul", "Seo", "You")

    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])
    thumbs_up = 1
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    message1 = message_send_dm_v1(member_user["token"], dm0["dm_id"], "all day yes")
    
    # Access error when member that isnt in channel is accessing channel message
    with pytest.raises(AccessError):
       message_unreact_v1(member_user["token"], message0["message_id"], thumbs_up)
    
    # Access error when a member that isnt in the dm is accessing dm message
    with pytest.raises(AccessError):
        message_unreact_v1(member_user2["token"], message1["message_id"], thumbs_up)

# Test 2 The message_id is valid
def test_message_unreact_v1_invalid_message_id(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    thumbs_up = 1
    with pytest.raises(InputError):
        message_unreact_v1(owner_user["token"], 123123, thumbs_up)

# Test 3 React_id is one
def test_message_unreact_v1_invalid_react_id(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "helloworld")
    invalid_react = 232323232
    with pytest.raises(InputError):
        message_unreact_v1(owner_user["token"], message0["message_id"], invalid_react)

# Test 4 Unreacting a message with no active react raises InputError 
def test_message_unreact_v1_no_react(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "samemessage")
    thumbs_up = 1

    with pytest.raises(InputError):
        message_unreact_v1(owner_user["token"], message0["message_id"], thumbs_up)

# Test 5 Unreating a message in the channel
def test_message_unreact_v1(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    channel0 = channels_create_v2(owner_user["token"], "Littt", False)
    message0 = message_send_v2(owner_user["token"], channel0["channel_id"], "samemessage")
    thumbs_up = 1
    
    react_found = False
    message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)
    
    channel_m = channel_messages_v2(owner_user["token"], channel0["channel_id"], 0)

    for message in channel_m["messages"]:
        if message0["message_id"] == message["message_id"]:
            tmp_message = message
    
    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
            if owner_user["auth_user_id"] in react["u_ids"]:
               react_found = True

    assert react_found == True    

    message_unreact_v1(owner_user["token"], message0["message_id"], thumbs_up)
    channel_m = channel_messages_v2(owner_user["token"], channel0["channel_id"], 0)
    
    for message in channel_m["messages"]:
        if message0["message_id"] == message["message_id"]:
            tmp_message = message
    
    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
            if owner_user["auth_user_id"] in react["u_ids"]:
                react_found = True
            else:
                react_found = False

    assert react_found == False
    
# Test 6 Unreating a message in the dm
def test_message_unreact_v1_dm(clear_users):
    owner_user = auth_register_v2("honxing@gmail.com", "devilsdrink", "Hong", "Xing")
    member_user = auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    dm0 = dm_create_v1(owner_user["token"],[member_user["auth_user_id"]])

    message0 = message_send_dm_v1(member_user["token"], dm0["dm_id"], "all day yes")
    thumbs_up = 1

    react_found = False
    message_react_v1(owner_user["token"], message0["message_id"], thumbs_up)
    
    search_m = search_v2(owner_user["token"], "all day yes")
    for message in search_m["messages"]:
        if message["message_id"] == message0["message_id"]:
            tmp_message = message 
    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
            if owner_user["auth_user_id"] in react["u_ids"]:
                react_found = True

    assert react_found == True

    message_unreact_v1(owner_user["token"], message0["message_id"], thumbs_up)
    search_m = search_v2(owner_user["token"], "all day yes")

    for message in search_m["messages"]:
        if message["message_id"] == message0["message_id"]:
            tmp_message = message 

    for react in tmp_message["reacts"]:
        if react["react_id"] == thumbs_up:
            if owner_user["auth_user_id"] in react["u_ids"]:
                react_found = True
            else:
                react_found = False

    assert react_found == False

# Jonathan Sinani
# Testing message_pin_v1.
# Sets up a channel with 1 owner and 1 member and 1 message sent.
@pytest.fixture
def setup_channel(Clearing):
    channel_owner = auth_register_v2("hello@gmail.com", "123456", "John", "Hello")
    channel_member = auth_register_v2("member@gmail.com", "123456", "John", "Member")
    
    my_channel = channels_create_v2(channel_owner["token"], "My Channel", True)
    channel_invite_v2(channel_owner["token"], my_channel["channel_id"], channel_member["auth_user_id"])
    
    my_message = message_send_v2(channel_owner["token"], my_channel["channel_id"], 'Hello there!')
    return {
        'owner': channel_owner,
        'member': channel_member,
        'channel': my_channel,
        'message': my_message
    }

# Checks that a normal pin from the owner works.
def test_message_pin_valid_normal(setup_channel):
    assert message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"]) == {}
    
    assert channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Tests an invalid message id attempting to be pinned.
def test_message_pin_invalid_message(setup_channel):
    with pytest.raises(InputError):
        message_pin_v1(setup_channel["owner"]["token"], -1)
    
    assert not channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Tests where the message is already pinned, so it cannot be pinned again.
def test_message_pin_invalid_already_pinned(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    with pytest.raises(InputError):
        message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

# Tests a non-member of the channel attempting to pin a message (should raise AccessError).
def test_message_pin_invalid_non_member(setup_channel):
    user = auth_register_v2("gmail@gmail.com", "PASSWORD", "John", "Gmail")

    with pytest.raises(AccessError):
        message_pin_v1(user["token"], setup_channel["message"]["message_id"])

# Tests a non-owner of the channel attempting to pin a message (should raise AccessError).
def test_message_pin_invalid_non_owner(setup_channel):
    with pytest.raises(AccessError):
        message_pin_v1(setup_channel["member"]["token"], setup_channel["message"]["message_id"])

# Tests an invalid token attempting to pin a message (should raise AccessError).
def test_message_pin_invalid_token(setup_channel):
    with pytest.raises(AccessError):
        message_pin_v1(-1, setup_channel["message"]["message_id"]) 

# Tests a case where this reflects a DM instead of a channel.
def test_message_pin_valid_dm(setup_channel):
    my_dm = dm_create_v1(setup_channel["owner"]["token"], [setup_channel["member"]["auth_user_id"]])

    my_dm_message = message_send_dm_v1(setup_channel["owner"]["token"], my_dm["dm_id"], "Hello world!")
    assert message_pin_v1(setup_channel["owner"]["token"], my_dm_message["message_id"]) == {}
    
    assert dm_messages_v1(setup_channel["owner"]["token"], my_dm["dm_id"], 0)["messages"][0]["is_pinned"]

# Tests a case where many messages are pinned.
def test_message_pin_valid_many(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    for i in range(10):
        my_message = message_send_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], f"{i}. Hello")
        assert message_pin_v1(setup_channel["owner"]["token"], my_message["message_id"]) == {}
        
# Testing message_unpin_v1
# Tests a case where the message is pinned, so it can be unpinned.
def test_message_unpin_valid_normal(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])
    assert channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

    assert message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"]) == {}
    assert not channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Testing a case where the message_id is unvalid, so an exception is raised.
def test_message_unpin_invalid_message_id(setup_channel): 
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    with pytest.raises(InputError):
        message_unpin_v1(setup_channel["owner"]["token"], -1)
    
    assert channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Testing a case where the message is not pinned, so it cannot be unpinned.
def test_message_unpin_invalid_not_pinned(setup_channel):
    with pytest.raises(InputError):
        message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    assert not channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Testing a case where the token given is not valid.
def test_message_unpin_invalid_token(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    with pytest.raises(AccessError):
        message_unpin_v1(-1, setup_channel["message"]["message_id"])

# Testing a case where the user attempting to unpin the message is not a member.
def test_message_unpin_invalid_not_member(setup_channel):
    user = auth_register_v2("unpin@gmail.com", "PASSWORD", "John", "Unpin")
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    with pytest.raises(AccessError):
        message_pin_v1(user["token"], setup_channel["message"]["message_id"])

# Testing a case where the user attempting to unpin the message is not a channel owner.
def test_message_unpin_invalid_not_owner(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

    with pytest.raises(AccessError):
        message_pin_v1(setup_channel["member"]["token"], setup_channel["message"]["message_id"])

# Testing a case with a message being unpinned and pinned multiple times.
def test_message_unpin_valid_pin(setup_channel):
    i = 0
    while i < 20:
        message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])
        assert message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"]) == {}
        i += 1
    
    assert not channel_messages_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], 0)["messages"][0]["is_pinned"]

# Testing a case where a dm message is being unpinned.
def test_message_unpin_valid_dm(setup_channel):
    my_dm = dm_create_v1(setup_channel["owner"]["token"], [setup_channel["member"]["auth_user_id"]])

    my_message = message_send_dm_v1(setup_channel["owner"]["token"], my_dm["dm_id"], "BIG NEWS")

    message_pin_v1(setup_channel["owner"]["token"], my_message["message_id"])
    assert dm_messages_v1(setup_channel["owner"]["token"], my_dm["dm_id"], 0)["messages"][0]["is_pinned"]

    assert message_unpin_v1(setup_channel["owner"]["token"], my_message["message_id"]) == {}
    assert not dm_messages_v1(setup_channel["owner"]["token"], my_dm["dm_id"], 0)["messages"][0]["is_pinned"]

# Testing a case where the message has already been unpinned, so it cannot be unpinned again.
def test_message_unpin_invalid_already_unpinned(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])
    message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])
    
    with pytest.raises(InputError):
        message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])

# Testing a case where many messages that are pinned are now unpinned.
def test_message_unpin_valid_messages(setup_channel):
    message_pin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"])
    assert message_unpin_v1(setup_channel["owner"]["token"], setup_channel["message"]["message_id"]) == {}

    for i in range(20):
        many = message_send_v2(setup_channel["owner"]["token"], setup_channel["channel"]["channel_id"], f"Wassup! ({i})")
        message_pin_v1(setup_channel["owner"]["token"], many["message_id"])
        assert message_unpin_v1(setup_channel["owner"]["token"], many["message_id"]) == {}
