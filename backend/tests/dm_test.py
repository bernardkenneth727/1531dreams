from src.dm import dm_messages_v1, dm_remove_v1, dm_create_v1, message_send_dm_v1, dm_invite_v1, dm_leave_v1, dm_details_v1, dm_list_v1
from src.auth import auth_register_v2
from src.error import AccessError, InputError
from src.other import clear_v2
from src.secret import token_encode
from src.json_edit import access_json
import pytest
import uuid
from src import config

# Lawson's dm function tests


@pytest.fixture
def clear():
    clear_v2()


@pytest.fixture
def user1():
    return auth_register_v2("example@example.com", "password", "John", "Doe")


@pytest.fixture
def user2():
    return auth_register_v2("example2@example.com", "password", "Jane", "Doe")


@pytest.fixture
def dm(user1, user2):
    return dm_create_v1(user1["token"], [user2["auth_user_id"]])

# dm_messages tests


def test_dm_messages_return_type(clear, user1, dm):
    message = "nick is cool"
    start = 0
    message_send_dm_v1(user1["token"], dm["dm_id"], message)
    assert type(dm_messages_v1(user1["token"], dm["dm_id"], start)) is dict


def test_dm_messages_return_message(clear, user1, dm):
    message = "nick is cool"
    start = 0
    message_id = message_send_dm_v1(
        user1["token"], dm["dm_id"], message)["message_id"]
    messages = dm_messages_v1(user1["token"], dm["dm_id"], start)["messages"]
    message_expected = {
        "message_id": message_id,
        "message": message,
        "u_id": user1["auth_user_id"],
        "reacts": [{'react_id': 1, 'u_ids': [], 'is_this_user_reacted': False}],
        "is_pinned": False
    }
    message_selected = {}

    for msg in messages:
        if msg["message_id"] == message_id:
            message_selected = msg

    del message_selected["time_created"]

    assert message_selected == message_expected


def test_dm_messages_start_greater_messages(clear, user1, dm):
    start = 50
    with pytest.raises(InputError):
        dm_messages_v1(user1["token"], dm["dm_id"], start)


def test_dm_messages_return_message_user_not_dm_member(clear, user1, dm):
    message = "nick is cool"
    start = 0
    message_send_dm_v1(user1["token"], dm["dm_id"], message)
    user_not_member = auth_register_v2(
        "example3@example.com", "password", "Jack", "Doe")

    with pytest.raises(AccessError):
        dm_messages_v1(user_not_member["token"], dm["dm_id"], start)


def test_dm_messages_valid_dm(clear, user1, dm):
    message = "nick is cool"
    start = 0
    message_send_dm_v1(user1["token"], dm["dm_id"], message)

    with pytest.raises(InputError):
        dm_messages_v1(user1["token"], 1, start)


def test_dm_messages_end_many_messages(clear, user1, dm):
    message = 'welcome to comp'
    start = 0

    x = 0
    while x < 51:
        message_send_dm_v1(user1['token'], dm['dm_id'], message)
        x += 1

    assert dm_messages_v1(user1['token'], dm['dm_id'], start)["end"] == 50

# dm_remove tests


def test_dm_remove_return_type(clear, user1, dm):
    assert type(dm_remove_v1(user1["token"], dm["dm_id"])) is dict


def test_dm_remove_not_creator(clear, user2, dm):
    with pytest.raises(AccessError):
        dm_remove_v1(user2["token"], dm["dm_id"])


def test_dm_remove_invalid_dm_id(clear, user1):
    with pytest.raises(InputError):
        dm_remove_v1(user1["token"], 1)


def test_dm_remove_owner_valid(clear, dm):
    token = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })
    with pytest.raises(AccessError):
        dm_remove_v1(token, dm["dm_id"])


def test_dm_remove_json_change(clear, dm, user1, user2):
    dms = access_json("dms")
    dm_expected = [{
        "dm_id": dm["dm_id"],
        "dm_name": dm["dm_name"],
        "members_owner": [
            {
                "u_id": user1["auth_user_id"]
            }
        ],
        "members_all": [
            {
                "u_id": user1["auth_user_id"]
            },
            {
                "u_id": user2["auth_user_id"]
            }
        ]
    }]
    assert dms == dm_expected

    dm_remove_v1(user1["token"], dm["dm_id"])
    dms = access_json("dms")
    assert dms == []

# Lawson's dm function tests

# dm_create tests


def test_dm_create_return_type(clear, user1, user2):
    assert type(dm_create_v1(user1["token"], [user2["auth_user_id"]])) is dict


def test_dm_create_only_owner(clear, user1):
    assert dm_create_v1(user1["token"], [])["dm_name"] == "johndoe"


def test_dm_create_return_handle(clear, user1, user2):
    assert dm_create_v1(user1["token"], [user2["auth_user_id"]])[
        "dm_name"] == "janedoe, johndoe"


def test_dm_create_return_multiple_handles(clear, user1, user2):
    user3 = auth_register_v2("example3@example.com", "password", "Jack", "Doe")
    assert dm_create_v1(user1["token"], [user2["auth_user_id"], user3["auth_user_id"]])[
        "dm_name"] == "jackdoe, janedoe, johndoe"


def test_dm_create_valid_user(clear, user1):
    with pytest.raises(InputError):
        dm_create_v1(user1["token"], [1])


def test_dm_create_owner_valid(clear, user1):
    token = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })
    with pytest.raises(AccessError):
        dm_create_v1(token, [user1["auth_user_id"]])


def test_dm_create_json_change(clear, user1, user2):
    result = dm_create_v1(user1["token"], [user2["auth_user_id"]])
    assert access_json("dms") == [
        {
            "dm_id": result["dm_id"],
            "dm_name": result["dm_name"],
            "members_owner": [
                {
                    "u_id": user1["auth_user_id"]
                }
            ],
            "members_all": [
                {
                    "u_id": user1["auth_user_id"]
                },
                {
                    "u_id": user2["auth_user_id"]
                }
            ]
        }
    ]


# message_send_dm tests

def test_message_send_dm_return_type(clear, user1, user2, dm):
    message = "Hi. This is the best team in COMP1531"
    assert type(message_send_dm_v1(
        user1["token"], dm["dm_id"], message)) is dict

    assert type(message_send_dm_v1(
        user2["token"], dm["dm_id"], message)) is dict


def test_message_send_dm_return_matches_json_id(clear, user1, dm):
    message = "Hi. This is the best team in COMP1531"

    message_id = message_send_dm_v1(
        user1["token"], dm["dm_id"], message)["message_id"]

    # get messages sent in dm
    messages = access_json("messages")
    messages_id = []
    for msg in messages:
        if msg["dm_id"] == dm["dm_id"]:
            messages_id.append(msg["message_id"])

    assert message_id in messages_id


def test_message_send_dm_message_long(clear, user1, dm):
    message = "lit" * 1000

    with pytest.raises(InputError):
        message_send_dm_v1(user1["token"], dm["dm_id"], message)


def test_message_send_dm_user_not_member(clear, dm):
    message = "Hi. This is the best team in COMP1531"
    user_not_member = auth_register_v2(
        "example3@example.com", "password", "Jack", "Doe")

    with pytest.raises(AccessError):
        message_send_dm_v1(user_not_member["token"], dm["dm_id"], message)

# def test_message_send_dm_valid_user(clear, dm):
#     message = "Hi. This is the best team in COMP1531"

#     with pytest.raises(AccessError):
#         message_send_dm_v1(1, dm["dm_id"], message)


def test_message_send_dm_empy_message(clear, user1, dm):
    message = ""
    message_id = message_send_dm_v1(
        user1["token"], dm["dm_id"], message)["message_id"]

    # get message dictionary in json matching message_id
    messages = access_json("messages")
    entry_accessed = {}

    for msg in messages:
        if msg["message_id"] == message_id:
            entry_accessed = msg

    # remove time_created key since send_dm and test are at different times
    del entry_accessed["time_created"]

    entry_expected = {
        "message_id": message_id,
        "message": "",
        "u_id": user1["auth_user_id"],
        "channel_id": -1,
        "dm_id": dm["dm_id"],
        "reacts": [{"react_id": 1,
                    "u_ids": [],
                    }],
        "is_pinned": False
    }

    assert entry_expected == entry_accessed


# Yoni's dm function tests
@pytest.fixture
def intro_dm():
    clear_v2()
    result1 = auth_register_v2(
        "johnsmith@gmail.com", "123456", "John", "Smith")
    result2 = auth_register_v2(
        "michaelscott@dundermifflin.com", "PASSWORD", "Michael", "Scott")
    result3 = auth_register_v2(
        "leslieknope@pawnee.gov", "JoeBiden1", "Leslie", "Knope")
    result4 = auth_register_v2(
        "jake.peralta@nypdbrooklyn.gov", "nine-nine", "Jake", "Peralta")
    result5 = auth_register_v2(
        "eleanorshellstrop@yahoo.com", "TheBadPlace", "Eleanor", "Shellstrop")
    new_dm = dm_create_v1(
        result1["token"], [result2["auth_user_id"], result3["auth_user_id"]])

    return {
        "john": result1,
        "michael": result2,
        "leslie": result3,
        "jake": result4,
        "eleanor": result5,
        "dm": new_dm
    }


@pytest.fixture
def invalid_token():
    return token_encode({
        "auth_user_id": -1,
        "session_id": -1
    })


@pytest.fixture
def extra_dm():
    result1 = auth_register_v2(
        "jonnysmithy@gmail.com", "123456", "Jonny", "Smith")
    result2 = auth_register_v2(
        "jimhalpert@dundermifflin.com", "DWIGHT", "Jim", "Halpert")
    dm_create_v1(result1["token"], [result2["auth_user_id"]])


# Testing dm_invite_v1.
# Tests a normal invite scenario that is valid.
def test_dm_invite_valid_normal(intro_dm):
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 4

    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["eleanor"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 5

# Testing when the non-original creator of the dm invites is valid.


def test_dm_invite_valid_non_owner(intro_dm):
    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_invite_v1(intro_dm["michael"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 4

    dm_invite_v1(intro_dm["jake"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["eleanor"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["jake"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 5

# Testing when the token parameter for the auth user is invalid.


def test_dm_invite_invalid_token(intro_dm, invalid_token):
    with pytest.raises(AccessError):
        dm_invite_v1(
            invalid_token, intro_dm["dm"]["dm_id"], intro_dm["jake"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing when the user being invited has an invalid u_id.


def test_dm_invite_invalid_u_id(intro_dm):
    with pytest.raises(InputError):
        dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"], -1)
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing when the dm_id being used to invite someone is invalid.


def test_dm_invite_invalid_dm_id(intro_dm):
    with pytest.raises(InputError):
        dm_invite_v1(intro_dm["john"]["token"], -1,
                     intro_dm["eleanor"]["auth_user_id"])

# Testing when the user attempting to invite is not authorised (they are not in the dm).


def test_dm_invite_invalid_not_auth_user(intro_dm):
    with pytest.raises(AccessError):
        dm_invite_v1(intro_dm["jake"]["token"], intro_dm["dm"]
                     ["dm_id"], intro_dm["jake"]["auth_user_id"])

    with pytest.raises(AccessError):
        dm_invite_v1(intro_dm["jake"]["token"], intro_dm["dm"]
                     ["dm_id"], intro_dm["eleanor"]["auth_user_id"])

    with pytest.raises(AccessError):
        dm_invite_v1(intro_dm["eleanor"]["token"], intro_dm["dm"]
                     ["dm_id"], intro_dm["john"]["auth_user_id"])

    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing inviting the same person multiple times is valid.


def test_dm_invite_valid_duplicates(intro_dm):
    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["john"]["auth_user_id"])
    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])
    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])

    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 4

# Testing inviting someone to a dm that's not the first dm created.


def test_dm_invite_valid_more_dms(intro_dm, extra_dm):
    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])
    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 4

# Testing dm_leave_v1.
# Testing a normal valid dm leave scenario.


def test_dm_leave_valid_normal(intro_dm):
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_leave_v1(intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 2

    dm_leave_v1(intro_dm["leslie"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 1

# Testing the original creator leaving the dm scenario is valid.


def test_dm_leave_valid_creator(intro_dm):
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_leave_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 2

# Testing an invalid token leaving the dm.


def test_dm_leave_invalid_token(intro_dm, invalid_token):
    with pytest.raises(AccessError):
        dm_leave_v1(invalid_token, intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing an incorrect dm_id for a member leaving the dm.


def test_dm_leave_invalid_dm(intro_dm):
    with pytest.raises(InputError):
        dm_leave_v1(intro_dm["michael"]["token"], -1)
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing the authorised user (in the token) not being in the dm to leave.


def test_dm_leave_invalid_auth_user(intro_dm):
    with pytest.raises(AccessError):
        dm_leave_v1(intro_dm["jake"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

# Testing the authorised user leaving multiple times where the first time is
# valid but after is invalid.


def test_dm_leave_invalid_multiple_times(intro_dm):
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 3

    dm_leave_v1(intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 2

    with pytest.raises(AccessError):
        dm_leave_v1(intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 2

# Testing someone leaving a dm that's not the first dm created.


def test_dm_leave_valid_more_dms(intro_dm, extra_dm):
    dm_leave_v1(intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])
    assert len(dm_details_v1(
        intro_dm["john"]["token"], intro_dm["dm"]["dm_id"])["members"]) == 2

# Testing dm_list_v1.
# Testing a normal valid circumstance with a singular dm.


def test_dm_list_valid_singular(intro_dm):
    assert dm_list_v1(intro_dm["john"]["token"]) == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }
        ]
    }

# Testing a normal valid circumstance with multiple dms.


def test_dm_list_valid_multiple(intro_dm):
    result1 = dm_create_v1(intro_dm["john"]["token"], [
                           intro_dm["jake"]["auth_user_id"]])
    result2 = dm_create_v1(intro_dm["john"]["token"], [
                           intro_dm["leslie"]["auth_user_id"]])
    result3 = dm_create_v1(intro_dm["john"]["token"], [
                           intro_dm["eleanor"]["auth_user_id"]])

    assert dm_list_v1(intro_dm["john"]["token"]) == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }, {
                "dm_id": result1["dm_id"],
                "name": result1["dm_name"]
            }, {
                "dm_id": result2["dm_id"],
                "name": "johnsmith, leslieknope"
            }, {
                "dm_id": result3["dm_id"],
                "name": "eleanorshellstrop, johnsmith"
            }
        ]
    }

# Testing a normal valid circumstance with the auth_user not in any dms.


def test_dm_list_valid_empty(intro_dm):
    assert dm_list_v1(intro_dm["jake"]["token"]) == {
        "dms": []
    }

# Testing an invalid circumstance with the token not being valid.


def test_dm_list_invalid_token(intro_dm, invalid_token):
    with pytest.raises(AccessError):
        dm_list_v1(invalid_token)

# Testing a valid circumstance where the auth is added to a dm.


def test_dm_list_valid_invited(intro_dm):
    assert dm_list_v1(intro_dm["jake"]["token"]) == {
        "dms": []
    }

    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])

    assert dm_list_v1(intro_dm["jake"]["token"]) == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }
        ]
    }

# Testing a valid circumstance where the auth leaves the dm.


def test_dm_list_valid_leave(intro_dm):
    assert dm_list_v1(intro_dm["leslie"]["token"]) == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }
        ]
    }

    dm_leave_v1(intro_dm["leslie"]["token"], intro_dm["dm"]["dm_id"])

    assert dm_list_v1(intro_dm["leslie"]["token"]) == {
        "dms": []
    }

# Testing dm_details_v1.
# Testing a normal valid circumstance.


def test_dm_details_valid_normal(intro_dm):
    assert dm_details_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"]) == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [
            {
                "u_id": intro_dm["john"]["auth_user_id"],
                "email": "johnsmith@gmail.com",
                "name_first": "John",
                "name_last": "Smith",
                "handle_str": "johnsmith",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["michael"]["auth_user_id"],
                "email": "michaelscott@dundermifflin.com",
                "name_first": "Michael",
                "name_last": "Scott",
                "handle_str": "michaelscott",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["leslie"]["auth_user_id"],
                "email": "leslieknope@pawnee.gov",
                "name_first": "Leslie",
                "name_last": "Knope",
                "handle_str": "leslieknope",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }
        ]
    }

# Testing a valid circumstance where a user is invited to the dm.


def test_dm_details_valid_invite(intro_dm):
    dm_invite_v1(intro_dm["john"]["token"], intro_dm["dm"]
                 ["dm_id"], intro_dm["jake"]["auth_user_id"])
    assert dm_details_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"]) == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [
            {
                "u_id": intro_dm["john"]["auth_user_id"],
                "email": "johnsmith@gmail.com",
                "name_first": "John",
                "name_last": "Smith",
                "handle_str": "johnsmith",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["michael"]["auth_user_id"],
                "email": "michaelscott@dundermifflin.com",
                "name_first": "Michael",
                "name_last": "Scott",
                "handle_str": "michaelscott",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["leslie"]["auth_user_id"],
                "email": "leslieknope@pawnee.gov",
                "name_first": "Leslie",
                "name_last": "Knope",
                "handle_str": "leslieknope",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["jake"]["auth_user_id"],
                "email": "jake.peralta@nypdbrooklyn.gov",
                "name_first": "Jake",
                "name_last": "Peralta",
                "handle_str": "jakeperalta",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }
        ]
    }

# Testing a valid circumstance where a user leaves the dm.


def test_dm_details_valid_leave(intro_dm):
    dm_leave_v1(intro_dm["leslie"]["token"], intro_dm["dm"]["dm_id"])
    assert dm_details_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"]) == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [
            {
                "u_id": intro_dm["john"]["auth_user_id"],
                "email": "johnsmith@gmail.com",
                "name_first": "John",
                "name_last": "Smith",
                "handle_str": "johnsmith",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["michael"]["auth_user_id"],
                "email": "michaelscott@dundermifflin.com",
                "name_first": "Michael",
                "name_last": "Scott",
                "handle_str": "michaelscott",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }
        ]
    }

# Testing an invalid circumstance where the dm_id is not valid.


def test_dm_details_invalid_dm_id(intro_dm):
    with pytest.raises(InputError):
        dm_details_v1(intro_dm["john"]["token"], -1)

# Testing an invalid circumstance where the auth_user (from token) is not a
# member of the dm.


def test_dm_details_invalid_auth_user(intro_dm):
    with pytest.raises(AccessError):
        dm_details_v1(intro_dm["jake"]["token"], intro_dm["dm"]["dm_id"])

    with pytest.raises(AccessError):
        dm_details_v1(intro_dm["eleanor"]["token"], intro_dm["dm"]["dm_id"])

# Testing an invalid circumstance where the token is not a valid user and
# session.


def test_dm_details_invalid_token(intro_dm, invalid_token):
    with pytest.raises(AccessError):
        dm_details_v1(invalid_token, intro_dm["dm"]["dm_id"])

# Testing a valid circumstance where all the users (except admin) have left the dm.


def test_dm_details_valid_empty(intro_dm):
    dm_leave_v1(intro_dm["michael"]["token"], intro_dm["dm"]["dm_id"])
    dm_leave_v1(intro_dm["leslie"]["token"], intro_dm["dm"]["dm_id"])

    assert dm_details_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"]) == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [{
            "u_id": intro_dm["john"]["auth_user_id"],
            "email": "johnsmith@gmail.com",
            "name_first": "John",
            "name_last": "Smith",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg"
        }]
    }

# Testing finding the details of a dm that's not the first dm created.


def test_dm_details_valid_more_dms(intro_dm, extra_dm):
    assert dm_details_v1(intro_dm["john"]["token"], intro_dm["dm"]["dm_id"]) == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [
            {
                "u_id": intro_dm["john"]["auth_user_id"],
                "email": "johnsmith@gmail.com",
                "name_first": "John",
                "name_last": "Smith",
                "handle_str": "johnsmith",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["michael"]["auth_user_id"],
                "email": "michaelscott@dundermifflin.com",
                "name_first": "Michael",
                "name_last": "Scott",
                "handle_str": "michaelscott",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["leslie"]["auth_user_id"],
                "email": "leslieknope@pawnee.gov",
                "name_first": "Leslie",
                "name_last": "Knope",
                "handle_str": "leslieknope",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }
        ]
    }
