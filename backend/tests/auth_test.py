# Jonathan (Yoni)
import pytest
from src.auth import auth_login_v2, auth_register_v2, auth_logout_v1, auth_passwordreset_request_v1, auth_passwordreset_reset_v1
from src.error import InputError, AccessError
from src.other import clear_v2
from src.secret import token_encode
from src.user import user_profile_v2
from src.auth_code import reset

# Basic fixture to clear the data
@pytest.fixture
def clear_users():
    clear_v2()

# Testing auth_register_v2.
# Tests auth_register_v2 with normal valid names, passwords and emails.
def test_auth_register_valid_normal(clear_users):
    result0 = auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")
    assert user_profile_v2(result0["token"], result0["auth_user_id"])["user"]["handle_str"] == 'johnsmith'

    result1 = auth_register_v2("michaelscott@yahoo.com", "that'swhatshesaid", "Michael", "Scott")
    assert user_profile_v2(result1["token"], result1["auth_user_id"])["user"]["handle_str"] == 'michaelscott'

    result2 = auth_register_v2("jim_halpert@hotmail.com", "pam1234", "Jim", "Halpert")
    assert user_profile_v2(result2["token"], result2["auth_user_id"])["user"]["handle_str"] == 'jimhalpert'

    result3 = auth_register_v2("beesly.pam@outlook.com", "CecePhilip", "Pam", "Beesly")
    assert user_profile_v2(result3["token"], result3["auth_user_id"])["user"]["handle_str"] == 'pambeesly'

    result4 = auth_register_v2("dwight@schrute_farms.co", "b33troot", "Dwight", "Schrute")
    assert user_profile_v2(result4["token"], result4["auth_user_id"])["user"]["handle_str"] == 'dwightschrute'

# Tests auth_register_v2 with full names that are repeated in the handle.
def test_auth_register_valid_renames(clear_users):
    result0 = auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")
    assert user_profile_v2(result0["token"], result0["auth_user_id"])["user"]["handle_str"] == 'johnsmith'

    result1 = auth_register_v2("smith_john@gmail.com", "smith1234", "John", "Smith")
    assert user_profile_v2(result1["token"], result1["auth_user_id"])["user"]["handle_str"] == 'johnsmith0'

    result2 = auth_register_v2("john@smith.com", "champion_1", "John", "Smith")
    assert user_profile_v2(result2["token"], result2["auth_user_id"])["user"]["handle_str"] == 'johnsmith1'

    result3 = auth_register_v2("johns_mith@yahoo.com", "c00l_name", "Johns", "Mith")
    assert user_profile_v2(result3["token"], result3["auth_user_id"])["user"]["handle_str"] == 'johnsmith2'

# Tests auth_register_v2 with full names that are longer than the 20 character
# limit in the handle.
def test_auth_register_valid_limit(clear_users):
    result = auth_register_v2("jerry@seinfeld.com", "654321", "Jerome Jerry Allen", "Seinfeld")
    assert user_profile_v2(result["token"], result["auth_user_id"])["user"]["handle_str"] == 'jeromejerryallensein'

# Tests auth_register_v2 with an invalid email.
def test_auth_register_invalid_email(clear_users):
    with pytest.raises(InputError):
        assert auth_register_v2("hello.com.", "hello123", "John", "Hello")
    
    with pytest.raises(InputError):
        assert auth_register_v2("@email", "password", "First", "Last")
    
    with pytest.raises(InputError):
        assert auth_register_v2("qwerty@asdf.au.com", "123456", "Qwerty", "Asdf")

# Tests auth_register_v2 with a repeated email used (invalid).
def test_auth_register_invalid_reemail(clear_users):
    auth_register_v2("email@myemail.com", "hello123!", "Art", "Vandelay")
    with pytest.raises(InputError):
        assert auth_register_v2("email@myemail.com", "mypassword", "George", "Costanza")

 # Tests auth_register_v2 with a password that is invalid.
def test_auth_register_invalid_password(clear_users):
    with pytest.raises(InputError):
        assert auth_register_v2("george.costanza@gmail.com", "1234", "George", "Costanza")
    
    with pytest.raises(InputError):
        assert auth_register_v2("kramer@yahoo.com", "", "Cosmo", "Kramer")

# Tests auth_register_v2 with a first name or last name that is invalid.
def test_auth_register_invalid_name(clear_users):
    # Last name too short.
    with pytest.raises(InputError):
        assert auth_register_v2("george@gmail.com", "123456", "George", "")
    
    # First name too short.
    with pytest.raises(InputError):
        assert auth_register_v2("drwho@yahoo.com", "123456", "", "Who")
    
    # First name too long.
    with pytest.raises(InputError):
        assert auth_register_v2("hhhh@hotmail.com", "123456", "H" * 51, "uman")
    
    # Last name too long.
    with pytest.raises(InputError):
        assert auth_register_v2("long@surname.com", "123456", "Michael", "L" * 51)

# Testing auth_login_v2.
# Testing logging in with a valid account.
def test_auth_login_valid(clear_users):
    result0 = auth_register_v2("hello@gmail.com", "123456!", "Hello", "World")
    result0_login = auth_login_v2("hello@gmail.com", "123456!")
    assert result0["auth_user_id"] == result0_login["auth_user_id"]

    result1 = auth_register_v2("doctor@unsw.com", "D0ct0r", "Doctor", "UNSW")
    result1_login = auth_login_v2("doctor@unsw.com", "D0ct0r")
    assert result1["auth_user_id"] == result1_login["auth_user_id"]
    assert result1_login["auth_user_id"] != result0["auth_user_id"]

    result2 = auth_register_v2("people@mac.com", "******", "Person", "People")
    result2_login = auth_login_v2("people@mac.com", "******")

    assert result2["auth_user_id"] == result2_login["auth_user_id"]
    assert result2["token"] != result2_login["token"]

# Testing logging in with an invalid email.
def test_auth_login_invalid_email(clear_users):
    auth_register_v2("hello@gmail.com", "password", "Hello", "Com")
    with pytest.raises(InputError):
        auth_login_v2("hello@gmail", "password")
    
    with pytest.raises(InputError):
        auth_login_v2("gmail.com", "password")
    
    with pytest.raises(InputError):
        auth_login_v2("hello@gmail.com.", "password")

# Testing logging in with the correct email in system but the password is wrong.
def test_auth_login_incorrect_password(clear_users):
    auth_register_v2("johnsmith@yahoo.com", "()()()", "John", "Smith")
    with pytest.raises(InputError):
        auth_login_v2("johnsmith@yahoo.com", "()()()1")
    
    with pytest.raises(InputError):
        auth_login_v2("johnsmith@yahoo.com", "909090")
    
    auth_register_v2("name@gmail.com", "password", "Name", "Name")
    with pytest.raises(InputError):
        auth_login_v2("name@gmail.com", "")
    
    with pytest.raises(InputError):
        auth_login_v2("name@gmail.com", "PASSWORD")

# Tests logging in with an email that is valid but not in the database.
def test_auth_login_no_email(clear_users):
    with pytest.raises(InputError):
        auth_login_v2("name@gmail.com", "PASSWORD")
    
    result0 = auth_register_v2("name@gmail.com", "PASSWORD", "Name", "Name")
    result0_login = auth_login_v2("name@gmail.com", "PASSWORD")
    assert result0["auth_user_id"] == result0_login["auth_user_id"]

    with pytest.raises(InputError):
        auth_login_v2("name1@gmail.com", "PASSWORD")

# Tests logging in and registering combined.
def test_auth_register_login_valid(clear_users):
    result0 = auth_register_v2("zer0@gmail.com", "000000", "Zero", "Number")
    result1 = auth_register_v2("0n3@yahoo.com", "111111", "One", "Number")
    result2 = auth_register_v2("twoo@bigpond.net", "222222", "Two", "Number")
    result3 = auth_register_v2("thr33@hotmail.co", "333333", "Three", "Number")

    assert result0["auth_user_id"] == auth_login_v2("zer0@gmail.com", "000000")["auth_user_id"]
    assert result2["auth_user_id"] == auth_login_v2("twoo@bigpond.net", "222222")["auth_user_id"]
    assert result1["auth_user_id"] != auth_login_v2("thr33@hotmail.co", "333333")["auth_user_id"]
    assert result3["auth_user_id"] == auth_login_v2("thr33@hotmail.co", "333333")["auth_user_id"]

# Tests attempting to login with no registered users.
def test_auth_login_no_users(clear_users):
    with pytest.raises(InputError):
        auth_login_v2("first@gmail.com", "PASSWORD")
    
    with pytest.raises(InputError):
        auth_login_v2("youtube@gmail.com", "123456")
    
    with pytest.raises(InputError):
        auth_login_v2("name@gmail.com", "000000")

# Testing auth_logout_v1.
# Testing a successful logout for a user.
def test_auth_logout_valid_normal(clear_users):
    result = auth_register_v2("hello@gmail.com", "PASSWORD", "Hello", "Gmail")
    assert auth_logout_v1(result["token"])["is_success"]

# Testing a successful logout for a user that has logged in many times.
def test_auth_logout_valid_many_logins(clear_users):
    result0 = auth_register_v2("drstrange@mcu.com", "123456!", "Stephen", "Strange")
    auth_login_v2("drstrange@mcu.com", "123456!")
    auth_login_v2("drstrange@mcu.com", "123456!")
    auth_login_v2("drstrange@mcu.com", "123456!")
    auth_login_v2("drstrange@mcu.com", "123456!")

    assert auth_logout_v1(result0["token"])["is_success"]

# Testing a logout for a user that has already logged out.
def test_auth_logout_already_out(clear_users):
    result0 = auth_register_v2("marvel@yahoo.com", "000000", "Marvel", "Studios")
    assert auth_logout_v1(result0["token"])["is_success"]
    with pytest.raises(AccessError):
        auth_logout_v1(result0["token"])

# Testing multiple logins and outs.
def test_auth_logout_valid_multiple(clear_users):
    result0 = auth_register_v2("123456@numbers.com", "654321", "Numbers", "Inc")
    assert auth_logout_v1(result0["token"])["is_success"]

    result1 = auth_login_v2("123456@numbers.com", "654321")
    assert auth_logout_v1(result1["token"])["is_success"]

    result2 = auth_login_v2("123456@numbers.com", "654321")
    assert auth_logout_v1(result2["token"])["is_success"]

    result3 = auth_login_v2("123456@numbers.com", "654321")
    result4 = auth_login_v2("123456@numbers.com", "654321")
    result5 = auth_login_v2("123456@numbers.com", "654321")
    
    assert auth_logout_v1(result5["token"])["is_success"]
    assert auth_logout_v1(result4["token"])["is_success"]
    assert auth_logout_v1(result3["token"])["is_success"]

# Testing multiple users logging in and out.
def test_auth_logout_valid_multiple_users(clear_users):
    result0 = auth_register_v2("johnsmith@hotmail.com", "password", "John", "Smith")
    result1 = auth_register_v2("johnsmithy@hotmail.com", "password", "John", "Smith")
    result2 = auth_register_v2("johnsmith1@hotmail.com", "password", "John", "Smith")
    result3 = auth_register_v2("jonnysmith@hotmail.com", "password", "John", "Smith")

    assert auth_logout_v1(result0["token"])["is_success"]
    assert auth_logout_v1(result1["token"])["is_success"]
    assert auth_logout_v1(result2["token"])["is_success"]
    assert auth_logout_v1(result3["token"])["is_success"]

# Testing invalid user logging out.
def test_auth_logout_invalid_user(clear_users):
    with pytest.raises(AccessError):
        auth_logout_v1("randomtoken.random.random")
    
    with pytest.raises(AccessError):
        auth_logout_v1(token_encode({
            "auth_user_id": 100,
            "session_id": 100
        }))

# Attempts to log out a user unsuccessfully with the wrong session_id but
# correct user_id. 
def test_auth_logout_invalid_session(clear_users):
    result0 = auth_register_v2("johncena@wwe.com", "987654321", "John", "Cena")

    with pytest.raises(AccessError):
        auth_logout_v1(token_encode({
            "auth_user_id": result0["auth_user_id"],
            "session_id": -1
        }))

    assert auth_logout_v1(result0["token"])["is_success"]

# Testing auth_passwordreset_request_v1

def test_auth_passwordreset_request_return():
    assert auth_passwordreset_request_v1('cactuscomp1531@gmail.com') == {}
    
# Testing auth_passwordreset_reset_v1

def test_auth_passwordreset_invalid_code():
    reset_code = '0.2'
    password = '123456'
    with pytest.raises(InputError):
        auth_passwordreset_reset_v1(reset_code, password)

def test_auth_passwordreset_password_long():
    reset_code = reset['reset_code']
    password = '123'
    with pytest.raises(InputError):
        auth_passwordreset_reset_v1(reset_code, password)

    password = '12345'
    with pytest.raises(InputError):
        auth_passwordreset_reset_v1(reset_code, password)

def test_auth_passwordreset_return():
    reset_code = reset['reset_code']
    password = '123456'
    assert auth_passwordreset_reset_v1(reset_code, password) == {}
