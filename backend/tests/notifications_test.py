import pytest
from src.notifications import notifications_get_v1
from src.error import AccessError
from src.other import clear_v2
from src.dm import dm_create_v1, message_send_dm_v1
from src.auth import auth_register_v2
from src.message import message_send_v2, message_share_v1
from src.channels import channels_create_v2
from src.channel import channel_invite_v2

@pytest.fixture
def clear_users():
    clear_v2()

def test_notifications_get_v1_invalid_token(clear_users):
    with pytest.raises(AccessError):
        notifications_get_v1(232312312312124)

def test_notications_get_v1_one_notif(clear_users):
    owner_user = auth_register_v2("JohnCena@gmail.com", "Ucantseeme","John", "Cena")
    member_user = auth_register_v2("AhmedMoeydw@hotmail.com", "thearea", "Ahmed", "Moey")

    
    dm_create_v1(owner_user["token"], [member_user["auth_user_id"]])

    notif_list = notifications_get_v1(member_user["token"])

    notif_count = len(notif_list["notifications"])

    assert notif_count == 1

def test_notications_get_v1_twenty_notif(clear_users):
    owner_user = auth_register_v2("JohnCena@gmail.com", "Ucantseeme","John", "Cena")
    member_user = auth_register_v2("AhmedMoeydw@hotmail.com", "thearea", "Ahmed", "Moey")

    dm0 = dm_create_v1(owner_user["token"], [member_user["auth_user_id"]])
    i = 0
    while i < 21:
        message_send_dm_v1(owner_user["token"], dm0["dm_id"], "@ahmedmoey")
        i = i + 1
    
    notif_list = notifications_get_v1(member_user["token"])

    notif_count = len(notif_list["notifications"])

    assert notif_count == 20

def test_notifications_get_v1_notifications_message(clear_users):
    owner_user = auth_register_v2("JohnCena@gmail.com", "Ucantseeme","John", "Cena")
    member_user = auth_register_v2("AhmedMoeydw@hotmail.com", "thearea", "Ahmed", "Moey")

    dm_create_v1(owner_user["token"], [member_user["auth_user_id"]])
    
    notif_list = notifications_get_v1(member_user["token"])


    assert notif_list["notifications"][0]['notification_message'] == "johncena added you to ahmedmoey, johncena"

def test_notifications_get_v1_notifications_other_message(clear_users):

    owner_user = auth_register_v2("JohnCena@gmail.com", "Ucantseeme","John", "Cena")
    member_user = auth_register_v2("AhmedMoeydw@hotmail.com", "thearea", "Ahmed", "Moey")
    member_user2 = auth_register_v2("yelllamyg@hotmail.com", "liverpool", "Abdul", "Khan")

    channel0 = channels_create_v2(owner_user["token"], "Yesssirr", False)
    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user["auth_user_id"])
    channel_invite_v2(owner_user["token"], channel0["channel_id"], member_user2["auth_user_id"])

    dm0 = dm_create_v1(owner_user["token"], [member_user["auth_user_id"]])
    message_send_dm_v1(owner_user["token"], dm0["dm_id"], "@ahmedmoey where we going shisa")
   
    message_send_v2(owner_user["token"], channel0["channel_id"], "@ahmedmoey @abdulkhan WE HAVE HSP BOIS")
    message_send_v2(member_user["token"], channel0["channel_id"], "@johncena Wallah hurry")

    
    notif_list1 = notifications_get_v1(member_user["token"])
    notif_list2 = notifications_get_v1(owner_user["token"])
    notif_list3 = notifications_get_v1(member_user2["token"])

    owner_notif_count = len(notif_list2["notifications"])
    member1_notif_count = len(notif_list1["notifications"])
    member2_notif_count = len(notif_list3["notifications"])
    
    assert member1_notif_count == 4
    assert owner_notif_count == 1
    assert member2_notif_count == 2

