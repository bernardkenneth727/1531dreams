import pytest
from src.auth import auth_register_v2
from src.other import clear_v2
from src.error import AccessError, InputError
from src.json_edit import access_json, clear_json, add_to_json
from src.channel import channel_join_v2, channel_messages_v2
from src.channels import channels_create_v2
from src.standup import standup_start_v1, standup_send_v1, standup_active_v1
from datetime import datetime, timedelta, timezone 
import threading 
import time 

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2()
    clear_json()

@pytest.fixture
def RegisterUser():

    return auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")

@pytest.fixture 
def RegisterUser2():

    return auth_register_v2("jack.swoosh@gmail.com", "password2", "Jack", "Swoosh")

@pytest.fixture 
def RegisterUser3():

    return auth_register_v2("jacky.harlow@gmail.com", "password3", "Jacky", "Harlow")

@pytest.fixture
def channelsCreate(RegisterUser):

    return channels_create_v2(RegisterUser['token'], 'Teams1', 'True')

@pytest.fixture
def channelJoin(RegisterUser2, channelsCreate):

    channel_join_v2(RegisterUser2['token'], channelsCreate['channel_id'])



#standup start tests 
def test_standup_start_normal(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):

    time_finish = standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 2)  
    time_active = standup_active_v1(RegisterUser['token'], channelsCreate['channel_id'])

    assert time_finish['time_finish'] == time_active['time_finish']

    time.sleep(2.1)
    
def test_standup_start_channelInvalid(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    invalidChannel = channelsCreate['channel_id'] + 111

    with pytest.raises(InputError):
        assert standup_start_v1(RegisterUser['token'], invalidChannel, 2)
 
def test_standup_start_InvalidToken(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    invalidToken = RegisterUser['token'] + 'invalid'

    with pytest.raises(AccessError):
        assert standup_start_v1(invalidToken, channelsCreate['channel_id'], 2)

def test_standup_start_UserNotInChannel(Clearing, RegisterUser, RegisterUser2, RegisterUser3, channelsCreate, channelJoin):

    with pytest.raises(AccessError):
        assert standup_start_v1(RegisterUser3['token'], channelsCreate['channel_id'], 2)

def standup_start_activeStandupAlready_test(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    channel_id = channelsCreate['channel_id']
    standup_start_v1(RegisterUser['token'], channel_id, 5)

    with pytest.raises(InputError):
        assert standup_start_v1(RegisterUser2['token'], channel_id, 2)

    time.sleep(5.1)



#standup active tests 
def test_standup_active_normal(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    finish_time = standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 3)
    assert standup_active_v1(RegisterUser['token'], channelsCreate['channel_id']) == {
        "is_active": True,
        "time_finish": finish_time['time_finish'] 
    }

    time.sleep(3.1)
    assert standup_active_v1(RegisterUser['token'], channelsCreate['channel_id']) == {
        "is_active": False,
        "time_finish": None
    }

def test_standup_active_channelInvalid(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 3)
    invalidChannel = channelsCreate['channel_id'] + 111

    with pytest.raises(InputError):
        assert standup_active_v1(RegisterUser['token'], invalidChannel)

    time.sleep(3.5)


def test_standup_active_InvalidToken(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 3)
    invalidToken = RegisterUser['token'] + 'invalid'

    with pytest.raises(AccessError):
        assert standup_active_v1(invalidToken, channelsCreate['channel_id'])

    time.sleep(3.5)



#standup send tests
def test_standup_send_normal(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 5)

    message1 = "Hello, everyone!"
    message2 = "Bye, everyone!"

    standup_send_v1(RegisterUser['token'], channelsCreate['channel_id'], message1)
    standup_send_v1(RegisterUser2['token'], channelsCreate['channel_id'], message2)

    time.sleep(5.1)
    standup_message = channel_messages_v2(RegisterUser['token'], channelsCreate['channel_id'], 0)
    

    assert standup_message['messages'][0]['message'] == "johnsmith: Hello, everyone!\njackswoosh: Bye, everyone!" 
    
        
def test_standup_send_invalidChannel(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 1)

    invalidChannel = channelsCreate['channel_id'] + 111

    message1 = "Hello, everyone!"

    with pytest.raises(InputError):
        assert standup_send_v1(RegisterUser['token'], invalidChannel, message1)

    time.sleep(1.1)

def test_standup_send_InvalidToken(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 1)

    invalidToken = RegisterUser['token'] + 'invalid'
    message1 = "Hello, everyone!"

    with pytest.raises(AccessError):
        assert standup_send_v1(invalidToken, channelsCreate['channel_id'], message1)

    time.sleep(1.1)

def test_standup_send_UserNotInChannel(Clearing, RegisterUser, RegisterUser2, RegisterUser3, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 1)

    message1 = "Hello, everyone!"

    with pytest.raises(AccessError):
        assert standup_send_v1(RegisterUser3['token'], channelsCreate['channel_id'], message1)

    time.sleep(1.1)

def test_standup_send_MessageInvalid(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    standup_start_v1(RegisterUser['token'], channelsCreate['channel_id'], 1)

    message_invalid = "hi" * 1000

    with pytest.raises(InputError):
        assert standup_send_v1(RegisterUser['token'], channelsCreate['channel_id'], message_invalid)

    time.sleep(1.1)


def test_standup_send_standupInactive(Clearing, RegisterUser, RegisterUser2, channelsCreate, channelJoin):
    message1 = "Hello, everyone!"

    with pytest.raises(InputError):
        assert standup_send_v1(RegisterUser['token'], channelsCreate['channel_id'], message1)