import pytest
from src.other import search_v2, clear_v2
from src.channels import channels_create_v2
from src.message import message_send_v2
from src.error import InputError, AccessError
from src.dm import dm_create_v1, message_send_dm_v1
from src.auth import auth_register_v2
from src.secret import token_encode
import json

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2() 

@pytest.fixture
def SetName(Clearing):
    """
    creates a valid name to be used for testing clear via create channels
    and clears previous names
    """
    name = "Cacti"
    return name

@pytest.fixture
def Set_is_public(Clearing):
    """
    creates a valid boolean for the is_public parameter for channels create
    and clears previous booleans created
    """
    is_public = True
    return is_public

@pytest.fixture
def userCreate(Clearing):
    """
    creates a valid auth_user_id via the auth_register function to be used for channels create
    also adds a user to users and clears any ids that have been previously created
    """
    auth_user_id = auth_register_v2("john.smith@gmail.com", "password", "Jimmy", "Smith")
    return auth_user_id
    
@pytest.fixture
def channelsCreate(userCreate, SetName, Set_is_public):
    """
    creates a channel to be cleared later on to test the clear function
    and clears previous channels
    """
    channels_create_v2(userCreate["token"], SetName, Set_is_public)


def test_clear_v1(userCreate, channelsCreate):
    """
    First creates a user then a channel then uses the clear function and checks whether the user and the channel have
    been cleared. Messages and permissions will not have been created so it checks that these are still empty
    """
    clear_v2()
    with open('src/data.json', 'r+') as FILE:
        DATA = json.load(FILE)
    users = DATA['users']
    channels = DATA['channels']
    messages = DATA['messages']
    dms = DATA['dms']
    assert users == []
    assert channels == []
    assert messages == []
    assert dms == []

# Tests for Search function

@pytest.fixture
def clear():
    clear_v2()

@pytest.fixture
def user1(clear):
    return auth_register_v2("example@example.com", "password", "John", "Doe")

@pytest.fixture
def user2():
    return auth_register_v2("example2@example.com", "password", "Jane", "Doe")

@pytest.fixture
def channel(user1):
    return channels_create_v2(user1["token"], "channel1", True)

@pytest.fixture
def dm(user1, user2):
    return dm_create_v1(user1["token"], [user2["auth_user_id"]])

def test_search_return_type(clear, user1):  
    result = search_v2(user1["token"], "")

    assert type(result) is dict

def test_search_channel_message(clear, user1, channel):
    message = "this is a message"
    message_send_v2(user1["token"], channel["channel_id"], message)
    result = search_v2(user1["token"], "this")["messages"]

    for msg in result:
        assert msg["message"] == message

def test_search_dm_message(clear, user1, dm):
    message = "welcome and hello and goodbye"
    message_send_dm_v1(user1["token"], dm["dm_id"], message)
    result = search_v2(user1["token"], "lo")["messages"]

    for msg in result:
        assert msg["message"] == message

def test_search_both_message(clear, user1, channel, dm):
    message1 = "welcome and hello and goodbye"
    message2 = "this has been a great and sunny day"
    message_send_v2(user1["token"], channel["channel_id"], message1)
    message_send_dm_v1(user1["token"], dm["dm_id"], message2)
    result = search_v2(user1["token"], "and")["messages"]

    for msg in result:
        assert msg["message"] == message1 or message2

def test_search_query_too_long(clear, user1, dm):
    message = "h"
    search_over = "h" * 1001
    search_under = "h" * 999
    message_send_dm_v1(user1["token"], dm["dm_id"], message)

    with pytest.raises(InputError):
        search_v2(user1["token"], search_over)

    assert search_v2(user1["token"], search_under)["messages"] == []

def test_search_token_valid(clear, user1, dm):
    message = "today is sunny and 31"
    search = "d 3"
    message_send_dm_v1(user1["token"], dm["dm_id"], message)

    token_invalid = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })

    with pytest.raises(AccessError):
        search_v2(token_invalid, search)