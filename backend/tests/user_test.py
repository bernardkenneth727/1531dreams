import pytest
from src.auth import auth_register_v2
from src.other import clear_v2
from src.user import user_profile_v2, user_profile_setname_v2, user_profile_setemail_v2, user_profile_sethandle_v1, users_all_v1, users_stats_v1, user_profile_uploadphoto_v1
from src.error import AccessError, InputError
from src.channel import channel_invite_v2
from src.channels import channels_create_v2
from src.json_edit import access_json
from src.dm import dm_create_v1, message_send_dm_v1
from src.message import message_send_v2
from src import config

@pytest.fixture
def Clearing():
    """
    calls the clear function for every fixture onwards
    """
    clear_v2()

@pytest.fixture
def RegisterUser():

    return auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")

@pytest.fixture
def RegisterUser2():

    return auth_register_v2("jack.swish@gmail.com", "password2", "Jack", "Swish")


#user_profile tests below 

#testing user_profile to return user's own profile - should return user dictionary of the user sending token parameter
def test_user_profile_ownProfile(Clearing, RegisterUser):
    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"]) == {"user": 
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "john.smith@gmail.com",
            'name_first': "John",
            'name_last': "Smith",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
    }

#testing user_profile with invalid token - should return AccessError
def test_user_profile_invalidToken(Clearing):
    user = auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")
    invalidToken = user["token"] + "invalid"

    with pytest.raises(AccessError):
        user_profile_v2(invalidToken, user["auth_user_id"])

#testing user_profile with invalid u_id - should return InputError
def test_user_profile_invalidUserId(Clearing):
    user = auth_register_v2("john.smith@gmail.com", "password", "John", "Smith")
    invalid_u_id = user["auth_user_id"] + 1

    with pytest.raises(InputError):
        user_profile_v2(user["token"], invalid_u_id)

#testing user_profile to return profile of other user - should return user dictionary of the user associated with auth_user_id
def test_user_profile_OtherProfile(Clearing, RegisterUser, RegisterUser2):
    assert user_profile_v2(RegisterUser["token"], RegisterUser2["auth_user_id"]) == {"user": 
        {
            "u_id": RegisterUser2["auth_user_id"],
            "email": "jack.swish@gmail.com",
            'name_first': "Jack",
            'name_last': "Swish",
            "handle_str": "jackswish",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
    }




#user_profile_setname tests below 

#testing user_profile_setname_v2 with 1 registered user - should return user dictionary with new names 
def test_user_profile_setname(Clearing, RegisterUser):
    user_profile_setname_v2(RegisterUser["token"], "new_first", "new_last")
    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"]) == {"user": 
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "john.smith@gmail.com",
            'name_first': "new_first",
            'name_last': "new_last",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
    }

#testing user_profile_setname with invalid token - should return AccessError 
def test_user_profile_setname_invalidToken(Clearing, RegisterUser):
    invalidToken = RegisterUser["token"] + "invalid"

    with pytest.raises(AccessError):
        user_profile_setname_v2(invalidToken, "name_first", "name_last")

#testing user_profile_setname with invalid first name - should return InputError
def test_user_profile_setname_invalid_first_name(Clearing, RegisterUser):

    with pytest.raises(InputError):
        user_profile_setname_v2(RegisterUser["token"], "qwertyuioasdfghjzxcvbngfdggfggfgdfgdfgdgdggdfgdfgfggsgfgggsgdgdsd", "name_last")

#testing user_profile_setname with invalid last name - should return InputError
def test_user_profile_setname_invalid_last_name(Clearing, RegisterUser):

    with pytest.raises(InputError):
        user_profile_setname_v2(RegisterUser["token"], "name_first", "qwertyuioasdfghjzxcvbngfdggfggfgdfgdfgdgdggdfgdfgfggsgfgggsgdgdsd")





#user_profile_setemail tests below 

#testing user_setemail_v2 with 1 registered user - should return user dictionary with new email 
def test_user_profile_setemail(Clearing, RegisterUser):
    user_profile_setemail_v2(RegisterUser["token"], "new.email@gmail.com")
    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"]) == {"user":
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "new.email@gmail.com",
            'name_first': "John",
            'name_last': "Smith",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },
    } 

#testing user_profile_setemail with invalid token - should return AccessError
def test_user_profile_setemail_invalidToken(Clearing, RegisterUser):
    invalidToken = RegisterUser["token"] + "invalid"

    with pytest.raises(AccessError):
        user_profile_setemail_v2(invalidToken, "new.email@gmail.com")

#testing user_profile_setemail with invalid email - should return InputError
def test_user_profile_setemail_invalidEmail(Clearing, RegisterUser):

    with pytest.raises(InputError):
        user_profile_setemail_v2(RegisterUser["token"], "invalid.email.com")


#testing user_profile_setemail with email address being used by another user - should return InputError 
def test_user_profile_setemail_emailTaken(Clearing, RegisterUser, RegisterUser2):
    
    with pytest.raises(InputError):
        user_profile_setemail_v2(RegisterUser["token"], "jack.swish@gmail.com")







#user_profile_sethandle tests below 

#testing user_sethandle_v2 with 1 registered user - should return user dictionary with new handle 
def test_user_profile_sethandle(Clearing, RegisterUser):
    user_profile_sethandle_v1(RegisterUser["token"], "new_handle")
    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"]) == {"user": 
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "john.smith@gmail.com",
            'name_first': "John",
            'name_last': "Smith",
            "handle_str": "new_handle",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
    }

#testing user_profile_sethandle with invalid token - should return AccessError 
def test_user_profile_sethandle_invalidToken(Clearing, RegisterUser):
    invalidToken = RegisterUser["token"] + "invalid"

    with pytest.raises(AccessError):
        user_profile_sethandle_v1(invalidToken, "new_handle")

#testing user_profile_sethandle with invalid handle - should return InputError
def test_user_profile_sethandle_invalidHandle(Clearing, RegisterUser):

    with pytest.raises(InputError):
        user_profile_sethandle_v1(RegisterUser["token"], "invalidinvalidinvalid_handle")

#testing user_profile_sethandle with handle taken by other user - should return InputError
def test_user_profile_sethandle_handleTaken(Clearing, RegisterUser, RegisterUser2):
    
    with pytest.raises(InputError):
        user_profile_sethandle_v1(RegisterUser["token"], "jackswish")





#users_all tests below 

#testing users_all with 1 registered user - should return users dictionary containing a list with the registered user (1)
def test_users_all_1User(Clearing, RegisterUser):
    assert users_all_v1(RegisterUser["token"]) == {"users": [
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "john.smith@gmail.com",
            'name_first': "John",
            'name_last': "Smith",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
    ]}

#testing users_all with multiple users - should return users dictionary containing a list with the registered users (2)
def test_users_all_MultipleUsers(Clearing, RegisterUser, RegisterUser2):
    assert users_all_v1(RegisterUser["token"]) == {"users": [
        {
            "u_id": RegisterUser["auth_user_id"],
            "email": "john.smith@gmail.com",
            'name_first': "John",
            'name_last': "Smith",
            "handle_str": "johnsmith",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        },   
        {
            "u_id": RegisterUser2["auth_user_id"],
            "email": "jack.swish@gmail.com",
            'name_first': "Jack",
            'name_last': "Swish",
            "handle_str": "jackswish",
            "profile_img_url": f"{config.url}static/default_pic.jpg",
        }, 
    ]}

#testing users_all with invalid token - should return AccessError
def test_users_all_invalidToken(Clearing, RegisterUser):
    invalidToken = RegisterUser["token"] + "invalid"

    with pytest.raises(AccessError):
        users_all_v1(invalidToken)

# users_stats_v1 tests 
# Test 1 Check if the token is invalid
def test_users_stats_invalidToken(Clearing):
    with pytest.raises(AccessError):
        users_stats_v1(12312312312)
    
    with pytest.raises(AccessError):
        users_stats_v1("invalid")

# Test 2 Check that the number of channels is updated
def test_users_stats_channels(Clearing, RegisterUser):
    i = 0
    while i < 15:
        channels_create_v2(RegisterUser["token"], f"channel {i}", False)
        i = i + 1
    
    stats = users_stats_v1(RegisterUser["token"])
    
    assert stats["dreams_stats"]["channels_exist"][-1]["num_channels_exist"] == 15
    
# Test 3 Check that the number of dms is updated
def test_users_stats_dms(Clearing, RegisterUser):
    i = 0
    while i < 50:
        dm_create_v1(RegisterUser["token"],[])
        i = i + 1

    stats = users_stats_v1(RegisterUser["token"])

    assert stats["dreams_stats"]["dms_exist"][-1]["num_dms_exist"] == 50

# Test 4 Check that the number of messages is updated
def test_users_stats_messages(Clearing, RegisterUser, RegisterUser2):
    dm0 = dm_create_v1(RegisterUser["token"],[RegisterUser2["auth_user_id"]])
    channel0 = channels_create_v2(RegisterUser["token"], "CapTain", False)
    
    i = 0
    while i < 25:
        message_send_v2(RegisterUser["token"], channel0["channel_id"], f"random spam {i}")
        i = i + 1

    stats = users_stats_v1(RegisterUser["token"])
    assert stats["dreams_stats"]["messages_exist"][-1]["num_messages_exist"] == 25

    i = 0
    while i < 25:
        message_send_dm_v1(RegisterUser["token"], dm0["dm_id"], f"we are groot {i}")
        i = i + 1
    stats = users_stats_v1(RegisterUser["token"])
    assert stats["dreams_stats"]["messages_exist"][-1]["num_messages_exist"] == 50

# Test 5 Check that the utilization rate is correct
def test_users_stats_utilization_rate(Clearing, RegisterUser, RegisterUser2):
    i = 0
    while i < 15:
        channels_create_v2(RegisterUser["token"], f"channel {i}", False)
        i = i + 1
    
    stats = users_stats_v1(RegisterUser["token"])
    
    assert stats["dreams_stats"]["channels_exist"][-1]["num_channels_exist"] == 15

    i = 0
    while i < 20:
        dm_create_v1(RegisterUser["token"],[])
        i = i + 1

    stats = users_stats_v1(RegisterUser["token"])

    assert stats["dreams_stats"]["dms_exist"][-1]["num_dms_exist"] == 20

    auth_register_v2("coldescent@gmail.com", "dontleaveme", "Colde", "Scent")
    auth_register_v2("mrjohncena@gmail.com", "ucantcME", "John", "Cena")

    users_not_in_channel = 2
    total_users = 4
    assert stats["dreams_stats"]["utilization_rate"] == float((total_users - users_not_in_channel) / total_users)

# Jonathan Sinani
# Testing a user's profile is updated to the new uploaded image.
def test_user_profile_uploadphoto_valid_normal(Clearing, RegisterUser):
    user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", 0, 0, 256, 256)
    u_id = RegisterUser["auth_user_id"]
    profile_img_url = user_profile_v2(RegisterUser["token"], u_id)["user"]["profile_img_url"]
    assert profile_img_url != f"{config.url}static/default_pic.jpg"

# Testing a user changing their profile photo multiple times.
def test_user_profile_uploadphoto_valid_many(Clearing, RegisterUser):
    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"])["user"]["profile_img_url"] == f"{config.url}static/default_pic.jpg"

    user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", 0, 0, 256, 256)
    user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/qTBoGA6Mc.jpg", 0, 0, 400, 400)
    user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/image_gallery/23065.jpg", 0, 0, 300, 300)

    assert user_profile_v2(RegisterUser["token"], RegisterUser["auth_user_id"])["user"]["profile_img_url"] != f"{config.url}static/default_pic.jpg"

# Testing a user's profile uploading a photo where the img_url provided does not return a 200.
def test_user_profile_uploadphoto_invalid_url(Clearing, RegisterUser):
    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc123456789.jpg", 0, 0, 256, 256)

# Testing a user's profile uploading a photo where the dimensions are not within the image size (The image size is 960x640).
def test_user_profile_uploadphoto_invalid_dimensions(Clearing, RegisterUser):
    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", 0, 0, 1000, 1000)
    
    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", -10, -10, 100, 100)

    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", -10, 10, 20, 1000)

    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/5TRraKEgc.jpg", 250, 250, 100, 100)

# Testing a user's profile uploading a photo where the img_url provided is not a jpg.
def test_user_profile_uploadphoto_invalid_not_jpg(Clearing, RegisterUser):
    with pytest.raises(InputError):
        user_profile_uploadphoto_v1(RegisterUser["token"], "http://clipart-library.com/images/8T65aoEyc.png", 0, 0, 50, 50)

# Testing a user's profile uploading a photo where the token provided is invalid.
def test_user_profile_uploadphoto_invalid_token(Clearing, RegisterUser):
    with pytest.raises(AccessError):
        user_profile_uploadphoto_v1('hello', "http://clipart-library.com/images/5TRraKEgc.jpg", 0, 0, 256, 256)
