from src.json_edit import access_json
from src.exceptions import token_invalid_id_AccessError
import json


def notifications_get_v1(token):
    '''
    Function that get the users most recent 20 notifications and returns them as a 
    dictionary

    Arguments:
    token (string) - encoded string of the auth_user_id and session_id

    Exceptions:
    AccessError - An AccessError is thrown when the token passed in is not a valid id.
    
    Return: 
    {
        "notifications" : notif_list (list of notifications)
    }

    '''

    u_id = token_invalid_id_AccessError(token)
    users = access_json("users")

    for user in users:
        if user["u_id"] == u_id:
            max_notif = len(user["notifications"])
            if max_notif > 20:
                max_notif = 20
            user_notif = user["notifications"]

    notif_list = []
    for i in range(0, max_notif):
        notif_list.append(user_notif[i])

    return {
        "notifications" : notif_list
    }
