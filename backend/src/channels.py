from src.error import AccessError, InputError
import src.user
from src.json_edit import access_json, clear_json, add_to_json
from src.secret import token_encode, token_decode
import uuid
from src.exceptions import token_invalid_id_AccessError

def channels_create_v2(token, name, is_public):  
    """
    Creates a new channel when called and returns the channel id in a dictionary.
    The function throws an exception if the name is longer than 20 characters and if the 
    auth_user_id is invalid. Each channel is a dictionary stored in a list of dictionaries in a data file.
    This function creates a channel and appends it to the list in the data file
    
    Arguments: 
        token (int) - an encoded string that when decoded contains a dictionary with auth_user_id and session_id
        name (string) - the name of the channel 
        is_public (boolean) - True or False value to indicate whether the channel is public
    
    Exceptions: 
        The function throws an exception if the name is longer than 20 characters and if the 
        auth_user_id is invalid  

    Return Value:
        Returns the channel_id in a dictionary  
    """      
    auth_user_id = token_invalid_id_AccessError(token)

    channel_id = int(str(uuid.uuid1().int)[0:15])
    channel_name = name
    members_owner = [
        {
            "u_id": auth_user_id
        },
    ]
    members_all = [
        {
            "u_id": auth_user_id
        },
    ]
    standup = {
        "is_active": False, 
        "time_finish": None,
        'message_queue': ""
    }

    if len(name) > 20:
        raise InputError 
   
    user_is_valid = False
    # check auth_user_id is a valid user
    userS = access_json("users")
    tokenChecker = token_decode(token)
    for user in userS:
        if user["u_id"] == tokenChecker["auth_user_id"]:
            user_is_valid = True
    if not user_is_valid:
        raise AccessError    
    #if type(auth_user_id) is not int:
    #    raise AccessError
        
    newChannel = {
        "channel_id": channel_id,
        "name": channel_name,
        "members_owner": members_owner,
        "members_all": members_all,
        "is_public": str(is_public),
        "standup": standup,
    }
    add_to_json("channels", newChannel)
    src.user.users_stats_update(channels=True)
    #channels.append(newChannel)
    
    src.user.channel_no_finder(tokenChecker["auth_user_id"])
    src.user.involvement_rate_finder(tokenChecker["auth_user_id"])

    #channels.append(newChannel)
    return {
        'channel_id': channel_id,
    }

def channels_list_v2(token):
    '''
    The channels_list function provides a list of all channels that the authorised user is a part of providing information about the channel_id and channel name

    Arguments:
        token (string) - an encoded string that when decoded contains a dictionary with auth_user_id and session_id. 
        

    Exceptions:
        AccessError - Occurs when a matching token is not found in the data.json file after a function searches for a match in the channels list.

    Return Value: 
        Returns a channels dictionary containing the channel_id and channel name in the format: 
        {"channels": [
        {
            "channel_id": <int>,
            "name": <string>,
        },
        {
            "channel_id": <int>,
            "name": <string>,  
        },
        ]}
    '''

    #Checks for validity of auth_user_id
    auth_user_id = token_invalid_id_AccessError(token)

    #Creates channels list 
    channels_list = []

    #Loops to find where auth_user_id is in channels 
    channels = access_json("channels")
    for channel in channels: 
        for user in channel["members_all"]: 
            if auth_user_id == user["u_id"]: 
                channel_new = {}
                channel_new["channel_id"] = channel["channel_id"]
                channel_new["name"] = channel["name"]

                channels_list.append(channel_new)

    channels_list_dict = {"channels": channels_list} 

    #Returns new dictionary associated with auth_user_id
    return channels_list_dict



def channels_listall_v2(token):
    '''
    The channels_listall function provides a list of all channels in the database after validating the token of the user, providing the channel_id and name of all the channels. 

    Arguments:
        token (string) - an encoded string that when decoded contains a dictionary with auth_user_id and session_id. 

    Exceptions:
        AccessError - Occurs when a matching token is not found in the data.json file after a function searches for a match in the channels list.

    Return Value: 
        Returns a channels dictionary containing the channel_id and channel name of all the channels in the format: 
        {"channels": [
        {
            "channel_id": <int>,
            "name": <string>,
        },
        {
            "channel_id": <int>,
            "name": <string>,  
        },
        ]}
    '''
    #Checks for validity of auth_user_id
    token_invalid_id_AccessError(token)

    #Creates channels list 
    channels_list = []

    #Loops to find where auth_user_id is in channels 
    channels = access_json("channels")
    for channel in channels: 
        channel_new = {}
        channel_new["channel_id"] = channel["channel_id"]
        channel_new["name"] = channel["name"]

        channels_list.append(channel_new)

    channels_list_dict = {"channels": channels_list} 

    #Returns channels_list dictionary
    return channels_list_dict