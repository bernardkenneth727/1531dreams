import json

# Allows you to access elements in the key of the data.json and returns
# the data such as users, channels.
def access_json(key):
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)

    return DATA[key]

# Adds an element (that you provide) to the key in the data.json file and
# returns the entirety of the json file.
def add_to_json(key, dictionary):
    with open('src/data.json', 'r+') as FILE:
        DATA = json.load(FILE)

        DATA[key].append(dictionary)
    
    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)
    
    return DATA

# Clears the data.json file (temporary function)
def clear_json():
    with open('src/data.json', 'w') as FILE:
        DATA = {
            "users": [], 
            "channels": [], 
            "messages": [],
            "dms": [],
            "dreams_stats": {
                "channels_exist": [],
                "dms_exist": [], 
                "messages_exist": [], 
                "utilization_rate": 0.0
            }
            }
        json.dump(DATA, FILE)

# Removes a dictionary (that you provide) from the key in the data.json file
# and returns the entirety of the json file.
def remove_from_json(key, dictionary):
    with open('src/data.json', 'r+') as FILE:
        DATA = json.load(FILE)

        DATA[key].remove(dictionary)
    
    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)
    
    return DATA
