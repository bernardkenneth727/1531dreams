from src.error import AccessError, InputError
from src.json_edit import access_json, clear_json, add_to_json
from src.exceptions import token_invalid_id_AccessError, channel_id_invalid_InputError, auth_user_not_channel_member_AccessError, message_length_incorrect_InputError
import threading
from src.message import message_send_v2
import time
from datetime import datetime, timedelta, timezone
import json


def helper_standup_end(token, channel_id):
    """
    Helper function to end the standup that is started in standup_start_v1.
    """

    message_queue = ""

    with open('src/data.json', 'r') as FILE: 
        DATA = json.load(FILE)
        for channel in DATA["channels"]:
            if channel["channel_id"] == channel_id:
                message_queue = channel['standup']['message_queue']
                channel['standup']['message_queue'] = ""
                channel["standup"]['is_active'] = False

    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)

    message_send_v2(token, channel_id, message_queue)

def standup_active_v1(token, channel_id):
    """
    For a given channel, return whether a standup is active in it, and what time the standup finishes. 
    If no standup is active, then time_finish returns None
    
    Arguments:
        token (string) - the token of the user who wants request the function.
        channel_id (int) - the channel_id of the standup active status being returned.

     Exceptions:
        AccessError - Occurs when a matching token is not found in the data.json file after a function searches for a match in the users list.
        InputError - Occurs when the channel_id is not a valid channel

    Return Value:
        Returns {
            is_active: <boolean>    
            time finish: <int>
            }
    """
    #check for validity of token
    token_invalid_id_AccessError(token)

    #channel_id is not a valid channel (InputError)
    channel_id_invalid_InputError(channel_id)

    #loop through data file for active standup and return info 
    channels = access_json("channels")
    for channel in channels: 
        if channel['channel_id'] == channel_id:
            if channel['standup']['is_active']: 
                standup_active = {}
                standup_active["is_active"] = channel['standup']['is_active']
                standup_active["time_finish"] = channel['standup']['time_finish']

            else: 
                standup_active = {}
                standup_active["is_active"] = False
                standup_active["time_finish"] = None

    return standup_active 

def standup_start_v1(token, channel_id, length):
    """
    For a given channel, start the standup period whereby for the next "length" seconds if someone calls "standup_send" with a message, 
    it is buffered during the X second window then at the end of the X second window a message will be added to the message queue in the channel from the user who started the standup. 

    Arguments:
        token (string) - the token of the user who wants to start the standup.
        channel_id (int) - the channel_id of the standup to start within.
        length (int) - the length of the standup in seconds.

     Exceptions:
        AccessError - Occure when the authorised user is not in the given channel
        AccessError - Occurs when a matching token is not found in the data.json file after a function searches for a match in the users list.
        InputError - Occurs when the channel_id is not a valid channel
        InputError - Occurs when the an active standup is already running 

    Return Value:
        Returns {time finish: <int>}
    """

    #check for validity of token
    token_invalid_id_AccessError(token)

    #check for validity of channel 
    channel_id_invalid_InputError(channel_id)

    #check if user is in channel 
    auth_user_not_channel_member_AccessError(token, channel_id)

    #check if standup is already active (InputError)- need new exception function - loop through data.json file to see if standup is active 
    standup_status = standup_active_v1(token, channel_id)
    if standup_status['is_active']:
        raise InputError(description='An active standup is currently running in this channel')

    #add timenow to length and convert to unix timestamp
    dt = datetime.now(timezone.utc) + timedelta(seconds=int(length))
    time_finish_stamp = dt.replace(tzinfo=timezone.utc).timestamp()
    

    #open data.json and set to true and add time finished 
    with open('src/data.json', 'r') as FILE: 
        DATA = json.load(FILE)
        for channel in DATA["channels"]:
            if channel["channel_id"] == channel_id:
                channel["standup"]['is_active'] = True 
                channel["standup"]['time_finish'] = time_finish_stamp 

    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)


    #set timer and then send message_queue to channel from data.json and set status to false
    t = threading.Timer(length, helper_standup_end, [token, channel_id])
    t.start()

    
    #return the time_finish in a dictionary  
    return {
        'time_finish': time_finish_stamp
    }



def standup_send_v1(token, channel_id, message):
    """
    Sending a message to get buffered in the standup queue, assuming a standup is currently active
    
    Arguments:
        token (string) - the token of the user who wants to send the message in the standup.
        channel_id (int) - the channel_id of the standup the message is being sent to.
        message (string) - the message being sent to the standup. 

     Exceptions:
        AccessError - Occure when the authorised user is not in the given channel
        AccessError - Occurs when a matching token is not found in the data.json file after a function searches for a match in the users list.
        InputError - Occurs when the channel_id is not a valid channel
        InputError - Occurs when the the standup is not running in the channel. 
        InputError - Occurs when the message is longer than 1,000 characters. 

    Return Value:
        Returns {}
    """
    #check for validity of token
    u_id = token_invalid_id_AccessError(token)

    #channel_id is not a valid channel (InputError)
    channel_id_invalid_InputError(channel_id)

    #message is invalid (InputError)
    message_length_incorrect_InputError(message)

    #check if user is in channel (AccessError)
    auth_user_not_channel_member_AccessError(token, channel_id)

    #standup is inactive (InputError) - need new exception function - loop through data.json to see if standup is active
    standup_status = standup_active_v1(token, channel_id)
    if not standup_status['is_active']:
        raise InputError(description='An active standup is not currently running in this channel')


    formatted_message = ""

    users = access_json("users")
    for user in users:  
        if u_id == user["u_id"]: 
            handle = user["handle_str"]
 
    formatted_message = handle + ": " + message

    #add formatted message to data.json standup message_queue key
    with open('src/data.json', 'r') as FILE: 
        DATA = json.load(FILE)
        for channel in DATA["channels"]:
            if channel["channel_id"] == channel_id:
                if channel['standup']['message_queue'] == "":
                    channel['standup']['message_queue'] = formatted_message
                else:
                    channel['standup']['message_queue'] = channel['standup']['message_queue'] + '\n' + formatted_message

    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)