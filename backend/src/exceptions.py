import re
import hashlib
import imgspy
import urllib.request

from src.error import InputError, AccessError
from src.json_edit import access_json, add_to_json
from src.secret import token_encode, token_decode


def token_invalid_id_AccessError(token):
    """
    AccessError - An AccessError is thrown when the token passed in is not a valid id.
    
    Functions using this exception - all except auth/register, auth/login.

    Return Value:
    Returns the u_id of the user that's token has been passed in (if not raised InputError)
    """
    try:
        decoded_token = token_decode(token)
    except:
        raise AccessError(description='The token passed in is not a valid id.') from None
    users = access_json("users")
    id_found = False
    for user in users:
        if decoded_token["auth_user_id"] == user["u_id"]:
            for session in user["session_id"]:
                if decoded_token["session_id"] == session:
                    id_found = True
    
    if not id_found:
        raise AccessError(description='The token passed in is not a valid id.')
    
    return decoded_token["auth_user_id"]

def email_invalid_InputError(email):
    """
    InputError - Email entered is not a valid email.
    
    Functions using this exception - auth/login, auth/register, user/profile/setemail.
    """
    valid_email = '^[a-zA-Z0-9]+[\\._]?[a-zA-Z0-9]+[@]\\w+[.]\\w{2,3}$'
    if not re.search(valid_email, email):
        raise InputError(description='Email entered is not a valid email.')

def email_not_belonging_user_InputError(email):
    """
    InputError - Email entered does not belong to a user.
    
    Functions using this exception - auth/login.

    Return Value:
    Returns email_user_id which is a unique integer corresponding to the user's
    email provided (if not raised InputError).

    """
    users = access_json("users")
    email_user_found = False
    for user in users:
        if user["email"] == email:
            email_user_id = user["u_id"]
            email_user_found = True
    
    if not email_user_found:
        raise InputError(description='Email entered does not belong to a user.')
    
    return email_user_id

def password_incorrect_InputError(u_id, password):
    """
    InputError - Password is not correct.

    Functions using this exception - auth/login.
    """
    encrypted_password = hashlib.sha256(password.encode()).hexdigest()
    users = access_json("users")
    for user in users:
        if user["u_id"] == u_id and user["password"] != encrypted_password:
            raise InputError(description='Password is not correct.')

def email_repeat_InputError(email):
    """
    InputError - Email address is already being used by another user.

    Functions using this exception - auth/register, user/profile/setemail.
    """
    users = access_json("users")
    for user in users:
        if  user["email"] == email:
            raise InputError(description='Email address is already being used by another user.')

def password_length_incorrect_InputError(password):
    """
    InputError - Password entered is less than 6 characters long.

    Functions using this exception - auth/register.
    """
    if len(password) < 6:
        raise InputError(description='Password entered is less than 6 characters long.')

def name_length_incorrect_InputError(name_first, name_last):
    """
    InputError - name_first is not between 1 and 50 characters inclusively in length.
    InputError - name_last is not between 1 and 50 characters inclusively in length.

    Functions using this exception - auth/register, user/profile/setname.
    """
    if len(name_first) < 1 or len(name_first) > 50:
        raise InputError(description='name_first is not between 1 and 50 characters inclusively in length.')
    if len(name_last) < 1 or len(name_last) > 50:
        raise InputError(description='name_last is not between 1 and 50 characters inclusively in length.')

def channel_id_invalid_InputError(channel_id):
    """
    InputError - channel_id does not refer to a valid channel.

    Functions using this exception - channel/invite, channel/details, channel/messages, channel/join, channel/addowner, channel/removeowner, channel/leave.

    Return Values:
    Returns channel_dict, a dictionary of the channel (with the provided channel_id), if no InputError is raised.
    """
    channels = access_json("channels")
    channel_found = False
    for channel in channels:
        if channel["channel_id"] == channel_id:
            channel_found = True
            channel_dict = channel
    
    if not channel_found:
        raise InputError(description='channel_id does not refer to a valid channel.')
    else:
        return channel_dict

def user_id_invalid_InputError(u_id):
    """
    InputError - u_id does not refer to a valid user.

    Functions using this exception - channel/invite, dm/create, dm/invite, user/profile, admin/user/remove, admin/userpermission/change.
    """
    users = access_json("users")
    user_found = False
    for user in users:
        if user["u_id"] == u_id:
            user_found = True
    
    if not user_found:
        raise InputError(description='u_id does not refer to a valid user.')

def auth_user_not_channel_member_AccessError(token, channel_id):
    """
    AccessError - the authorised user is not a member of the channel with channel_id.

    Functions using this exception - channel/invite, channel/details, channel/messages, channel/leave, message/send.
    """
    auth_user_id = token_invalid_id_AccessError(token)
    channel = channel_id_invalid_InputError(channel_id)

    auth_user_found = False
    for user in channel["members_all"]:
        if user["u_id"] == auth_user_id:
                auth_user_found = True

    if not auth_user_found:
        raise AccessError(description='The authorised user is not a member of the channel with channel_id.')

def start_number_too_high_InputError(start, channel_id, dm_id):
    """
    Takes in channel_id or dm_id (if channel_id is -1).
    InputError - start is greater than the total number of messages in the channel.

    Functions using this exception - channel/messages, dm/messages.

    Return Value:
    Returns messages_in_channel - a list containing all the messages sent 
    within the channel (if not raised InputError).
    """
    messages = access_json("messages")
    if channel_id != -1:
        compare_channel_ids = True
    else:
        compare_channel_ids = False
    # Filter messages to which only those that are sent in channel.
    messages_in_channel = []
    for message in messages:
        if compare_channel_ids and message["channel_id"] == channel_id:
            messages_in_channel.append(message)
        if not compare_channel_ids and message["dm_id"] == dm_id:
            messages_in_channel.append(message)

    # Check start is not greater than last index of messages list.
    messages_length = len(messages_in_channel)
    if messages_length > 0:
        if start > messages_length - 1:
            raise InputError(description='start is greater than the total number of messages in the channel.')
    elif messages_length == 0:
        if start > messages_length:
            raise InputError(description='start is greater than the total number of messages in the channel.')
    
    return messages_in_channel

def is_user_global_owner(token):
    """
    Determines whether the user provided through the token is a global owner.

    Return Values:
        True if the user is a global owner
        False is the user is not a global owner
    """
    user_global_owner = False
    u_id = token_invalid_id_AccessError(token)
    users = access_json("users")

    for user in users:
        if user["u_id"] == u_id and user["permission_id"] == 1:
            user_global_owner = True
    
    return user_global_owner

def channel_is_private_AccessError(channel_id, token):
    """
    AccessError - channel_id refers to a channel that is private (when authorised user is not a global owner).
    
    Functions using this exception - channel/join.
    """
    is_public = False
    channel = channel_id_invalid_InputError(channel_id)
    if channel["is_public"] == "True":
        is_public = True

    if not is_public and not is_user_global_owner(token):
        raise AccessError(description='channel_id refers to a channel that is private (when authorised user is not a global owner).')

def user_already_owner_InputError(channel_id, u_id):
    """
    InputError - user with user id u_id is already an owner of the channel.

    Functions using this exception - channel/addowner.
    """
    already_owner = False
    channel = channel_id_invalid_InputError(channel_id)
    for user in channel["members_owner"]:
        if user["u_id"] == u_id:
            already_owner = True
    
    if already_owner:
        raise InputError(description='User with user id u_id is already an owner of the channel.')

def auth_user_not_owner_AccessError(token, channel_id):
    """
    AccessError - the authorised user is not an owner of the Dreams or an owner of this channel.

    Functions using this exception - channel/addowner, channel/removeowner.
    """
    auth_user_id = token_invalid_id_AccessError(token)
    channel = channel_id_invalid_InputError(channel_id)

    is_channel_owner = False
    for user in channel["members_owner"]:
        if user["u_id"] == auth_user_id:
            is_channel_owner = True
    
    if not is_channel_owner and not is_user_global_owner(token):
        raise AccessError(description='The authorised user is not an owner of the Dreams or an owner of this channel.')

def user_not_owner_InputError(u_id, channel_id):
    """
    InputError - when user with user id u_id is not an owner of the channel.

    Functions using this exception - channel/removeowner.
    """
    channel = channel_id_invalid_InputError(channel_id)

    is_channel_owner = False
    for user in channel["members_owner"]:
        if user["u_id"] == u_id:
            is_channel_owner = True
    
    if not is_channel_owner:
        raise InputError(description='User with user id u_id is not an owner of the channel.')

def user_only_channel_owner_InputError(u_id, channel_id):
    """
    InputError - the user is currently the only owner.

    Functions using this exception - channel/removeowner.
    """

    channel = channel_id_invalid_InputError(channel_id)

    if len(channel["members_owner"]) == 1:
        raise InputError(description='The user is currently the only owner.')

def message_length_incorrect_InputError(message):
    """
    InputError - message is more than 1000 characters.

    Functions using this exception - message/send, message/edit, message/senddm.
    """
    if len(message) > 1000:
        raise InputError(description='message is more than 1000 characters.')

def message_id_deleted_InputError(message_id):
    """
    InputError - message_id refers to a deleted message.

    Functions using this exception - message/edit, message/remove, message/pin.

    Return Value:
    Returns the message dictionary corresponding to the message_id found (if not InputError).
    """
    messages = access_json("messages")
    message_found = False
    for message in messages:
        if message["message_id"] == message_id:
            message_found = True
            message_dict = message

    
    if not message_found:
        raise InputError(description='message_id refers to a deleted message.')
    
    return message_dict

def dm_id_invalid_InputError(dm_id):
    """
    InputError - DM ID is not a valid DM.

    Functions using this exception - dm/details, dm/remove, dm/invite, dm/leave, dm/messages.

    Return Values:
    Returns the dm dictionary if the dm_id is valid (if not InputError).
    """
    dms = access_json("dms")
    dm_found = False
    for dm in dms:
        if dm["dm_id"] == dm_id:
            dm_found = True
            dm_dict = dm
    
    if not dm_found:
        raise InputError(description='DM ID is not a valid DM.')
    
    return dm_dict

def auth_user_not_dm_member_AccessError(token, dm_id):
    """
    AccessError - authorised user is not a member of this DM with dm_id.

    Functions using this exception - dm/details, dm/invite, dm/leave, dm/messages, message/senddm.
    """
    dm = dm_id_invalid_InputError(dm_id)
    auth_user_id = token_invalid_id_AccessError(token)
    is_dm_member = False
    
    for user in dm["members_all"]:
        if user["u_id"] == auth_user_id:
            is_dm_member = True
    
    if not is_dm_member:
        raise AccessError(description='Authorised user is not a member of this DM with dm_id.')

def auth_user_not_channel_dm_member_AccessError(token, channel_id, dm_id):
    """
    AccessError - the authorised user has not joined the channel or DM they are trying to share the message to.

    Functions using this exception - message/share.
    """
    if channel_id != -1:
        auth_user_not_channel_member_AccessError(token, channel_id)
    else:
        auth_user_not_dm_member_AccessError(token, dm_id)

def not_DM_creator_AccessError(token, dm_id):
    """
    AccessError - the user is not the original DM creator.

    Functions using this exception - dm/remove. 
    """
    dm = dm_id_invalid_InputError(dm_id)
    auth_user_id = token_invalid_id_AccessError(token)

    owner_found = False
    for user in dm["members_owner"]:
        if user["u_id"] == auth_user_id:
            owner_found = True
    
    if not owner_found:
        raise AccessError(description='The user is not the original DM creator.')

def auth_user_not_sent_message_AccessError(token, message_id):
    """
    Takes in channel_id or dm_id (if channel_id is -1).
    
    AccessError - none of the following are true:
        - Message with message_id was sent by the authorised user making this request.
        - The authorised user is an owner of this channel (if it was sent to a channel) or the Dreams.
    
    Functions using this exception - message/edit, message/remove.
    """
    message = message_id_deleted_InputError(message_id)
    u_id = token_invalid_id_AccessError(token)
    auth_user_found = False

    if message["channel_id"] != -1:
        channel = channel_id_invalid_InputError(message["channel_id"])
        for user in channel["members_owner"]:
            if user["u_id"] == u_id:
                auth_user_found = True

    if is_user_global_owner(token):
        auth_user_found = True

    if message["u_id"] != u_id and not auth_user_found:
        raise AccessError(description='None of the following are true: message with message_id was sent by authorised user, the authorised user is an owner of this channel or global owner.')

def handle_length_incorrect_InputError(handle_str):
    """
    InputError - handle_str is not between 3 and 20 characters inclusive.

    Functions using this exception - user/profile/sethandle. 
    """
    if len(handle_str) < 3 or len(handle_str) > 20:
        raise InputError(description='handle_str is not between 3 and 20 characters inclusive.')

def handle_repeat_InputError(handle_str):
    """
    InputError - handle is already used by another user.

    Functions using this exception - user/profile/sethandle. 
    """
    users = access_json("users")

    for user in users:
        if user["handle_str"] == handle_str:
            raise InputError(description='handle is already used by another user.')

def query_str_length_incorrect_InputError(query_str):
    """
    InputError - query_str is above 1000 characters.

    Functions using this exception - search.
    """
    if len(query_str) > 1000:
        raise InputError(description='query_str is above 1000 characters.')

def user_only_owner_InputError(u_id):
    """
    InputError - the user is currently the only owner.

    Functions using this exception - admin/user/remove.
    """
    users = access_json("users")
    owner_count = 0

    for user in users:
        if user["permission_id"] == 1:
            owner_count += 1
    
    if owner_count == 1:
        raise InputError(description='The user is currently the only owner.')

def auth_user_not_global_owner_AccessError(token):
    """
    AccessError - the authorised user is not an owner.

    Functions using this exception - admin/user/remove, admin/userpermission/change.
    """
    if not is_user_global_owner(token):
        raise AccessError(description='The authorised user is not an owner.')

def permission_id_not_valid(permission_id):
    """
    InputError - permission_id does not refer to a value permission.

    Functions using this exception - admin/userpermission/change.
    """
    if permission_id != 1 and permission_id != 2:
        raise InputError(description='permission_id does not refer to a value permission.')

def auth_user_react_not_member_AccessError(token, message_id):
    '''
    AccessError - the authorised user is not a member of the channel or DM  that the message is in 

    Functions using this exception are message_react_v1 and message_unreact_v2, message/pin

    '''
    messages = access_json("messages")
    channels = access_json("channels")
    dms = access_json("dms")

    u_id = token_invalid_id_AccessError(token)

    for message in messages:
        if message_id == message["message_id"]:
            tmp_channel_id = message["channel_id"]
            tmp_dm_id = message["dm_id"]

    auth_user_found = False
    
    # Checking member is in the channel  
    if tmp_channel_id != -1:
        for channel in channels:
            if tmp_channel_id == channel["channel_id"]:
                for user in channel["members_all"]:
                    if user["u_id"] == u_id:
                        auth_user_found = True

    # Checking member is in the dm
    if tmp_dm_id != -1:
        for dm in dms:
            if tmp_dm_id == dm["dm_id"]:
                for user in dm["members_all"]:
                    if user["u_id"] == u_id:
                        auth_user_found = True
                        
    if auth_user_found == False:
        raise AccessError(description='authorised user is not a member of the channel or dm')

def message_id_is_invalid_InputError(message_id):
    '''
    InputError - message_id is not a valid message within a channel or DM that the authorised user has joined

    Functions using this exception are message_react_v1 and message_unreact_v2

    '''
    messages = access_json("messages")

    message_found = False
    for message in messages:
        if message_id == message["message_id"]:
            message_found = True
    
    if not message_found:
        raise InputError(description='the message_id provided does not refer to any message')

def react_id_invalid_InputError(react_id):
    '''
    InputError - react_id is not a valid React ID. The only valid react ID the frontend has is 1

    Functions using this exception are message_react_v1 and message_unreact_v2

    '''
    if react_id != 1:
        raise InputError(description='the react_id is not 1')

def reacted_already_InputError(token, message_id, react_id):
    '''
    InputError - Message with ID message_id already contains an active React with ID react_id from the authorised user

    Functions using this exception are message_react_v1 and message_unreact_v2

    '''
    u_id = token_invalid_id_AccessError(token)
    messages = access_json("messages")
    for message in messages:
        if message_id == message["message_id"]:
            tmp_message = message

    for react in tmp_message["reacts"]:
        if react["react_id"] == react_id:
            if u_id in react["u_ids"]:
                raise InputError(description='you have already reacted to the message') 

def no_react_InputError(token, message_id, react_id):
    '''
    InputError - Message with ID message_id does not contain an active React with ID react_id from the authorised user

    Functions using this exception are message_react_v1 and message_unreact_v2

    '''
    u_id = token_invalid_id_AccessError(token)
    messages = access_json("messages")

    for message in messages:
        if message_id == message["message_id"]:
            tmp_message = message
    react_found = False
    for react in tmp_message["reacts"]:
        if react["react_id"] == react_id:
            if u_id in react["u_ids"]:
                react_found = True

    if react_found == False:
        raise InputError(description='there is not active react on this message') 

def auth_user_not_channel_dm_owner_AccessError(token, message_id):
    '''
    AccessError - The authorised user is not an owner of the channel or DM.

    Functions using this exception - message/pin, message/unpin.
    '''
    my_message = message_id_deleted_InputError(message_id)
    auth_user_id = token_invalid_id_AccessError(token)
    if my_message["channel_id"] != -1:
        channel = channel_id_invalid_InputError(my_message["channel_id"])
        is_channel_owner = False
        for user in channel["members_owner"]:
            if user["u_id"] == auth_user_id:
                is_channel_owner = True
        if not is_channel_owner:
            raise AccessError(description='The authorised user is not an owner of this channel.')
    else:
        not_DM_creator_AccessError(token, my_message["dm_id"])

def message_already_unpinned_pinned_InputError(message_id, pinned):
    '''
    InputError - Message with ID message_id is already pinned/unpinned.
    Arguments:
        pinned (bool) - if pinned is true, then testing if message is already pinned, otherwise testing if message is already unpinned.
    Functions using this exception - message/pin, message/unpin.
    '''
    my_message = message_id_deleted_InputError(message_id)
    # If the message is already pinned (for message/pin)
    if pinned and my_message["is_pinned"]:
        raise InputError(description='Message with ID message_id is already pinned.')
    # If the message is already unpinned (for message/unpin)
    elif not pinned and not my_message["is_pinned"]:
        raise InputError(description='Message with ID message_id is already unpinned.')
        
def img_url_not_success_InputError(img_url):
    '''
    InputError - img_url returns an HTTP status other than 200.

    Function using this exception - user/profile/uploadphoto.
    '''
    try:
        urllib.request.urlopen(img_url)
    except:
        raise InputError(description="img_url returns an HTTP status other than 200.") from None

def img_url_not_jpg_InputError(img_url):
    '''
    InputError - Image uploaded is not a JPG.
    
    Function using this exception - user/profile/uploadphoto.
    '''
    image = imgspy.info(img_url)
    image_type = image["type"]
    if image_type != 'jpg':
        raise InputError(description="Image uploaded is not a JPG.")

def img_dimensions_invalid_InputError(img_url, x_start, y_start, x_end, y_end):
    '''
    InputError - any of x_start, y_start, x_end, y_end are not within the dimensions of the image at the URL.
    
    Function using this exception - user/profile/uploadphoto.
    '''
    image = imgspy.info(img_url)
    dimension_x, dimension_y = image["width"], image["height"]
    if x_start < 0 or y_start < 0 or x_end > dimension_x or y_end > dimension_y or x_start > x_end or y_start > y_end:
        raise InputError(description="Any of x_start, y_start, x_end, y_end are not within the dimensions of the image at the URL.")
