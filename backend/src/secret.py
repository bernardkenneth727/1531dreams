import jwt

SECRET = "COMP1531CACTUS"

def token_encode(payload):
    return jwt.encode(payload, SECRET, algorithm='HS256')

def token_decode(encoded_jwt):
    return jwt.decode(encoded_jwt, SECRET, algorithms=['HS256'])
    
