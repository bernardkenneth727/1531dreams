from src.json_edit import access_json, clear_json, add_to_json
from src.secret import token_encode, token_decode
from src.exceptions import user_id_invalid_InputError, permission_id_not_valid, auth_user_not_global_owner_AccessError, user_only_owner_InputError, token_invalid_id_AccessError
from src.user import users_stats_update
import json

def admin_user_permission_change_v1(token, u_id, permission_id):
    """
    this function allows authorised users (dreams owners) to change their global permissions

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        u_id  - int    - unique identifier for each user
        permission_id - int - 1 for dreams owner and 2 for member
    
    Exceptions:
        InputError -u_id does not refer to a valid user
                   -permission_id does not refer to a value permission
        AccessError -The authorised user is not an owner

    Return Value:
        Returns nothing

    """
    
    token_invalid_id_AccessError(token)
    user_id_invalid_InputError(u_id)
    permission_id_not_valid(permission_id)
    auth_user_not_global_owner_AccessError(token)
    
    with open('src/data.json', 'r+') as FILE:
        DATA = json.load(FILE)
        users = DATA["users"]
        #counter = 0
        for user in users:  
            if user["u_id"] == u_id:
                user["permission_id"] = permission_id
                #DATA["users"][counter] = user
            #counter = counter + 1    
        DATA["users"] = users
    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)
    return {}

def admin_user_remove_v1(token, u_id):
    """
    this function allows dreams owners to remove users from the dreams

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        u_id  - int    - unique identifier for each user
    
    Exceptions:
        InputError -u_id does not refer to a valid user
                   -the user is currently the only owner
        AccessError -The authorised user is not an owner

    Return Value:
        Returns nothing

    """
    token_invalid_id_AccessError(token)
    auth_user_not_global_owner_AccessError(token)
    user_id_invalid_InputError(u_id)
    user_only_owner_InputError(u_id)

    with open('src/data.json', 'r+') as FILE:
        DATA = json.load(FILE)
        users = DATA["users"]
        messages = DATA["messages"]
        #dms = DATA["dms"]
        for user in users:
            if user["u_id"] == u_id:
                user["name_first"] = "Removed "
                user["name_last"] = "user"
                user["permission_id"] = 2

        for message in messages:
            if message["u_id"] == u_id:
                message["message"] = "Removed user"
        DATA["users"] = users
        DATA["messages"] = messages
        #DATA["dms"] = dms
    
    with open('src/data.json', 'w') as FILE:
        
        json.dump(DATA, FILE)
    
    users_stats_update()

    return {}
