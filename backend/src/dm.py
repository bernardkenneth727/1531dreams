from src.json_edit import remove_from_json, access_json, add_to_json
from src.secret import token_decode
from src.exceptions import user_id_invalid_InputError, token_invalid_id_AccessError,\
    not_DM_creator_AccessError, dm_id_invalid_InputError, message_length_incorrect_InputError,\
    auth_user_not_dm_member_AccessError, start_number_too_high_InputError
from src.notifications_helpers import notifications_tag, notifications_added
from src.user import users_stats_update, dm_part_finder, no_messages_sent, involvement_rate_finder
from datetime import datetime, timezone
import uuid
import json

# Lawson's dm functions

def dm_messages_v1(token, dm_id, start):
    '''
    Returns a dictionary with messages, start index and end index which are boundary of messages returned. Messages
    are returned based on start index and dm in which messages are sent of which user given is a member of.

    Arguments:
        token (string)  - an unique identifier for a user
        dm_id (int)     - an unique identifier for a dm
        start (int)     - an index which points to start of messages chronologically

    Exceptions:
        InputError  - Occurs when dm_id is invalid, token is invalid, start is greater
            than total number of messages in dm
        AccessError - Occurs when user is not a member of dm

    Returns:
        Returns messages
        Returns start
        Returns end
    '''

    u_id = token_invalid_id_AccessError(token)
    dm_id_invalid_InputError(dm_id)
    auth_user_not_dm_member_AccessError(token, dm_id)
    messages_in_dm = start_number_too_high_InputError(start, -1, dm_id)

    messages_length = len(messages_in_dm)

    # sort messages based on time in ascending order
    messages_sorted = sorted(messages_in_dm, key=lambda i: i["time_created"])

    # filter messages based on start index
    messages_filtered = messages_sorted[start:start + 50]

    # set end based on number of messages filtered
    end = start + len(messages_filtered)

    if end == messages_length:
        end = -1

    # add only properties specified in readme to list
    for msg in messages_filtered:
        del msg['channel_id']
        del msg['dm_id']

        for react in msg["reacts"]:
            if u_id in react["u_ids"]:
                react["is_this_user_reacted"] = True
            else:
                react["is_this_user_reacted"] = False

    return {
        "messages": messages_filtered,
        "start": start,
        "end": end
    }


def dm_remove_v1(token, dm_id):
    '''
    Removes an existing dm when authorised by the original creator of the dm.

    Arguments:
        token (string)  - an unique identifier for a user
        dm_id (int)    - an unique identifier for a dm

    Exceptions:
        InputError  - Occurs when dm_id does not refer to a valid dm
        AccessError - The user is not the original creator of the dm
    '''

    # check dm_id is valid, token is valid and user is owner of dm

    u_id = token_invalid_id_AccessError(token)

    dm_id_invalid_InputError(dm_id)

    not_DM_creator_AccessError(token, dm_id)

    # get list of dms from json

    dm_data = access_json("dms")

    # find dm in json and remove from json

    for dm in dm_data:
        if dm["dm_id"] == dm_id:
            remove_from_json("dms", dm)

    users_stats_update(dms=True)
    
    dm_part_finder(u_id)
    involvement_rate_finder(u_id)
    return {}


def dm_create_v1(token, u_ids):

    auth_user_id = token_invalid_id_AccessError(token)

    dm_id = int(str(uuid.uuid1().int)[0:15])
    dm_name = ""
    users_id = []

    # add owner to list of all members
    users_id.append({"u_id": auth_user_id})

    for u_id in u_ids:
        # check each u_id is valid
        user_id_invalid_InputError(u_id)

        # format each user id into a list of dictionaries
        users_id.append({"u_id": u_id})

    u_ids_all = u_ids
    u_ids_all.append(auth_user_id)

    users = access_json("users")
    handle_list = []

    for user in users:
        # check u_id is in list given
        if user["u_id"] in u_ids_all:
            # get handle for each user
            handle_list.append(user["handle_str"])

    # sort handles alphabetically
    handle_list_alpha = sorted(handle_list)

    # concat handles into a single string
    for handle in handle_list_alpha:
        if not dm_name:
            dm_name = handle
        else:
            dm_name = dm_name + ", " + handle

    entry = {
        "dm_id": dm_id,
        "dm_name": dm_name,
        "members_owner": [
            {
                "u_id": auth_user_id
            }
        ],
        "members_all": users_id
    }

    add_to_json("dms", entry)
    for u_id in u_ids:
        if u_id != auth_user_id:
            notifications_added(auth_user_id, -1, dm_id, u_id)
    
    users_stats_update(dms=True)
    for u_id in u_ids:
        dm_part_finder(u_id)
        involvement_rate_finder(u_id)
    
    return {
        "dm_id": dm_id,
        "dm_name": dm_name
    }


def message_send_dm_v1(token, dm_id, message):
    '''
    Sends a message to the dm specified when user is authorised.

    Arguments:
        token (string)      - an encoded string that when decoded contains a dictionary with
            auth_user_id and session_id.
        dm_id (int)         - an unique identifier for a dm
        message (string)    - a string od message to be sent to dm

    Exceptions:
        InputError  - Occurs when message is more than 1000 characters
        AccessError - Occurs when the authorised user is not a member of dm
        AccessError - the token passed in does not contain a valid u_id or session_id

    Return Value:
        Returns { message_id }
    '''

    # handle exceptions
    auth_user_id = token_invalid_id_AccessError(token)
    message_length_incorrect_InputError(message)
    auth_user_not_dm_member_AccessError(token, dm_id)

    # get new id for message
    message_id = int(str(uuid.uuid1().int)[0:15])

    # create unix timestamp
    dt = datetime.now(timezone.utc)
    time_created = dt.replace(tzinfo=timezone.utc).timestamp()

    entry = {
        "message_id": message_id,
        "message": message,
        "u_id": auth_user_id,
        "channel_id": -1,
        "dm_id": dm_id,
        "time_created": time_created,
        "reacts": [{"react_id": 1,
                    "u_ids": [],
                    }],
        "is_pinned": False
    }

    add_to_json("messages", entry)
    notifications_tag(auth_user_id, -1, dm_id, message)
    users_stats_update(messages=True)
    no_messages_sent(auth_user_id)
    involvement_rate_finder(auth_user_id)
    return {
        "message_id": message_id
    }


# Yoni's dm functions

def dm_details_v1(token, dm_id):
    """
    dm_details_v1 gives the user some basic information of the DM they
    requested (provided they are part of the dm).

    Arguments:
        token (string) - an encoded string containing a dicitonary with auth_user_id and session_id when decoded.
        dm_id (int) - an id of the existing dm used to identify it as different from other dms.

    Exceptions:
        InputError - Occurs when the dm_id does not refer to an existing DM.
        AccessError - Occurs when the token passed in is not valid or when the auth_user_id passed in through the token is not a member of the DM.

    Return Value:
        Returns a dictionary containing the name of the dm and a list of the members in the dm, with some basic information of them.
    """
    # Raises the exceptions specified in the specification for dm/details/v1
    # interface.
    token_invalid_id_AccessError(token)
    dm_id_invalid_InputError(dm_id)
    auth_user_not_dm_member_AccessError(token, dm_id)

    # Accesses the dm requested and gathers the dm name and all the user ids
    # for members in the dm.
    dms = access_json("dms")
    u_ids_list = []
    for dm in dms:
        if dm["dm_id"] == dm_id:
            dm_name = dm["dm_name"]
            for user in dm["members_all"]:
                u_ids_list.append(user["u_id"])

    # Acceses the users list and if the user is in the dm, adds their basic
    # information such as u_id, email, name, handle to the members_list to
    # return this list at the end.
    users = access_json("users")
    members_list = []
    for user in users:
        if user["u_id"] in u_ids_list:
            members_list.append({
                "u_id": user["u_id"],
                "email": user["email"],
                "name_first": user["name_first"],
                "name_last": user["name_last"],
                "handle_str": user["handle_str"],
                "profile_img_url": user["profile_img_url"]

            })

    return {
        "name": dm_name,
        "members": members_list
    }


def dm_list_v1(token):
    """
    dm_list_v1 returns a list of DMs that the token user is a member of.

    Arguments:
        token (string) - an encoded string containing a dicitonary with auth_user_id and session_id when decoded.

    Exceptions:
        AccessError - Occurs when the dm_id does not refer to an existing DM.

    Return Value:
        Returns a dictionary with the list of DMs that the token user is a member of when no exceptions are raised.
    """
    auth_user_id = token_invalid_id_AccessError(token)

    dms = access_json("dms")
    dm_list = []

    # Checks for each dm if the user is a member of it, and if so adds that
    # dm's contents (id and name) to the dm_list to be returned at the end.
    for dm in dms:
        for user in dm["members_all"]:
            if user["u_id"] == auth_user_id:
                dm_list.append({
                    "dm_id": dm["dm_id"],
                    "name": dm["dm_name"]
                })

    return {
        "dms": dm_list
    }


def add_or_remove_dm_member(dm_id, u_id, add):
    """
    Adds or removes a u_id in dm_member from the dm depending on whether it is
    set to add or not add, this updates on the data.json file as well.

    Arguments:
        dm_id (int) - an id of the existing dm used to identify it as different from other dms.
        u_id (int) - the id of the user to be added/removed to/from the DM.
        add (boolean) - if True, adds to the members_all, otherwise removes from members_all.
    """
    # Opens the data.json file and navigates to the members_all in the dm,
    # adding/removing the u_id to/from members_all, then writes the update
    # to the file.
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        for dm in DATA["dms"]:
            if dm["dm_id"] == dm_id:
                if add:
                    dm["members_all"].append({"u_id": u_id})
                else:
                    dm["members_all"].remove({"u_id": u_id})

    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)


def dm_invite_v1(token, dm_id, u_id):
    """
    dm_invite_v1 invites a user to an existing DM, automatically adding them
    to the DM group.

    Arguments:
        token (string) - an encoded string containing a dicitonary with auth_user_id and session_id when decoded.
        dm_id (int) - an id of the existing dm used to identify it as different from other dms.
        u_id (int) - the id of the user to be added to the DM.

    Exceptions:
        InputError - Occurs when the dm_id does not refer to an existing DM or when the u_id does not refer to an existing user.
        AccessError - Occurs when the token passed in is not valid or when the auth_user_id passed in through the token is not already a member of the DM.

    Return Value:
        {}
    """
    # Raises the exceptions specified in the specification for dm/invite/v1
    # interface.
    auth_user_id = token_invalid_id_AccessError(token)
    user_id_invalid_InputError(u_id)
    dm_id_invalid_InputError(dm_id)
    auth_user_not_dm_member_AccessError(token, dm_id)

    add_or_remove_dm_member(dm_id, u_id, True)
    notifications_added(auth_user_id, -1, dm_id, u_id)
    users_stats_update()
    dm_part_finder(auth_user_id)
    involvement_rate_finder(auth_user_id)
    return {}


def dm_leave_v1(token, dm_id):
    """
    dm_leave_v1 removes the user that has requested it from the dm as a member.

    Arguments:
        token (string) - an encoded string containing a dicitonary with auth_user_id and session_id when decoded.
        dm_id (int) - an id of the existing dm used to identify it as different from other dms.

    Exceptions:
        InputError - Occurs when the dm_id does not refer to an existing DM.
        AccessError - Occurs when the token passed in is not valid or when the auth_user_id passed in through the token is not already a member of the DM.

    Return Value:
        {}
    """
    # Raises the exceptions specified in the specification for dm/leave/v1
    # interface.
    auth_user_id = token_invalid_id_AccessError(token)
    dm_id_invalid_InputError(dm_id)
    auth_user_not_dm_member_AccessError(token, dm_id)

    add_or_remove_dm_member(dm_id, auth_user_id, False)
    users_stats_update()
    dm_part_finder(auth_user_id)
    involvement_rate_finder(auth_user_id)
    return {}
