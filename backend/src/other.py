from src.secret import token_decode
from src.json_edit import access_json
from src.channels import channels_list_v2
from src.json_edit import clear_json
from src.exceptions import token_invalid_id_AccessError, query_str_length_incorrect_InputError
from src.dm import dm_list_v1
from src import config
from src.user import users_stats_initialise
import os

def clear_user_profiles():
    """
    Clears all the user profile pictures stored in src/static aside from the default_pic.jpg.
    """
    # Checks to see all the photos in the src/static folder using the os.listdir function.
    # If the filename is not 'default_pic.jpg' (the default photo), then removes that image.
    profile_list = []
    if os.path.exists(f"src/static/"):
        profile_list = os.listdir("src/static/")
    for pic in profile_list:
        if pic != "default_pic.jpg":
            os.remove(f"src/static/{pic}")

def clear_v2():
    """
    This function clears all the data that is stored
    """
    clear_user_profiles()
    clear_json()
    users_stats_initialise()
    return {}

def search_v2(token, query_str):
    '''
    Returns a dictionary containing a list of messages that match a given query string in all
    messages that the user has joined.

    Arguments:
        token (string)      - an encoded string that when decoded contains a dictionary with
            auth_user_id and session_id.
        query_str (string)  - a string that is used to query for messages.

    Exceptions:
        InputError  - Occurs when query_str has above 1000 characters
        AccessError - the token passed in does not contain a valid u_id or session_id

    Return Value:
        Returns { messages }
    '''

    u_id = token_invalid_id_AccessError(token)

    query_str_length_incorrect_InputError(query_str)

    messages = access_json("messages")

    # list messages that match search
    list_search_match = []

    for elem in messages:
        if query_str in elem["message"]:
            list_search_match.append(elem)

    channels_joined = channels_list_v2(token)["channels"]
    
    channels_joined_ids = []
    for elem in channels_joined:
        channels_joined_ids.append(elem["channel_id"])

    # get all messages in dm user is part of that is match of search
    dms_joined = dm_list_v1(token)["dms"]

    dms_joined_ids = []
    for elem in dms_joined:
        dms_joined_ids.append(elem["dm_id"])

    # list messages that match search, sent in channel user is part of or sent in dm user is part of
    list_messages_verified = []

    for elem in list_search_match:
        if elem["channel_id"] in channels_joined_ids and elem["dm_id"] == -1:
            list_messages_verified.append(elem)
        elif elem["dm_id"] in dms_joined_ids and elem["channel_id"] == -1:
            list_messages_verified.append(elem)

    # delete channel_id and dm_id keys to match correct format
    for msg in list_messages_verified:
        del msg['channel_id']
        del msg['dm_id']

        for react in msg["reacts"]:
            if u_id in react["u_ids"]:
                react["is_this_user_reacted"] = True
            else:
                react["is_this_user_reacted"] = False

    return {
        "messages": list_messages_verified
    }