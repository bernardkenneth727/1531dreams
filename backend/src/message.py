from src.json_edit import add_to_json, remove_from_json, access_json
from datetime import datetime, timezone, timedelta
from src.secret import token_decode
from src.exceptions import message_length_incorrect_InputError, message_id_deleted_InputError, auth_user_not_channel_member_AccessError, auth_user_not_sent_message_AccessError, token_invalid_id_AccessError, auth_user_not_channel_dm_member_AccessError, auth_user_react_not_member_AccessError, message_id_is_invalid_InputError, react_id_invalid_InputError, reacted_already_InputError, message_already_unpinned_pinned_InputError, auth_user_not_channel_dm_owner_AccessError, no_react_InputError, channel_id_invalid_InputError, auth_user_not_channel_member_AccessError, auth_user_not_dm_member_AccessError, dm_id_invalid_InputError
from src.user import users_stats_update, no_messages_sent, involvement_rate_finder
import threading
import time
import uuid
import json
from src.error import InputError, AccessError
from src.notifications_helpers import notifications_tag, notifications_react
from src.dm import message_send_dm_v1

message_send_later_output = 0
def message_send_v2(token, channel_id, message):
    """
    this function allows authorised users to send messages

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        channel_id  - int - unique identifier for each channel
        message - string - the message being sent
    
    Exceptions:
        InputError -message is more than 1000 characters
        AccessError- The authorised user has not joined the channel

    Return Value:
        Returns {message_id} if no errors

    """
    token_invalid_id_AccessError(token)
    token_dict = token_decode(token)
    now = datetime.now(timezone.utc)
    timestamp = now.replace(tzinfo=timezone.utc).timestamp()
    message_id = int(str(uuid.uuid1().int)[0:15])
    
    global message_send_later_output
    message_send_later_output = message_id
    message_being_added = {
        "message_id": message_id,
        "message": message,
        "u_id": token_dict["auth_user_id"],
        "channel_id": channel_id,
        "dm_id": -1,
        "time_created": timestamp,
        "reacts" : [{"react_id" : 1,
                    "u_ids" : [],
                    }],
        "is_pinned" : False
    }
    message_length_incorrect_InputError(message)
    auth_user_not_channel_member_AccessError(token, channel_id)
    
    add_to_json("messages", message_being_added)
    
    notifications_tag(token_dict["auth_user_id"], channel_id, -1, message)
    users_stats_update(messages=True)
    no_messages_sent(token_dict["auth_user_id"])
    involvement_rate_finder(token_dict["auth_user_id"])
    return {
        "message_id": message_send_later_output
    }

def message_remove_v1(token, message_id):
    """
    this function allows authorised users to remove messages

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        message_id  - int - unique identifier for each message
        
    Exceptions:
        InputError -message no longer exists
        AccessError - Message with message_id was sent by the authorised user making this request
                    -The authorised user is an owner of this channel (if it was sent to a channel) or the **Dreams**

    Return Value:
        Returns {}

    """
    u_id = token_invalid_id_AccessError(token)
    #dict_to_remove is a list of 'messages' dictionaries
    messages = access_json("messages")
    for message in messages:
        if message["message_id"] == message_id:
            dict_to_remove = message

    message_id_deleted_InputError(message_id)
    auth_user_not_sent_message_AccessError(token, message_id)
    
    remove_from_json("messages", dict_to_remove)
    users_stats_update(messages=True)
    no_messages_sent(u_id)
    involvement_rate_finder(u_id)
    return {
    }

def message_edit_v2(token, message_id, message):
    """
    this function allows authorised users to edit messages and deletes messages if edit is empty

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        message_id  - int - unique identifier for each message
        message - string - the message you want to replace the original message with
        
    Exceptions:
        InputError -Length of message is over 1000 characters
                   - message_id refers to a deleted message
        AccessError - Message with message_id was sent by the authorised user making this request
                    -The authorised user is an owner of this channel (if it was sent to a channel) or the **Dreams**

    Return Value:
        Returns {}

    """
    auth_user_id = token_invalid_id_AccessError(token)
    auth_user_not_sent_message_AccessError(token, message_id)
    message_length_incorrect_InputError(message)
    message_id_deleted_InputError(message_id)
    found = False
    messages = access_json("messages")
    for message_dict in messages:
        if message_dict["message_id"] == message_id:
            found = True
            if not message == "":
                message_dict["message"] = message
            else:
                messages.remove(message_dict)
                remove_from_json("messages", message_dict)
                users_stats_update(messages=True)
                no_messages_sent(auth_user_id)
    if found is not True:
        raise InputError(description='message_id refers to a deleted message.')
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        DATA["messages"] = messages
    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)
    notifications_tag(auth_user_id, message_dict["channel_id"], message_dict["dm_id"], message)
    return {
    }

###################### LAWRENCE LEUNG ######################

def message_share_v1(token, og_message_id, message, channel_id, dm_id):
    '''
    og_message_id is the original message. channel_id is the channel that the message is being shared to, 
    and is -1 if it is being sent to a DM. dm_id is the DM that the message is being shared to, and is -1 
    if it is being sent to a channel.
    message is the optional message in addition to the shared message, 
    and will be an empty string '' if no message is given
 
    Arguments: 
        token   (string) - allows you to check sessions and also confirm that the correct person is making the changes
        og_message_id  (int) - unique identifier for each message
        message (string) - the message you want to add to the original message 
        channel_id (integer) - an integer created by a channel_create function channel_id 
        dm_id (int) - an id of the existing dm used to identify it as different from other dms.
    Exceptions:
        AccessError - An AccessError is thrown when the token passed in is not a valid id.
        AccessError - the authorised user has not joined the channel or DM they are trying to share the message to.
    Return: 
        {
            "shared_message_id" : shared_message_id (a dictionary the shared message_id)
        }
 
    '''
    auth_user_id = token_invalid_id_AccessError(token)
    auth_user_not_channel_dm_member_AccessError(token, channel_id, dm_id)
    
    messages = access_json("messages")
    # channel_id is -1 if the message is being sent to a dm

    # dm_id is -1 if the message is being sent to the channel
    og_message = message

    # create new message id
    shared_message_id = int(str(uuid.uuid1().int)[0:15])

    # create unix timestamp
    dt = datetime.now(timezone.utc)
    time_created = dt.replace(tzinfo=timezone.utc).timestamp()

    for message in messages:
        # Find the message we want to share
        if message["message_id"] == og_message_id:
            tmp_message = message["message"]
    
    entry = {
        "message_id" : shared_message_id,
        "message" : f"{og_message} {tmp_message}",
        "u_id" : auth_user_id,
        "channel_id" : channel_id,
        "dm_id" : dm_id,
        "time_created" : time_created,
        "reacts" : [{"react_id" : 1,
                    "u_ids" : [],
                    }],
        "is_pinned" : False
    }
    
    add_to_json("messages", entry)
    notifications_tag(auth_user_id, channel_id, dm_id, og_message)
    users_stats_update(messages=True)
    no_messages_sent(auth_user_id)
    involvement_rate_finder(auth_user_id)
    return {
        "shared_message_id" : shared_message_id
    }

def message_react_v1(token, message_id, react_id):
    
    '''
    Given a specific message_id and token, and a valid react_id of 1, the auth user is added
    to the u_ids list in the reacts dictionary.

    Arguments: 
        token (string) - an encoded string that when decoded contains a dictionary with
        auth_user_id and session_id.
        message_id (integer) - unique identifier for each message
        react_id (integer) - a integer of 1 indicates a thumbs up react
    
    Exceptions:
        InputError - message_id is not a valid message within a channel or DM that the authorised user has joined
        InputError - react_id is not a valid React ID. The only valid react ID the frontend has is 1
        InputError -  Message with ID message_id already contains an active React with ID react_id from the authorised user
        
        AccessError - The authorised user is not a member of the channel or DM that the message is within
        AccessError - The token provided is invalid
    
    Returns:
        {}
    '''

    auth_user_id = token_invalid_id_AccessError(token)
    
    message_id_is_invalid_InputError(message_id)
    react_id_invalid_InputError(react_id)
    reacted_already_InputError(token, message_id, react_id)
    auth_user_react_not_member_AccessError(token, message_id)
    
    messages = access_json("messages")
    
    for message in messages:
        if message_id == message["message_id"]:
            tmp_message = message

    for react in tmp_message["reacts"]:
        if react["react_id"] == react_id:
            react["u_ids"].append(auth_user_id)
    
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        DATA["messages"] = messages

        with open('src/data.json', 'w') as FILE:
            json.dump(DATA, FILE)

    notifications_react(auth_user_id, tmp_message)
    
    return {
        
    }

def message_unreact_v1(token, message_id, react_id):
    '''
    Given a specific message_id and token, and a valid react_id of 1, the auth user is removed
    to the u_ids list in the reacts dictionary.

    Arguments: 
        token (string) - an encoded string that when decoded contains a dictionary with
        auth_user_id and session_id.
        message_id (integer) - unique identifier for each message
        react_id (integer) - a integer of 1 indicates a thumbs up react
    
    Exceptions:
        InputError - message_id is not a valid message within a channel or DM that the authorised user has joined
        InputError - react_id is not a valid React ID. The only valid react ID the frontend has is 1
        InputError - Message with ID message_id already contains an active React with ID react_id from the authorised user
        
        AccessError - The authorised user is not a member of the channel or DM that the message is within
        AccessError - The token provided is invalid
    
    Returns:
        {}
    '''
    auth_user_id = token_invalid_id_AccessError(token)
    message_id_is_invalid_InputError(message_id)
    react_id_invalid_InputError(react_id)
    auth_user_react_not_member_AccessError(token, message_id)
    no_react_InputError(token, message_id, react_id)

    messages = access_json("messages")

    for message in messages:
        if message_id == message["message_id"]:
            tmp_message = message

    for react in tmp_message["reacts"]:
        if react["react_id"] == react_id:
            react["u_ids"].remove(auth_user_id)

    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        DATA["messages"] = messages

    with open('src/data.json', 'w') as FILE:
        json.dump(DATA, FILE)

    return {
        
    }

# Jonathan Sinani
def message_pin_v1(token, message_id):
    """
    Marks a message as pinned, so that it is shown in front of other messages.

    Arguments:
        token (string) - a string that is tamper proof with the user's session_id and auth_user_id when decoded.
        message_id (int) - an integer that is the unique id of the message to be pinned.
    
    Exceptions:
        InputError - Occurs when message_id is not a valid message, message with message_id is already pinned
        AccessError - Occurs when the authorised user is not a member of the channel or DM that the message is within or the authorised user is not an owner of the channel or DM that the message is within.
    
    Return Value:
        {}
    """
    token_invalid_id_AccessError(token)
    message_id_deleted_InputError(message_id)
    auth_user_react_not_member_AccessError(token, message_id)
    auth_user_not_channel_dm_owner_AccessError(token, message_id)
    message_already_unpinned_pinned_InputError(message_id, True)

    messages = access_json("messages")
    for message in messages:
        if message["message_id"] == message_id:
            message["is_pinned"] = True
    
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        DATA["messages"] = messages

        with open('src/data.json', 'w') as FILE:
            json.dump(DATA, FILE)

    return {}

def message_unpin_v1(token, message_id):
    """
    Marks a message as unpinned, so that it removed from being shown in front of other messages.

    Arguments:
        token (string) - a string that is tamper proof with the user's session_id and auth_user_id when decoded.
        message_id (int) - an integer that is the unique id of the message to be unpinned.
    
    Exceptions:
        InputError - Occurs when message_id is not a valid message, message with message_id is already unpinned
        AccessError - Occurs when the authorised user is not a member of the channel or DM that the message is within or the authorised user is not an owner of the channel or DM that the message is within.
    
    Return Value:
        {}
    """
    token_invalid_id_AccessError(token)
    message_id_deleted_InputError(message_id)
    auth_user_react_not_member_AccessError(token, message_id)
    auth_user_not_channel_dm_owner_AccessError(token, message_id)
    message_already_unpinned_pinned_InputError(message_id, False)

    messages = access_json("messages")
    for message in messages:
        if message["message_id"] == message_id:
            message["is_pinned"] = False
    
    with open('src/data.json', 'r') as FILE:
        DATA = json.load(FILE)
        DATA["messages"] = messages

        with open('src/data.json', 'w') as FILE:
            json.dump(DATA, FILE)

    return {}

def message_sendlater_v1(token, channel_id, message, time_sent):
    """
    this function allows authorised users to send messages with a time delay

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        channel_id  - int - unique identifier for each channel
        message - string - the message being sent
        time_sent - the time in seconds to come that the message is to be sent in
    
    Exceptions:
        InputError  -message is more than 1000 characters
                    - channel ID is not a valid channel
                    - Time sent is a time in the past
        AccessError- The authorised user has not joined the channel

    Return Value:
        Returns {message_id} if no errors

    """
    token_invalid_id_AccessError(token)
    message_length_incorrect_InputError(message)
    auth_user_not_channel_member_AccessError(token, channel_id)
    
    now = datetime.now(timezone.utc)
    timeNow = now.replace(tzinfo=timezone.utc).timestamp()
    channel_id_invalid_InputError(channel_id)
    if (time_sent - timeNow) < 0:
        raise InputError(description='Time sent is a time in the past.')
    
    timeSent = datetime.fromtimestamp(time_sent, timezone.utc)
    
    naive = timeSent.replace(tzinfo=None)
    naive_now = now.replace(tzinfo=None)
    time_difference = naive - naive_now
    time_difference_seconds = time_difference.total_seconds()
    t = threading.Timer(time_difference_seconds, message_send_v2,[token, channel_id, message])
    t.start()

    return {"message_id": message_send_later_output}

def message_sendlaterdm_v1(token, dm_id, message, time_sent):
    """
    this function allows authorised users to send messages via dm with a time delay

    Arguments: 
        token - string - allows you to check sessions and also confirm that the correct person is making the changes
        dm_id  - int - unique identifier for each dm
        message - string - the message being sent
        time_sent - the time in seconds to come that the message is to be sent in
    
    Exceptions:
        InputError  - message is more than 1000 characters
                    - dm ID is not a valid channel
                    - Time sent is a time in the past
        AccessError- The authorised user has not joined the dm

    Return Value:
        Returns {message_id} if no errors

    """
    message_length_incorrect_InputError(message)
    dm_id_invalid_InputError(dm_id)
    auth_user_not_dm_member_AccessError(token, dm_id)

    now = datetime.now(timezone.utc)
    timeNow = now.replace(tzinfo=timezone.utc).timestamp()
    
    if (time_sent - timeNow) < 0:
        raise InputError(description='Time sent is a time in the past.')
    
    timeSent = datetime.fromtimestamp(time_sent, timezone.utc)
    
    naive = timeSent.replace(tzinfo=None)
    naive_now = now.replace(tzinfo=None)
    time_difference = naive - naive_now
    time_difference_seconds = time_difference.total_seconds()
    t = threading.Timer(time_difference_seconds, message_send_dm_v1,[token, dm_id, message])
    t.start()

    return {"message_id": message_send_later_output}