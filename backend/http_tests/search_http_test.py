import pytest
import requests
import json
from src import config
from src.secret import token_encode


@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1(clear):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def channel(user1):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': user1["token"],
        'name': 'channel1',
        'is_public': True
    })
    return json.loads(resp.text)


@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)


def test_search_return_type(clear, user1):
    resp = requests.get(config.url + 'search/v2', params={
        'token': user1['token'],
        'query_str': ''
    })
    payload = json.loads(resp.text)
    assert type(payload) is dict


def test_search_channel_message(clear, user1, channel):
    message = "this is a message"
    requests.post(config.url + 'message/send/v2', json={
        'token': user1['token'],
        'channel_id': channel['channel_id'],
        'message': message
    })

    resp = requests.get(config.url + 'search/v2', params={
        'token': user1['token'],
        'query_str': 'this'
    })

    payload_messages = json.loads(resp.text)['messages']

    for msg in payload_messages:
        assert msg["message"] == message


def test_search_query_too_long(clear, user1, dm):
    message = "h"
    search_over = "h" * 1001
    search_under = "h" * 999
    requests.post(config.url + 'message/senddm/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'message': message
    })

    assert requests.get(config.url + 'search/v2', params={
        'token': user1['token'],
        'query_str': search_over
    }).status_code == 400

    resp_messages = requests.get(config.url + 'search/v2', params={
        'token': user1['token'],
        'query_str': search_under
    })

    payload = json.loads(resp_messages.text)["messages"]

    assert payload == []


def test_search_token_valid(clear, user1, dm):
    message = "today is sunny and 31"
    search = "d 3"
    requests.post(config.url + 'message/senddm/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'message': message
    })

    token_invalid = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })

    assert requests.get(config.url + 'search/v2', params={
        'token': token_invalid,
        'query_str': search
    }).status_code == 403
