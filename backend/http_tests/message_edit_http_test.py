import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_http_invalid_token(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #makes channel id
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)

    #print(token)
    message_id_maker = requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "validMesgse"
    })
    mesage_id = json.loads(message_id_maker.text)

    assert requests.put(config.url + 'message/edit/v2', json = {
        'token': 'invalidToken',
        'message_id': mesage_id['message_id'],
        'message': 'valid Message'
    }).status_code == 403

def test_http_msg_too_long(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #makes channel id
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)

    #print(token)
    message_id_maker = requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "validMesgse"
    })
    mesage_id = json.loads(message_id_maker.text)

    assert requests.put(config.url + 'message/edit/v2', json = {
        'token': token['token'],
        'message_id': mesage_id['message_id'],
        'message': 'invalid Message'*500
    }).status_code == 400

def test_http_deleted_msg(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #makes channel id
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)

    #print(token)
    message_id_maker = requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "validMesgse"
    })
    mesage_id = json.loads(message_id_maker.text)

    requests.delete(config.url + 'message/remove/v1', json = {
        'token': token['token'],
        'message_id': mesage_id["message_id"]
    })

    assert requests.put(config.url + 'message/edit/v2', json = {
        'token': token['token'],
        'message_id': mesage_id['message_id'],
        'message': 'valid Message'
    }).status_code == 400

def test_http_user_not_owner_of_channel_or_dreams(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #makes channel id
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)

    #print(token)
    message_id_maker = requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "validMesgse"
    })
    mesage_id = json.loads(message_id_maker.text)

    #create new user who does not own channel and did not send msg
    token_maker2 = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'ema3iltovalid@gmail.com',
        "password": 'va3lisdpass',
        "name_first": "Ji3mfmy",
        "name_last": "Ba3rnes"
    })
    token2 = json.loads(token_maker2.text)

    assert requests.put(config.url + 'message/edit/v2', json = {
        'token': token2['token'],
        'message_id': mesage_id['message_id'],
        'message': 'valid Message'
    }).status_code == 403

def test_http_normal(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #makes channel id
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)

    #print(token)
    message_id_maker = requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "validMesgse"
    })
    mesage_id = json.loads(message_id_maker.text)

    requests.put(config.url + 'message/edit/v2', json = {
        'token': token['token'],
        'message_id': mesage_id['message_id'],
        'message': 'valid Message'
    })
    search_result = requests.get(config.url + 'search/v2', json = {
        'token': token['token'],
        'query_str': "valid Message"
    })
    search_actual_result = json.loads(search_result.text)
    assert not search_actual_result == {}

