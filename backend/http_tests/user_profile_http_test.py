import pytest
import requests
import json
from src import config

# Basic fixture to clear the data (just needed for clearing the users list), will change to clear method.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)


def test_http_user_profile_normal(clear_users, registerUser, registerUser2): 
    user1 = requests.get(config.url + 'user/profile/v2', params={
        'token': registerUser["token"],
        'u_id': registerUser["auth_user_id"]
    })

    user_dict1 = json.loads(user1.text)["user"]


    user2 = requests.get(config.url + 'user/profile/v2', params={
        'token': registerUser["token"],
        'u_id': registerUser2["auth_user_id"]
    })

    user_dict2 = json.loads(user2.text)["user"]

    assert user_dict1["u_id"] == registerUser["auth_user_id"]
    assert user_dict1["email"] == 'hello@gmail.com'
    assert user_dict1["name_first"] == 'Jack'
    assert user_dict1["name_last"] == 'Smith'
    assert user_dict1["handle_str"] == 'jacksmith'

    assert user_dict2["u_id"] == registerUser2["auth_user_id"]
    assert user_dict2["email"] == 'hello2@gmail.com'
    assert user_dict2["name_first"] == 'John'
    assert user_dict2["name_last"] == 'Swish'
    assert user_dict2["handle_str"] == 'johnswish'


def test_http_user_profile_token_invalid(clear_users, registerUser): 
    assert requests.get(config.url + 'user/profile/v2', params={
        'token': "invalid",
        'u_id': registerUser["auth_user_id"]
    }).status_code == 403