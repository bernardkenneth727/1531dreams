import pytest
import requests
import json
from src import config

@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def setup_channel_routes(clear):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    channel_owner = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'member@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Member'
    })
    channel_member = json.loads(resp.text)

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': channel_owner["token"],
        'name': "My Channel",
        'is_public': True
    })
    my_channel = json.loads(resp.text)

    requests.post(config.url + 'channel/invite/v2', json={
        'token': channel_owner["token"],
        'channel_id': my_channel["channel_id"],
        'u_id': channel_member["auth_user_id"]
    })

    resp = requests.post(config.url + 'message/send/v2', json={
        'token': channel_owner["token"],
        'channel_id': my_channel["channel_id"],
        'message': 'Hello there!'
    })
    my_message = json.loads(resp.text)

    return {
        'owner': channel_owner,
        'member': channel_member,
        'channel': my_channel,
        'message': my_message
    }

# Testing message/pin/v1.
# Testing a normal circumstance where a message has been pinned.
def test_message_pin_valid_normal(setup_channel_routes):
    resp = requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    })

    assert resp.status_code == 200
    my_pin = json.loads(resp.text)
    assert my_pin == {}

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': setup_channel_routes["owner"]["token"],
        'channel_id': setup_channel_routes["channel"]["channel_id"],
        'start': 0
    })
    assert json.loads(resp.text)["messages"][0]["is_pinned"]

# Testing an invalid message id of the message to be pinned.
def test_message_pin_invalid_message(setup_channel_routes):
    assert requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': -1
    }).status_code == 400

# Testing where the message is already pinned.
def test_message_pin_invalid_already_pinned(setup_channel_routes):
    requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    })

    assert requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 400

# Testing a user that is not a member of the channel unable to pin the message.
def test_message_pin_invalid_non_member(setup_channel_routes):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'user@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'User'
    })
    new_user = json.loads(resp.text)

    assert requests.post(config.url + 'message/pin/v1', json={
        'token': new_user["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 403

# Testing a member but not owner of the channel unable to pin the message.
def test_message_pin_invalid_non_owner(setup_channel_routes):
    assert requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["member"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 403

# Testing an invalid token being used to unsuccessfully pin a message
def test_message_pin_invalid_token(setup_channel_routes):
    assert requests.post(config.url + 'message/pin/v1', json={
        'token': -1,
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 403

