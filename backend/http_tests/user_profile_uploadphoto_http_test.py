import pytest
import requests
import json
from src import config

# Basic fixture to clear the data.
@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def new_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'john.smith@gmail.com',
        'password': 'password',
        'name_first': 'John',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

# Testing user/profile/uploadphoto/v1.
# Testing a normal case where the user can change their profile photo.
def test_user_profile_uploadphoto_valid_normal(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/5TRraKEgc.jpg', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 256, 
        'y_end': 256
    }).status_code == 200
    resp = requests.get(config.url + 'user/profile/v2', params={
        'token': new_user["token"],
        'u_id': new_user["auth_user_id"]
    })
    profile_img_url = json.loads(resp.text)["user"]["profile_img_url"]
    assert profile_img_url != f"{config.url}static/default_pic.jpg"

# Testing a valid case where the user wants their profile picture to be the default.
def test_user_profile_uploadphoto_valid_default(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': f'{config.url}static/default_pic.jpg', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 250, 
        'y_end': 250
    }).status_code == 200
    resp = requests.get(config.url + 'user/profile/v2', params={
        'token': new_user["token"],
        'u_id': new_user["auth_user_id"]
    })
    profile_img_url = json.loads(resp.text)["user"]["profile_img_url"]
    assert profile_img_url != f"{config.url}static/default_pic.jpg"

# Testing an invalid case where the img_url provided does not return a 200 so it raises an InputError.
def test_user_profile_uploadphoto_invalid_url(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/5TRraKEgc123456789.jpg', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 256, 
        'y_end': 256
    }).status_code == 400

# Testing an invalid case where the dimensions of the photo provided are too small for the coordinates provided, raises an InputError.
def test_user_profile_uploadphoto_invalid_dimensions(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/5TRraKEgc.jpg', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 1000, 
        'y_end': 1000
    }).status_code == 400

    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/5TRraKEgc.jpg', 
        'x_start': -10, 
        'y_start': -10, 
        'x_end': 100, 
        'y_end': 100
    }).status_code == 400

    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/5TRraKEgc.jpg', 
        'x_start': -10, 
        'y_start': 0, 
        'x_end': 20, 
        'y_end': 1000
    }).status_code == 400

# Testing an invalid case where the url of the image provided is not of a jpg format.
def test_user_profile_uploadphoto_invalid_not_jpg(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': new_user["token"],
        'img_url': 'http://clipart-library.com/images/8T65aoEyc.png', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 256, 
        'y_end': 256
    }).status_code == 400

# Testing an invalid case where the token provided is not valid.
def test_user_profile_uploadphoto_invalid_token(clear, new_user):
    assert requests.post(config.url + 'user/profile/uploadphoto/v1', json={
        'token': 'hello',
        'img_url': 'http://clipart-library.com/images/5TRraKEgc.jpg', 
        'x_start': 0, 
        'y_start': 0, 
        'x_end': 256, 
        'y_end': 256
    }).status_code == 403