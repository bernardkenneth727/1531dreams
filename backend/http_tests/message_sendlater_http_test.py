import pytest
import requests
import json
from src import config
from datetime import datetime, timedelta, timezone
import time

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_invalid_token(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]

    channel_id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token,
        'name': "valid name",
        'is_public': True
    })
    channel_id_made = json.loads(channel_id_maker.text)
    channel_id = channel_id_made["channel_id"]

    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()

    assert requests.post(config.url + 'message/sendlater/v1', json = {
        'token': "invalidToken",
        'channel_id': channel_id,
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 403
    #time.sleep(5.0)

def test_channel_id_not_valid_channel(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]

    timeSent = datetime.now(timezone.utc) + timedelta(seconds=1)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()

    assert requests.post(config.url + 'message/sendlater/v1', json = {
        'token': token,
        'channel_id': "invalid_channel_id",
        'message': "valid message",
        'time_sent': time_sent
    }).status_code == 400
    #time.sleep(5.0)

def test_message_more_than_1000_chars(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]

    channel_id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token,
        'name': "valid name",
        'is_public': True
    })
    channel_id_made = json.loads(channel_id_maker.text)
    channel_id = channel_id_made["channel_id"]

    timeSent = datetime.now(timezone.utc) + timedelta(seconds=1)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlater/v1', json = {
        'token': token,
        'channel_id': channel_id,
        'message': "msg too long"*500,
        'time_sent': time_sent
    }).status_code == 400
    #time.sleep(5.0)

def test_time_in_past(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]

    channel_id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token,
        'name': "valid name",
        'is_public': True
    })
    channel_id_made = json.loads(channel_id_maker.text)
    channel_id = channel_id_made["channel_id"]

    timeSent = datetime.now(timezone.utc) - timedelta(seconds=10)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlater/v1', json = {
        'token': token,
        'channel_id': channel_id,
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 400
    #time.sleep(10.0)

def test_authorised_user_not_joined_channel(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]

    token_maker2 = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailisavalid@gmail.com',
        "password": 'validdpass',
        "name_first": "Jimmy",
        "name_last": "Barnes"
    })
    token_made2 = json.loads(token_maker2.text)
    token2 = token_made2["token"]

    channel_id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token2,
        'name': "valid name",
        'is_public': True
    })
    channel_id = json.loads(channel_id_maker.text)["channel_id"]

    timeSent = datetime.now(timezone.utc) + timedelta(seconds=10)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlater/v1', json = {
        'token': token,
        'channel_id': channel_id,
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 403
