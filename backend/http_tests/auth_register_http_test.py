import pytest
import requests
import json
from src import config

# Basic fixture to clear the data.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

# Testing auth/register/v2.
# Tests a success circumstance where the user registered is valid.
def test_http_auth_register_valid_normal(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'john.smith@gmail.com',
        'password': 'password',
        'name_first': 'John',
        'name_last': 'Smith'
    })
    assert resp.status_code == 200
    payload_register = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/login/v2', json={
        'email': 'john.smith@gmail.com',
        'password': 'password'
    })
    payload_login = json.loads(resp.text)
    assert payload_register["auth_user_id"] == payload_login["auth_user_id"]
    
    
# Testing an failed register with invalid email address.
def test_http_auth_register_invalid_email(clear_users):
    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello.com.',
        'password': 'hello123',
        'name_first': 'John',
        'name_last': 'Hello'
    }).status_code == 400

    assert requests.post(config.url + 'auth/register/v2', json={
        'email': '@email',
        'password': 'password',
        'name_first': 'First',
        'name_last': 'Last'
    }).status_code == 400
    
    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'qwerty@asdf.au.com',
        'password': '123456',
        'name_first': 'Qwerty',
        'name_last': 'Asdf'
    }).status_code == 400

# Testing an failed register with email already registered by someone else.
def test_http_auth_register_invalid_reemail(clear_users):
    requests.post(config.url + 'auth/register/v2', json={
        'email': 'email@myemail.com',
        'password': 'hello123!',
        'name_first': 'Art',
        'name_last': 'Vandelay'
    })
    
    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'email@myemail.com',
        'password': 'mypassword',
        'name_first': 'George',
        'name_last': 'Costanza'
    }).status_code == 400

# Testing an failed register with an invalid password (too short).
def test_http_auth_register_invalid_password(clear_users):
    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'george.costanza@gmail.com',
        'password': '1234',
        'name_first': 'George',
        'name_last': 'Costanza'
    }).status_code == 400

    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'kramer@yahoo.com',
        'password': '',
        'name_first': 'Cosmo',
        'name_last': 'Kramer'
    }).status_code == 400

# Testing an failed register with an invalid first name or last name (length too long or short).
def test_http_auth_register_invalid_name(clear_users):
    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'george@gmail.com',
        'password': '123456',
        'name_first': 'George',
        'name_last': ''
    }).status_code == 400

    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'drwho@gmail.com',
        'password': '123456',
        'name_first': '',
        'name_last': 'Who'
    }).status_code == 400

    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'hhhh@hotmail.com',
        'password': '123456',
        'name_first': 'H' * 51,
        'name_last': 'uman'
    }).status_code == 400

    assert requests.post(config.url + 'auth/register/v2', json={
        'email': 'long@surname.com',
        'password': '123456',
        'name_first': 'Michael',
        'name_last': 'L' * 51
    }).status_code == 400