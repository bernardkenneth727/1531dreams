import pytest
import requests
import json
from src import config

# Basic fixture to clear the data (just needed for clearing the users list), will change to clear method.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)

@pytest.fixture
def channelsCreate(registerUser):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': registerUser['token'], 
        'name': 'Teams1',
        'is_public': True   
    })

    return json.loads(resp.text)

@pytest.fixture
def channelsCreate2(registerUser2):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': registerUser2['token'], 
        'name': 'Teams2',
        'is_public': True   
    })

    return json.loads(resp.text)




def test_http_channels_listall_normal(clear_users, registerUser, registerUser2, channelsCreate, channelsCreate2): 
    resp = requests.get(config.url + 'channels/listall/v2', params={
        'token': registerUser["token"],
    })

    channels_list_all = json.loads(resp.text)["channels"]

    assert channels_list_all[0]["channel_id"] == channelsCreate["channel_id"]
    assert channels_list_all[0]["name"] == 'Teams1'
    assert channels_list_all[1]["channel_id"] == channelsCreate2["channel_id"]
    assert channels_list_all[1]["name"] == 'Teams2'


def test_http_channels_listall_invalidToken(clear_users, registerUser, channelsCreate): 
    assert requests.get(config.url + 'channels/listall/v2', params={
        'token': 'invalid',
    }).status_code == 403