import pytest
import requests 
import json
from src import config
# 400 for input error 
# 403 for access error 
@pytest.fixture
def clear_user():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def owner_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'david.so@gmail.com',
        'password': 'GeniusBrain',
        'name_first': 'David',
        'name_last': 'So'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })
    return json.loads(resp.text)

@pytest.fixture
def channel_create(owner_user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token' : owner_user["token"],
        'name' : "iAmironman",
        'is_public' : False
    })
    return json.loads(resp.text)

@pytest.fixture 
def message_send_id(owner_user, channel_create):
    resp = requests.post(config.url + 'message/send/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'message' : "durrymuncher"
    })
    return json.loads(resp.text)
@pytest.fixture
def dm_create(owner_user, member_user1):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token' : owner_user["token"],
        'u_ids' : [member_user1["auth_user_id"]]
    })
    return json.loads(resp.text)
    
############### LAWRENCE LEUNG ####################################
# TESTS FOR CHANNEL MESSAGE SHARE
def test_http_message_share_v1_invalid_token(clear_user, message_send_id, owner_user, channel_create):
    assert requests.post(config.url + 'message/share/v1', json={
        'token' : 232323223,
        'og_message_id' : message_send_id["message_id"],
        'message' : '',
        'channel_id' : channel_create["channel_id"],
        'dm_id' :  -1
    }).status_code == 403
def test_http_message_share_v1_auth_user_join_channel_or_dm(clear_user, message_send_id, owner_user, member_user1, channel_create):
    
    
    assert requests.post(config.url + 'message/share/v1', json={
        'token' : member_user1["token"],
        'og_message_id' : message_send_id["message_id"],
        'message' : 'nyess',
        'channel_id' : channel_create["channel_id"],
        'dm_id' :  -1
    }).status_code == 403

    assert requests.post(config.url + 'message/share/v1', json={
        'token' : member_user1["token"],
        'og_message_id' : message_send_id["message_id"],
        'message' : '',
        'channel_id' : channel_create["channel_id"],
        'dm_id' : -1
    }).status_code == 403

def test_http_message_share_v1_to_channel(clear_user, message_send_id, owner_user, member_user1, channel_create):
    assert requests.post(config.url + 'message/share/v1', json={
        'token' : owner_user["token"],
        'og_message_id' : message_send_id["message_id"],
        'message' : 'nyess',
        'channel_id' : channel_create["channel_id"],
        'dm_id' : -1
    }).status_code == 200

def test_http_message_share_v1_to_dm(clear_user, message_send_id, owner_user, member_user1, dm_create):  
    assert requests.post(config.url + 'message/share/v1', json={
        'token' : owner_user["token"],
        'og_message_id' : message_send_id["message_id"],
        'message' : 'it iz what it izz',
        'channel_id' : -1,
        'dm_id' : dm_create["dm_id"]
    }).status_code == 200
    
    
