import pytest
import requests
import json
from src import config
from datetime import datetime, timedelta, timezone
import time

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)

@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)

@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)

def test_valid(clear_users, dm, user1):
    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': user1['token'],
        'dm_id': dm["dm_id"],
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 200
    time.sleep(5.5)

def test_invalid_token(clear_users, dm):
    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': 'invalidToken',
        'dm_id': dm["dm_id"],
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 403

def test_dm_id_not_valid_dm(clear_users, user1):
    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': user1['token'],
        'dm_id': 'invalidDmId',
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 400

def test_message_more_than_1000_chars(clear_users, dm, user1):
    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': user1['token'],
        'dm_id': dm["dm_id"],
        'message': "msg invalid"*500,
        'time_sent': time_sent
    }).status_code == 400

def test_time_in_past(clear_users, dm, user1):
    timeSent = datetime.now(timezone.utc) - timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': user1['token'],
        'dm_id': dm["dm_id"],
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 400

def test_user_not_member_of_dm(clear_users, dm):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token_made = json.loads(token_maker.text)
    token = token_made["token"]
    timeSent = datetime.now(timezone.utc) + timedelta(seconds=5)
    time_sent = timeSent.replace(tzinfo=timezone.utc).timestamp()
    assert requests.post(config.url + 'message/sendlaterdm/v1', json = {
        'token': token,
        'dm_id': dm["dm_id"],
        'message': "msg valid",
        'time_sent': time_sent
    }).status_code == 403
