import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_http_valid(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    #token = result['token']
    u_id = result['auth_user_id']
    #set permission to owner
    permission_id = 1

    assert requests.post(config.url + 'admin/userpermission/change/v1', json = {
        'token': result['token'],
        'u_id': u_id,
        'permission_id': permission_id
    }).status_code == 200

def test_http_invalid_token(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    #token = result['token']
    u_id = result['auth_user_id']
    #set permission to owner
    permission_id = 1

    assert requests.post(config.url + 'admin/userpermission/change/v1', json = {
        'token': -1,
        'u_id': u_id,
        'permission_id': permission_id
    }).status_code == 403

def test_http_invalid_u_id(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    token = result['token']
    #u_id = result['auth_user_id']
    #set permission to owner
    permission_id = 1

    assert requests.post(config.url + 'admin/userpermission/change/v1', json = {
        'token': token,
        'u_id': -1,
        'permission_id': permission_id
    }).status_code == 400

def test_http_invalid_permission_id(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    token = result['token']
    u_id = result['auth_user_id']

    assert requests.post(config.url + 'admin/userpermission/change/v1', json = {
        'token': token,
        'u_id': u_id,
        'permission_id': -1
    }).status_code == 400

def test_http_user_not_owner(clear_users):
    #create user who is owner
    requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailradd@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    #create user who is not owner
    token_maker2 = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailadd@gmail.com',
        "password": 'valisdpr7ss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker2.text)
    token = result['token']
    u_id = result['auth_user_id']

    assert requests.post(config.url + 'admin/userpermission/change/v1', json = {
        'token': token,
        'u_id': u_id,
        'permission_id': 2
    }).status_code == 403
