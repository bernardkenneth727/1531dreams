import pytest
import requests
import json
from src import config

@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def setup_channel_routes(clear):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    channel_owner = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'member@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Member'
    })
    channel_member = json.loads(resp.text)

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': channel_owner["token"],
        'name': "My Channel",
        'is_public': True
    })
    my_channel = json.loads(resp.text)

    requests.post(config.url + 'channel/invite/v2', json={
        'token': channel_owner["token"],
        'channel_id': my_channel["channel_id"],
        'u_id': channel_member["auth_user_id"]
    })

    resp = requests.post(config.url + 'message/send/v2', json={
        'token': channel_owner["token"],
        'channel_id': my_channel["channel_id"],
        'message': 'Hello there!'
    })
    my_message = json.loads(resp.text)

    return {
        'owner': channel_owner,
        'member': channel_member,
        'channel': my_channel,
        'message': my_message
    }

@pytest.fixture
def setup_message_pinned(setup_channel_routes):
    requests.post(config.url + 'message/pin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    })

# Testing message/unpin/v1.
# Testing a normal circumstance where the user can unpin the message.
def test_message_pin_valid_normal(setup_channel_routes, setup_message_pinned):
    resp = requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    })
    
    assert resp.status_code == 200
    unpin = json.loads(resp.text)
    assert unpin == {}

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': setup_channel_routes["owner"]["token"],
        'channel_id': setup_channel_routes["channel"]["channel_id"],
        'start': 0
    })
    assert not json.loads(resp.text)["messages"][0]["is_pinned"]

# Testing an invalid message id (should raise InputError).
def test_message_unpin_invalid_message(setup_channel_routes, setup_message_pinned):
    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': -1
    }).status_code == 400

# Testing an already unpinned message (should raise InputError).
def test_message_unpin_invalid_already_unpinned(setup_channel_routes, setup_message_pinned):
    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 200

    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 400

# Testing a message that has not been pinned (should raise InputError).
def test_message_unpin_invalid_not_pinned(setup_channel_routes):
    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["owner"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 400

# Testing an invalid token attempting to unpin a message (should raise AccessError).
def test_message_unpin_invalid_token(setup_channel_routes, setup_message_pinned):
    assert requests.post(config.url + 'message/unpin/v1', json={
            'token': -1,
            'message_id': setup_channel_routes["message"]["message_id"]
        }).status_code == 403


# Testing a non-member of the channel attempting to unpin the message given (should raise AccessError).
def test_message_unpin_invalid_nonmember(setup_channel_routes, setup_message_pinned):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'tocool4channel@gmail.com',
        'password': '123456',
        'name_first': 'Channel',
        'name_last': 'Cool'
    })
    new_user = json.loads(resp.text)

    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': new_user["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 403

# Testing a non-owner of the channel attempting to unpin the message given (should raise AccessError).
def test_message_unpin_invalid_nonowner(setup_channel_routes, setup_message_pinned):
    assert requests.post(config.url + 'message/unpin/v1', json={
        'token': setup_channel_routes["member"]["token"],
        'message_id': setup_channel_routes["message"]["message_id"]
    }).status_code == 403

