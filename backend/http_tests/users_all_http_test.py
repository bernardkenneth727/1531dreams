import pytest
import requests
import json
from src import config

# Basic fixture to clear the data (just needed for clearing the users list), will change to clear method.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)



def test_http_users_all(clear_users, registerUser, registerUser2): 
    resp = requests.get(config.url + 'users/all/v1', params={
        'token': registerUser["token"],
    })

    users_list = json.loads(resp.text)["users"]

    assert users_list[0]["u_id"] == registerUser["auth_user_id"]
    assert users_list[0]["email"] == 'hello@gmail.com'
    assert users_list[0]["name_first"] == 'Jack'
    assert users_list[0]["name_last"] == 'Smith'
    assert users_list[0]["handle_str"] == 'jacksmith'

    assert users_list[1]["u_id"] == registerUser2["auth_user_id"]
    assert users_list[1]["email"] == 'hello2@gmail.com'
    assert users_list[1]["name_first"] == 'John'
    assert users_list[1]["name_last"] == 'Swish'
    assert users_list[1]["handle_str"] == 'johnswish'

def test_http_users_all_tokenInvalid(clear_users, registerUser, registerUser2): 
    assert requests.get(config.url + 'users/all/v1', params={
        'token': "invalid"
    }).status_code == 403
    
