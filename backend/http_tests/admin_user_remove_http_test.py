import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_http_normalcase(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailtovaluid@gmail.com',
        "password": 'valisdpuass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    #u_id = token['auth_user_id']
    token_maker2 = requests.post(config.url + 'auth/register/v2', json={
        "email": 'jim.smith@gmail.com',
        "password": 'password',
        "name_first": "Jimmy",
        "name_last": "Smith"
    })
    token2 = json.loads(token_maker2.text)
    requests.post(config.url + 'admin/userpermission/change/v1', json={
        'token': token['token'],
        'u_id': token2['auth_user_id'],
        'permission_id': 1
    })
    assert requests.delete(config.url + 'admin/user/remove/v1', json={
        'token': token['token'],
        'u_id': token['auth_user_id']
    }).status_code == 200

def test_http_invalid_token(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailtovaluid@gmail.com',
        "password": 'valisdpuass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    #u_id = token['auth_user_id']

    assert requests.delete(config.url + 'admin/user/remove/v1', json={
        'token': "invalidToken",
        'u_id': token['auth_user_id']
    }).status_code == 403

def test_http_invalid_u_id(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    #u_id = token['auth_user_id']

    assert requests.delete(config.url + 'admin/user/remove/v1', json={
        'token': result['token'],
        'u_id': -1
    }).status_code == 400

def test_http_user_only_owner(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    result = json.loads(token_maker.text)
    token = result['token']
    u_id = result['auth_user_id']

    assert requests.delete(config.url + 'admin/user/remove/v1', json={
        'token': token,
        'u_id': u_id
    }).status_code == 400

def test_http_user_not_owner(clear_users):
    #first user added is the owner
    requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailrtovaluid@gmail.com',
        "password": 'valisdprss',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    #this second user is not the owner
    output = requests.post(config.url + 'auth/register/v2', json={
        "email": 'ema3ilrtovaluid@gmail.com',
        "password": 'valis2dprss',
        "name_first": "Ji4mfmy",
        "name_last": "Barn4es"
    })
    output2 = json.loads(output.text)
    token = output2['token']
    u_id = output2['auth_user_id']

    assert requests.delete(config.url + 'admin/user/remove/v1', json={
        'token': token,
        'u_id': u_id
    }).status_code == 403
