import pytest
import requests
import json
from src import config


@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

#post
#input error so 400
def test_http_invalid_u_id(clear_users):
    result = requests.post(config.url + 'auth/register/v2', json={
        "email": 'emailvalid@gmail.com',
        "password": 'validpass',
        "name_first": "Jimmy",
        "name_last": "Barnes"
    })
    register_dict = json.loads(result.text)
    assert requests.post(config.url + 'admin/userpermission/change/v1', json={
        'token': register_dict["token"],
        'u_id': -1,
        'permission_id': 1
    }).status_code == 400

