import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_http_large_name(clear_users):
    result = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emaildvalid@gmail.com',
        "password": 'valdidpass',
        "name_first": "Jidmmy",
        "name_last": "Bardnes"
    })
    register_dict = json.loads(result.text)

    assert requests.post(config.url + 'channels/create/v2', json = {
        "token": register_dict["token"],
        "name": "tooLong"*500,
        "is_public": True
    }).status_code == 400
    
def test_http_valid_case(clear_users):
    result = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emaildvalid@gmail.com',
        "password": 'valdidpass',
        "name_first": "Jidmmy",
        "name_last": "Bardnes"
    })
    register_dict = json.loads(result.text)
    token = register_dict['token']

    resp = requests.post(config.url + 'channels/create/v2', json = {
        "token": token,
        "name": "validName",
        "is_public": True
    })
    id_channel = json.loads(resp.text)
    assert type(id_channel["channel_id"]) == int
    #check channel id is int for some indication that
    #normal case works
