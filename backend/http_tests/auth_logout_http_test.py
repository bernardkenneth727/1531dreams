import pytest
import requests
import json
from src import config

# Basic fixture to clear the data.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

# Fixture for a registered user Hello World.
@pytest.fixture
def register_helloworld():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })

    return json.loads(resp.text)

# Testing auth/logout/v1.
# Tests a success circumstance where users register and logs out.
def test_http_auth_logout_valid_normal(clear_users, register_helloworld):
    resp = requests.post(config.url + 'auth/logout/v1', json={
        'token': register_helloworld['token']
    })
    assert json.loads(resp.text)["is_success"]

# Tests an invalid circumstance where the user attempts to log out twice, raising an AccessError.
def test_http_auth_logout_already_out(clear_users, register_helloworld):
    resp = requests.post(config.url + 'auth/logout/v1', json={
        'token': register_helloworld['token']
    })
    assert json.loads(resp.text)["is_success"]

    assert requests.post(config.url + 'auth/logout/v1', json={
        'token': register_helloworld['token']
    }).status_code == 403

# Tests an invalid circumstance where an invalid token is given.
def test_http_auth_logout_invalid_token(clear_users):
    assert requests.post(config.url + 'auth/logout/v1', json={
        'token': '123'
    }).status_code == 403

    assert requests.post(config.url + 'auth/logout/v1', json={
        'token': 'randomtoken.random.random'
    }).status_code == 403
