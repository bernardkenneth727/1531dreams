import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

def test_http_invalid_token(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #print(token)
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)
    
    assert requests.post(config.url + 'message/send/v2', json = {
        'token': 'invalid token',
        'channel_id': id_made["channel_id"],
        'message': "valid message"
    }).status_code == 403

def test_http_normal(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailtovalid@gmail.com',
        "password": 'valisdpass',
        "name_first": "Jimfmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    #print(token)
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token['token'],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)
    
    assert requests.post(config.url + 'message/send/v2', json = {
        'token': token['token'],
        'channel_id': id_made["channel_id"],
        'message': "valid message"
    }).status_code == 200

def test_http_message_too_long(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emailizvalid@gmail.com',
        "password": 'valdidpass',
        "name_first": "Jimdmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)
    
    id_maker = requests.post(config.url + 'channels/create/v2', json = {
        'token': token["token"],
        'name': "valid name",
        'is_public': True
    })
    id_made = json.loads(id_maker.text)
    
    assert requests.post(config.url + 'message/send/v2', json = {
        'token': token["token"],
        'channel_id': id_made["channel_id"],
        'message': "toolong"*500
    }).status_code == 400

def test_http_user_not_joined_channel(clear_users):
    token_maker = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emaildizvalid@gmail.com',
        "password": 'valdisdpass',
        "name_first": "Jimdmy",
        "name_last": "Barnes"
    })
    token = json.loads(token_maker.text)

    channel_creater = requests.post(config.url + 'channels/create/v2', json = {
        'token': token["token"],
        "name": "validName",
        "is_public": True
    })
    channel_id = json.loads(channel_creater.text)

    token_maker2 = requests.post(config.url + 'auth/register/v2', json = {
        "email": 'emfaildizvalid@gmail.com',
        "password": 'vafldisdpass',
        "name_first": "Jfimdmy",
        "name_last": "Bfarnes"
    })
    token2 = json.loads(token_maker2.text)

    assert requests.post(config.url + 'message/send/v2', json = {
        'token': token2["token"],
        'channel_id': channel_id["channel_id"],
        'message': "validMsg"
    }).status_code == 403
