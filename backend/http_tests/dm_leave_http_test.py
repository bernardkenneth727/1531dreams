import pytest
import requests
import json
from src import config

@pytest.fixture
def intro_dm():
    requests.delete(config.url + 'clear/v1')
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'johnsmith@gmail.com', 
        'password': '123456', 
        'name_first': 'John', 
        'name_last': 'Smith'
    })
    result1 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'michaelscott@dundermifflin.com', 
        'password': 'PASSWORD',
        'name_first': 'Michael',
        'name_last': 'Scott'
    })
    result2 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'leslieknope@pawnee.gov', 
        'password': 'JoeBiden1',
        'name_first': 'Leslie',
        'name_last': 'Knope'
    })
    result3 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'jake.peralta@nypdbrooklyn.gov', 
        'password': 'nine-nine',
        'name_first': 'Jake',
        'name_last': 'Peralta'
    })
    result4 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'eleanorshellstrop@yahoo.com', 
        'password': 'TheBadPlace',
        'name_first': 'Eleanor',
        'name_last': 'Shellstrop'
    })
    result5 = json.loads(resp.text)
    
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': result1["token"],
        'u_ids': [result2["auth_user_id"], result3["auth_user_id"]]
    })
    new_dm = json.loads(resp.text)
    
    return {
        "john": result1,
        "michael": result2,
        "leslie": result3,
        "jake": result4,
        "eleanor": result5,
        "dm": new_dm
    }

# Testing dm/leave/v1.
# Testing a normal valid dm leave scenario.
def test_dm_leave_valid_normal(intro_dm):
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

    requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["michael"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })

    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 2

    requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["leslie"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })

    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 1

# Testing an invalid token leaving the dm (AccessError).
def test_dm_leave_invalid_token(intro_dm):
    assert requests.post(config.url + 'dm/leave/v1', json={
        'token': '123.123.123',
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing an incorrect dm_id for a member leaving the dm (InputError).
def test_dm_leave_invalid_dm(intro_dm):
    assert requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["michael"]["token"],
        'dm_id': -1
    }).status_code == 400
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing the token user not being in the dm to be able to leave (AccessError).
def test_dm_leave_invalid_auth_user(intro_dm):
    assert requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["jake"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing the authorised user leaving multiple times where the first time is 
# valid but after is invalid.
def test_dm_leave_invalid_multiple_times(intro_dm):
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

    requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["michael"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })

    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 2

    assert requests.post(config.url + 'dm/leave/v1', json={
        'token': intro_dm["michael"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403

    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 2