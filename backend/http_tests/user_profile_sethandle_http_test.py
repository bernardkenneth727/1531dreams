import pytest
import requests
import json
from src import config

# Basic fixture to clear the data (just needed for clearing the users list), will change to clear method.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)


def test_http_user_profile_sethandle_normal(clear_users, registerUser): 
    requests.put(config.url + 'user/profile/sethandle/v2', json={
        'token': registerUser["token"],
        'handle_str': "new_handle",
    })

    user = requests.get(config.url + 'user/profile/v2', params={
        'token': registerUser["token"],
        'u_id': registerUser["auth_user_id"]
    })

    user_dict = json.loads(user.text)["user"]

    assert user_dict["u_id"] == registerUser["auth_user_id"]
    assert user_dict["email"] == 'hello@gmail.com'
    assert user_dict["name_first"] == 'Jack'
    assert user_dict["name_last"] == 'Smith'
    assert user_dict["handle_str"] == 'new_handle'

def test_http_user_profile_sethandle_handle_invalid(clear_users, registerUser): 
    assert requests.put(config.url + 'user/profile/sethandle/v2', json={
        'token': registerUser['token'],
        'handle_str': "invalidinvalidinvalidinvalidinvalidinvalid"
    }).status_code == 400

def test_http_user_profile_sethandle_token_invalid(clear_users, registerUser): 
    assert requests.put(config.url + 'user/profile/sethandle/v2', json={
        'token': 'invalid',
        'handle_str': "new_handle"
    }).status_code == 403


def test_http_user_profile_sethandle_handle_taken(clear_users, registerUser, registerUser2): 
    assert requests.put(config.url + 'user/profile/sethandle/v2', json={
        'token': registerUser["token"],
        'handle_str': "johnswish"
    }).status_code == 400