import pytest
import requests
import json
from src import config

# Basic fixture to clear the data (just needed for clearing the users list), will change to clear method.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)


def test_http_user_profile_setname_normal(clear_users, registerUser): 
    requests.put(config.url + 'user/profile/setname/v2', json={
        'token': registerUser["token"],
        'name_first': "new_first",
        'name_last': "new_last"
    })

    user = requests.get(config.url + 'user/profile/v2', params={
        'token': registerUser["token"],
        'u_id': registerUser["auth_user_id"]
    })

    user_dict = json.loads(user.text)["user"]

    assert user_dict["u_id"] == registerUser["auth_user_id"]
    assert user_dict["email"] == 'hello@gmail.com'
    assert user_dict["name_first"] == 'new_first'
    assert user_dict["name_last"] == 'new_last'
    assert user_dict["handle_str"] == 'jacksmith'

def test_http_user_profile_setname_token_invalid(clear_users, registerUser): 
    assert requests.put(config.url + 'user/profile/setname/v2', json={
        'token': "invalid",
        'name_first': "new_first",
        'name_last': "new_last"
    }).status_code == 403

def test_http_user_profile_setname_nameFirst_invalid(clear_users, registerUser): 
    assert requests.put(config.url + 'user/profile/setname/v2', json={
        'token': registerUser["token"],
        'name_first': "wertdstytrewertytrertytrertytrertytrerttrertytrertre>50trerttrre>50",
        'name_last': "new_last"
    }).status_code == 400

def test_http_user_profile_setname_nameLast_invalid(clear_users, registerUser): 
    assert requests.put(config.url + 'user/profile/setname/v2', json={
        'token': registerUser["token"],
        'name_first': "name_first",
        'name_last': "wertdstytrewertytrertytrertytrertytrerttrertytrertre>50trerttrre>50"
    }).status_code == 400