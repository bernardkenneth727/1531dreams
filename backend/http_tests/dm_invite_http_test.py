import pytest
import requests
import json
from src import config

@pytest.fixture
def intro_dm():
    requests.delete(config.url + 'clear/v1')
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'johnsmith@gmail.com', 
        'password': '123456', 
        'name_first': 'John', 
        'name_last': 'Smith'
    })
    result1 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'michaelscott@dundermifflin.com', 
        'password': 'PASSWORD',
        'name_first': 'Michael',
        'name_last': 'Scott'
    })
    result2 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'leslieknope@pawnee.gov', 
        'password': 'JoeBiden1',
        'name_first': 'Leslie',
        'name_last': 'Knope'
    })
    result3 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'jake.peralta@nypdbrooklyn.gov', 
        'password': 'nine-nine',
        'name_first': 'Jake',
        'name_last': 'Peralta'
    })
    result4 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'eleanorshellstrop@yahoo.com', 
        'password': 'TheBadPlace',
        'name_first': 'Eleanor',
        'name_last': 'Shellstrop'
    })
    result5 = json.loads(resp.text)
    
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': result1["token"],
        'u_ids': [result2["auth_user_id"], result3["auth_user_id"]]
    })
    new_dm = json.loads(resp.text)
    
    return {
        "john": result1,
        "michael": result2,
        "leslie": result3,
        "jake": result4,
        "eleanor": result5,
        "dm": new_dm
    }

# Testing dm/invite/v1.
# Tests a normal invite scenario that is valid.
def test_dm_invite_valid_normal(intro_dm):
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

    requests.post(config.url + 'dm/invite/v1', json={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"],
        'u_id': intro_dm["jake"]["auth_user_id"]
    })
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 4
    
    requests.post(config.url + 'dm/invite/v1', json={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"],
        'u_id': intro_dm["eleanor"]["auth_user_id"]
    })
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 5

# Testing when the token parameter for the auth user is invalid (AccessError).
def test_dm_invite_invalid_token(intro_dm):
    assert requests.post(config.url + 'dm/invite/v1', json={
        'token': '123.123.123',
        'dm_id': intro_dm["dm"]["dm_id"],
        'u_id': intro_dm["jake"]["auth_user_id"]
    }).status_code == 403
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing when the user being invited has an invalid u_id (InputError).
def test_dm_invite_invalid_u_id(intro_dm):
    assert requests.post(config.url + 'dm/invite/v1', json={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"],
        'u_id': -1
    }).status_code == 400
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing when the dm_id being used to invite someone is invalid (InputError).
def test_dm_invite_invalid_dm_id(intro_dm):
    assert requests.post(config.url + 'dm/invite/v1', json={
        'token': intro_dm["john"]["token"],
        'dm_id': -1,
        'u_id': intro_dm["eleanor"]["auth_user_id"]
    }).status_code == 400
    
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    members_count = len(json.loads(resp.text)["members"])
    assert members_count == 3

# Testing when the user attempting to invite is not in the dm (AccessError).
def test_dm_invite_invalid_not_auth_user(intro_dm):
    assert requests.post(config.url + 'dm/invite/v1', json={
        'token': intro_dm["jake"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"],
        'u_id': intro_dm["eleanor"]["auth_user_id"]
    }).status_code == 403