import pytest
import requests
import json
from src import config
import time

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser3():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello3@gmail.com',
        'password': '12345678!',
        'name_first': 'Jacky',
        'name_last': 'Harlow'
    })

    return json.loads(resp.text)

@pytest.fixture
def channelsCreate(registerUser):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': registerUser['token'], 
        'name': 'Teams1',
        'is_public': True   
    })

    return json.loads(resp.text)

@pytest.fixture
def channelJoin(registerUser2, channelsCreate):
    requests.post(config.url + 'channel/join/v2', json={
        'token': registerUser2['token'], 
        'channel_id': channelsCreate['channel_id'],
    })


@pytest.fixture
def standupStart(registerUser, channelsCreate):
    resp = requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    })

    return json.loads(resp.text)



def test_http_standup_send_normal(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart): 
    requests.post(config.url + 'standup/send/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'message': 'Hi everyone!'
    })

    time.sleep(2.1)

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': registerUser['token'],
        'channel_id': channelsCreate['channel_id'],
        'start': 0
    })

    standup_message = json.loads(resp.text)

    assert standup_message['messages'][0]['message'] == 'jacksmith: Hi everyone!'

def test_http_standup_send_invalidToken(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart): 
    assert requests.post(config.url + 'standup/send/v1', json={
        'token': 'invalid', 
        'channel_id': channelsCreate['channel_id'],
        'message': 'Hi everyone!'
    }).status_code == 403

def test_http_standup_send_invalidChannel(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart): 
    assert requests.post(config.url + 'standup/send/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'] + 111,
        'message': 'Hi everyone!'
    }).status_code == 400

def test_http_standup_send_invalidMessage(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart): 
    assert requests.post(config.url + 'standup/send/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'message': 'Hi everyone!' * 1000
    }).status_code == 400

def test_http_standup_send_InactiveStandup(clear_users, registerUser, registerUser2, channelsCreate, channelJoin): 
    assert requests.post(config.url + 'standup/send/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'message': 'Hi everyone!'
    }).status_code == 400

def test_http_standup_send_UserNotInChannel(clear_users, registerUser, registerUser2, registerUser3, channelsCreate, channelJoin, standupStart): 
    assert requests.post(config.url + 'standup/send/v1', json={
        'token': registerUser3['token'], 
        'channel_id': channelsCreate['channel_id'],
        'message': 'Hi everyone!'
    }).status_code == 403

    time.sleep(2.1)