import pytest
import requests
import json
from src import config

@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def user(clear):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)

@pytest.fixture
def channel(user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': user["token"],
        'name': 'channel1',
        'is_public': True
    })
    return json.loads(resp.text)

def test_http_channel_join_channel_id(clear, user, channel):

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': user['token'],
        'channel_id': -1
    }).status_code == 400

    resp = requests.post(config.url + 'channel/join/v2', json={
        'token': user['token'],
        'channel_id': channel['channel_id']
    })
    
    payload = json.loads(resp.text)
    assert payload == {}


# check user id is a valid user
def test_http_channel_join_user_id(clear, user, channel):

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': '1A',
        'channel_id': channel['channel_id']
    }).status_code == 403

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': '/A',
        'channel_id': channel['channel_id']
    }).status_code == 403

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': '2.4',
        'channel_id': channel['channel_id']
    }).status_code == 403

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': -1,
        'channel_id': channel['channel_id']
    }).status_code == 403

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': 2.7,
        'channel_id': channel['channel_id']
    }).status_code == 403

    resp = requests.post(config.url + 'channel/join/v2', json={
        'token': user['token'],
        'channel_id': channel['channel_id']
    })
    payload = json.loads(resp.text)
    assert payload == {}


def test_http_channel_join_perms(clear, user):

    resp_channel = requests.post(config.url + 'channels/create/v2', json={
        'token': user["token"],
        'name': 'channel1',
        'is_public': False
    })
    payload_channel = json.loads(resp_channel.text)

    resp_auth = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    payload_auth = json.loads(resp_auth.text)

    assert requests.post(config.url + 'channel/join/v2', json={
        'token': payload_auth['token'],
        'channel_id': payload_channel['channel_id']
    }).status_code == 403

# test return type
def test_http_channel_join_return(clear, user, channel):

    resp = requests.post(config.url + 'channel/join/v2', json={
        'token': user['token'],
        'channel_id': channel['channel_id']
    })
    payload = json.loads(resp.text)
    assert payload == {}