import pytest
import requests
import json
from src import config

# Basic fixture to clear the data.
@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

# Fixture for a registered user Hello World.
@pytest.fixture
def register_helloworld():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })

    return json.loads(resp.text)

# Fixture for a registered user Doctor UNSW.
@pytest.fixture
def register_doctor():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })

    return json.loads(resp.text)

# Testing auth/login/v2.
# Tests a success circumstance where users register and login.
def test_http_auth_login_valid_normal(clear_users, register_helloworld, register_doctor):
    resp = requests.post(config.url + 'auth/login/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!'
    })
    login_helloworld = json.loads(resp.text)
    assert register_helloworld["auth_user_id"] == login_helloworld["auth_user_id"]
    
    resp = requests.post(config.url + 'auth/login/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r'
    })
    login_doctor = json.loads(resp.text)
    assert register_doctor["auth_user_id"] == login_doctor["auth_user_id"]
    assert register_doctor["auth_user_id"] != login_helloworld["auth_user_id"]
    assert register_doctor["token"] != login_doctor["token"]

# Testing login with an invalid email.
def test_http_auth_login_invalid_email(clear_users, register_helloworld):
    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'hello@gmail',
        'password': 'password'
    }).status_code == 400

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'gmail.com',
        'password': 'password'
    }).status_code == 400

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'hello@gmail.com.',
        'password': 'password'
    }).status_code == 400

# Testing logging in with the correct email, but the password is incorrect.
def test_http_login_incorrect_password(clear_users, register_helloworld, register_doctor):
    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456'
    }).status_code == 400

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'hello@gmail.com',
        'password': ''
    }).status_code == 400

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'Doctor'
    }).status_code == 400

# Testing login with a valid email, but the email does not belong to a user.
def test_http_login_no_email(clear_users):
    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'johnsmith@yahoo.com',
        'password': 'password'
    }).status_code == 400

    requests.post(config.url + 'auth/register/v2', json={
        'email': 'johnsmith@yahoo.com',
        'password': 'password',
        'name_first': 'John',
        'name_last': 'Smith'
    })

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'johnsmith@yahooo.com',
        'password': 'password'
    }).status_code == 400

# Testing login with no registered users.
def test_http_login_no_users(clear_users):
    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'first@yahoo.com',
        'password': 'PASSWORD'
    }).status_code == 400

    assert requests.post(config.url + 'auth/login/v2', json={
        'email': 'elon@tesla.com',
        'password': 'ModelS3XY'
    }).status_code == 400
