import pytest
import requests
import json
from src import config

@pytest.fixture
def intro_dm():
    requests.delete(config.url + 'clear/v1')
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'johnsmith@gmail.com', 
        'password': '123456', 
        'name_first': 'John', 
        'name_last': 'Smith'
    })
    result1 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'michaelscott@dundermifflin.com', 
        'password': 'PASSWORD',
        'name_first': 'Michael',
        'name_last': 'Scott'
    })
    result2 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'leslieknope@pawnee.gov', 
        'password': 'JoeBiden1',
        'name_first': 'Leslie',
        'name_last': 'Knope'
    })
    result3 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'jake.peralta@nypdbrooklyn.gov', 
        'password': 'nine-nine',
        'name_first': 'Jake',
        'name_last': 'Peralta'
    })
    result4 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'eleanorshellstrop@yahoo.com', 
        'password': 'TheBadPlace',
        'name_first': 'Eleanor',
        'name_last': 'Shellstrop'
    })
    result5 = json.loads(resp.text)
    
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': result1["token"],
        'u_ids': [result2["auth_user_id"], result3["auth_user_id"]]
    })
    new_dm = json.loads(resp.text)
    
    return {
        "john": result1,
        "michael": result2,
        "leslie": result3,
        "jake": result4,
        "eleanor": result5,
        "dm": new_dm
    }

# Testing dm/details/v1.
# Testing a normal valid circumstance.
def test_dm_details_valid_normal(intro_dm):
    resp = requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    })
    result = json.loads(resp.text)
    assert result == {
        "name": intro_dm["dm"]["dm_name"],
        "members": [
            {
                "u_id": intro_dm["john"]["auth_user_id"],
                "email": "johnsmith@gmail.com",
                "name_first": "John",
                "name_last": "Smith",
                "handle_str": "johnsmith",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["michael"]["auth_user_id"],
                "email": "michaelscott@dundermifflin.com",
                "name_first": "Michael",
                "name_last": "Scott",
                "handle_str": "michaelscott",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }, {
                "u_id": intro_dm["leslie"]["auth_user_id"],
                "email": "leslieknope@pawnee.gov",
                "name_first": "Leslie",
                "name_last": "Knope",
                "handle_str": "leslieknope",
                "profile_img_url": f"{config.url}static/default_pic.jpg"
            }
        ]
    }

# Testing an invalid circumstance where the dm_id is not valid.
def test_dm_details_invalid_dm_id(intro_dm):
    assert requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["john"]["token"],
        'dm_id': -1
    }).status_code == 400
    

# Testing an invalid circumstance where the token user is not a
# member of the dm.
def test_dm_details_invalid_auth_user(intro_dm):
    assert requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["jake"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403

    assert requests.get(config.url + 'dm/details/v1', params={
        'token': intro_dm["eleanor"]["token"],
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403

# Testing an invalid circumstance where the token is not a valid user and 
# session.
def test_dm_details_invalid_token(intro_dm):
    assert requests.get(config.url + 'dm/details/v1', params={
        'token': '123.123.123',
        'dm_id': intro_dm["dm"]["dm_id"]
    }).status_code == 403