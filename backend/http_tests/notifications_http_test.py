import pytest
import requests 
import json
from src import config
# 400 for input error 
# 403 for access error 
@pytest.fixture
def clear_user():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def owner_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'david.so@gmail.com',
        'password': 'GeniusBrain',
        'name_first': 'David',
        'name_last': 'So'
    })
    return json.loads(resp.text)

@pytest.fixture
def channel_create(owner_user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token' : owner_user["token"],
        'name' : "iAmironman",
        'is_public' : False
    })

    return json.loads(resp.text)

@pytest.fixture
def dm_create(owner_user, member_user1):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token' : owner_user["token"],
        'u_ids' : [member_user1["auth_user_id"]]
    })
    return json.loads(resp.text)

def test_http_notifications_get_v1_invalid_token(clear_user):
    assert requests.get(config.url + 'notifications/get/v1', params={
        'token' : 244234242
    }).status_code == 403
    
def test_http_notications_get_v1_one_notif(clear_user, owner_user, member_user1, dm_create):
    
    resp = requests.get(config.url + 'notifications/get/v1', params={
        'token' : member_user1["token"]
    })
    result = json.loads(resp.text)
    notif_count = len(result["notifications"])

    assert notif_count == 1

def test_http_notications_get_v1_twenty_notif(clear_user, owner_user, channel_create):
    i = 0
    while i < 21:
        requests.post(config.url + 'message/send/v2', json={
            'token' : owner_user["token"],
            'channel_id' : channel_create["channel_id"],
            'message' : "@helloworld"
        })
        i = i + 1
    result = requests.get(config.url + 'notifications/get/v1', params={
        'token' : owner_user["token"]
    })

    notif = json.loads(result.text)
    notif_count = len(notif["notifications"])
    assert notif_count == 20
    
