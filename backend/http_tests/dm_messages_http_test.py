import pytest
import requests
import json
from src import config

@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')


def test_http_dm_messages_return_type(clear, user1, dm):
    message = 'nick is cool'
    start = 0

    resp = requests.post(config.url + 'message/senddm/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'message': message
    })
    assert resp.status_code == 200
    resp = requests.get(config.url + 'dm/messages/v1', params={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'start': start
    })
    payload = json.loads(resp.text)

    assert type(payload) is dict

def test_http_dm_messages_return_message(clear, user1, dm):
    message = "nick is cool"
    start = 0

    resp_senddm = requests.post(config.url + 'message/senddm/v1', json={
        'token': user1["token"],
        'dm_id': dm["dm_id"],
        'message': message
    })

    message_id = json.loads(resp_senddm.text)["message_id"]

    resp_messages = requests.get(config.url + 'dm/messages/v1', params={
        'token': user1["token"],
        'dm_id': dm["dm_id"],
        'start': start
    })

    messages = json.loads(resp_messages.text)['messages']

    message_expected = {
            "message_id": message_id,
            "message": message,
            "u_id": user1["auth_user_id"],
            "reacts": [{'react_id': 1, 'u_ids': [], 'is_this_user_reacted': False}],
            "is_pinned": False
        }
    message_selected = {}
    
    for msg in messages:
        if msg["message_id"] == message_id:
            message_selected = msg

    del message_selected["time_created"]
    assert message_selected == message_expected

def test_http_dm_messages_start_greater_messages(clear, user1, dm):
    start = 50

    assert requests.get(config.url + 'dm/messages/v1', params={
        'token': user1["token"],
        'dm_id': dm["dm_id"],
        'start': start
    }).status_code == 400

def test_http_dm_messages_return_message_user_not_dm_member(clear, user1, dm):
    message = "nick is cool"
    start = 0

    requests.post(config.url + 'message/senddm/v1', json={
        'token': user1["token"],
        'dm_id': dm["dm_id"],
        'message': message
    })

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example3@example.com",
        'password': "password",
        'name_first': "Jack",
        'name_last': "Doe"
    })
    user_not_member = json.loads(resp.text)

    assert requests.get(config.url + 'dm/messages/v1', params={
        'token': user_not_member["token"],
        'dm_id': dm["dm_id"],
        'start': start
    }).status_code == 403

def test_http_dm_messages_valid_dm(clear, user1, dm):
    message = "nick is cool"
    start = 0

    requests.post(config.url + 'message/senddm/v1', json={
        'token': user1["token"],
        'dm_id': dm["dm_id"],
        'message': message
    })

    assert requests.get(config.url + 'dm/messages/v1', params={
        'token': user1["token"],
        'dm_id': 1,
        'start': start
    }).status_code == 400