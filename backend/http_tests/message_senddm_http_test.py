import pytest
import requests
import json
from src import config


@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)


def test_http_message_send_dm_return_type(clear, user1, user2, dm):
    message = "Hi. This is the best team in COMP1531"

    resp_user1 = requests.post(config.url + 'message/senddm/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'message': message
    })
    payload_user1 = json.loads(resp_user1.text)
    assert type(payload_user1) is dict

    resp_user2 = requests.post(config.url + 'message/senddm/v1', json={
        'token': user2['token'],
        'dm_id': dm['dm_id'],
        'message': message
    })
    payload_user2 = json.loads(resp_user2.text)
    assert type(payload_user2) is dict


def test_http_message_send_dm_message_long(clear, user1, dm):
    message = "lit" * 1000

    assert requests.post(config.url + 'message/senddm/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id'],
        'message': message
    }).status_code == 400


def test_http_message_send_dm_user_not_member(clear, dm):
    message = "Hi. This is the best team in COMP1531"

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example3@example.com",
        'password': "password",
        'name_first': "Jack",
        'name_last': "Doe"
    })
    user_not_member = json.loads(resp.text)

    assert requests.post(config.url + 'message/senddm/v1', json={
        'token': user_not_member["token"],
        'dm_id': dm['dm_id'],
        'message': message
    }).status_code == 403

def test_message_send_dm_valid_user(clear, dm):
    message = "Hi. This is the best team in COMP1531"

    assert requests.post(config.url + 'message/senddm/v1', json={
        'token': 1,
        'dm_id': dm['dm_id'],
        'message': message
    }).status_code == 403
