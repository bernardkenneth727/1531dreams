import pytest
import requests
import json
from src import config
from src.secret import token_encode

@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)

def test_http_dm_remove_return_type(clear, user1, dm):
    resp = requests.delete(config.url + 'dm/remove/v1', json={
        'token': user1['token'],
        'dm_id': dm['dm_id']
    })
    payload = json.loads(resp.text)
    assert type(payload) is dict


def test_http_dm_remove_not_creator(clear, user2, dm):
    assert requests.delete(config.url + 'dm/remove/v1', json={
        'token': user2['token'],
        'dm_id': dm['dm_id']
    }).status_code == 403


def test_http_dm_remove_invalid_dm_id(clear, user1):
    assert requests.delete(config.url + 'dm/remove/v1', json={
        'token': user1['token'],
        'dm_id': 1
    }).status_code == 400


def test_http_dm_remove_owner_valid(clear, dm):
    token = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })
    assert requests.delete(config.url + 'dm/remove/v1', json={
        'token': token,
        'dm_id': dm['dm_id']
    }).status_code == 403