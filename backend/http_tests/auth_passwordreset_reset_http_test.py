import pytest
import requests
import json
from src import config
from src.auth_code import reset

def test_auth_passwordreset_invalid_code():

    assert requests.post(config.url + 'auth/passwordreset/reset/v1', json = {
        'reset_code': '0.2',
        'new_password': '123456'
    }).status_code == 400

def test_auth_passwordreset_password_long():

    assert requests.post(config.url + 'auth/passwordreset/reset/v1', json = {
        'reset_code': reset['reset_code'],
        'new_password': '123'
    }).status_code == 400

    assert requests.post(config.url + 'auth/passwordreset/reset/v1', json = {
        'reset_code': reset['reset_code'],
        'new_password': '12345'
    }).status_code == 400

def test_auth_passwordreset_return():

    resp = requests.post(config.url + 'auth/passwordreset/reset/v1', json = {
        'reset_code': reset['reset_code'],
        'new_password': '123456'
    })

    assert resp.status_code == 200

    assert  json.loads(resp.text) == {}