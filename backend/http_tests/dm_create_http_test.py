import pytest
import requests
import json
from src import config
from src.secret import token_encode


@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example2@example.com",
        'password': "password",
        'name_first': "Jane",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def dm(user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1["token"],
        'u_ids': [user2["auth_user_id"]]
    })
    return json.loads(resp.text)


def test_http_dm_create_return_type(clear, user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1['token'],
        'u_ids': [user2["auth_user_id"]]
    })

    payload = json.loads(resp.text)
    assert type(payload) is dict


def test_http_dm_create_only_owner(clear, user1):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1['token'],
        'u_ids': []
    })
    payload = json.loads(resp.text)
    assert payload['dm_name'] == 'johndoe'


def test_http_dm_create_return_handle(clear, user1, user2):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': user1['token'],
        'u_ids': [user2["auth_user_id"]]
    })
    payload = json.loads(resp.text)
    assert payload['dm_name'] == "janedoe, johndoe"


def test_http_dm_create_return_multiple_handles(clear, user1, user2):
    
    resp_auth = requests.post(config.url + 'auth/register/v2', json={
        'email': "example3@example.com",
        'password': "password",
        'name_first': "Jack",
        'name_last': "Doe"
    })
    user3 = json.loads(resp_auth.text)

    resp_create = requests.post(config.url + 'dm/create/v1', json={
        'token': user1['token'],
        'u_ids': [user2["auth_user_id"], user3["auth_user_id"]]
    })
    payload = json.loads(resp_create.text)

    assert payload['dm_name'] == "jackdoe, janedoe, johndoe"


def test_http_dm_create_valid_user(clear, user1):

    assert requests.post(config.url + 'dm/create/v1', json={
        'token': user1['token'],
        'u_ids': [1]
    }).status_code == 400


def test_http_dm_create_owner_valid(clear, user1):
    token = token_encode({
        'auth_user_id': 1,
        'session_id': 1
    })
    assert requests.post(config.url + 'dm/create/v1', json={
        'token': token,
        'u_ids': [user1["auth_user_id"]]
    }).status_code == 403