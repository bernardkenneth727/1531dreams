import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def setup_routes(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    owner_user = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'member@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Member'
    })
    member_user = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'Johnwalker@gmail.com',
        'password': 'Capisback',
        'name_first': 'John',
        'name_last': 'Walker'
    })
    member_user2 = json.loads(resp.text)

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': owner_user["token"],
        'name': "My Channel",
        'is_public': True
    })
    channel0 = json.loads(resp.text)

    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': owner_user["token"],
        'u_ids' : [member_user["auth_user_id"]]
    })
    dm0 = json.loads(resp.text)

    resp = requests.post(config.url + 'message/send/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel0["channel_id"],
        'message' : "helloworld"
    })
    message0 = json.loads(resp.text)

    resp = requests.post(config.url + 'message/senddm/v1', json={
        'token' : member_user["token"],
        'dm_id' : dm0["dm_id"],
        'message' : "alldayyes"
    })
    message1 = json.loads(resp.text)

    return {
        'owner': owner_user,
        'member1': member_user,
        'member2' : member_user2,
        'channel': channel0,
        'message_ch': message0,
        'message_dm' : message1
    }
# 400 for input error 
# 403 for access error
# users_stats_v1 tests 
# Test 1 Check if the token is invalid
def test_users_stats_invalidToken_http(clear_users):
    assert requests.get(config.url + 'users/stats/v1', params={
        'token' : 123123
    }).status_code == 403

# Test 2 Check that the number of channels is updated
def test_users_stats_channels_http(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    owner_user = json.loads(resp.text)

    i = 0
    while i < 20:
        requests.post(config.url + 'channels/create/v2', json={
            'token': owner_user["token"],
            'name': f"Channel {i}",
            'is_public': True
        })
        i = i + 1
    
    requests.get(config.url + 'users/stats/v1', params={
        'token' : owner_user["token"]
    }).status_code == 200
   
# Test 3 Check that the number of dms is updated
def test_users_stats_dms_http(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    owner_user = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'member@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Member'
    })
    member_user = json.loads(resp.text)

    i = 0
    while i < 35:
        requests.post(config.url + 'dm/create/v1', json={
            'token': owner_user["token"],
            'u_ids' : [member_user["auth_user_id"]]
        })
        i = i + 1

    requests.get(config.url + 'users/stats/v1', params={
        'token' : owner_user["token"]
    }).status_code == 200
    
# Test 4 Check that the number of messages is updated
def test_users_stats_messages(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    owner_user = json.loads(resp.text)

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': owner_user["token"],
        'name': "My Channel",
        'is_public': True
    })
    channel0 = json.loads(resp.text)
    
    i = 0
    while i < 20: 
        requests.post(config.url + 'message/send/v2', json={
            'token' : owner_user["token"],
            'channel_id' : channel0["channel_id"],
            'message' : f"mrstealyourgirl {i}"
        })
        i = i + 1

    requests.get(config.url + 'users/stats/v1', params={
        'token' : owner_user["token"]
    }).status_code == 200
    
    