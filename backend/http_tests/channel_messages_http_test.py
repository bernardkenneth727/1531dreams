import pytest
import requests
import json
from src import config
from src.json_edit import access_json


@pytest.fixture
def clear():
    requests.delete(config.url + 'clear/v1')


@pytest.fixture
def user(clear):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': "example@example.com",
        'password': "password",
        'name_first': "John",
        'name_last': "Doe"
    })
    return json.loads(resp.text)


@pytest.fixture
def channel(user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': user["token"],
        'name': 'channel1',
        'is_public': True
    })
    return json.loads(resp.text)

def test_http_channel_messages_start_message(clear, user, channel):
    messages = access_json("messages")
    number_messages = len(messages)

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': user['token'],
        'channel_id': channel['channel_id'],
        'start': number_messages + 1
    })
    assert resp.status_code == 400


def test_http_channel_messages_return(clear, user, channel):

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': user['token'],
        'channel_id': channel['channel_id'],
        'start': 0
    })
    payload = json.loads(resp.text)

    assert type(payload) is dict


def test_http_channel_messages_auth_user_perms(clear, user):

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': user["token"],
        'name': 'channel1',
        'is_public': False
    })
    payload_channel = json.loads(resp.text)

    assert requests.get(config.url + 'channel/messages/v2', params={
        'token': 2,
        'channel_id': payload_channel['channel_id'],
        'start': 0
    }).status_code == 403


def test_http_channel_messages_channel_id(clear, user):

    assert requests.get(config.url + 'channel/messages/v2', params={
        'token': user['token'],
        'channel_id': 0,
        'start': 0
    }).status_code == 400


def test_http_channel_messages_empty_list(clear, user, channel):

    resp = requests.get(config.url + 'channel/messages/v2', params={
        'token': user['token'],
        'channel_id': channel['channel_id'],
        'start': 0
    })
    
    payload = json.loads(resp.text)
    assert payload == {
        "messages": [],
        "start": 0,
        "end": -1
    }
