import pytest
import requests 
import json
from src import config
# 400 for input error 
# 403 for access error 
@pytest.fixture
def clear_user():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def owner_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'david.so@gmail.com',
        'password': 'GeniusBrain',
        'name_first': 'David',
        'name_last': 'So'
    })
    return json.loads(resp.text)

@pytest.fixture
def channel_create(owner_user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token' : owner_user["token"],
        'name' : "moeydw",
        'is_public' : 'False'
    })

    return json.loads(resp.text)

@pytest.fixture
def channel_details(owner_user, channel_create):
    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    }) 
    return json.loads(resp.text)


############### LAWRENCE LEUNG ####################################
# TESTS FOR CHANNEL REMOVE OWNER
def test_http_channel_removeowner_v1_not_valid_token(clear_user, member_user1, channel_create):
    assert requests.post(config.url + 'channel/removeowner/v1', json={
        'token' : 232323223,
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    }).status_code == 403
def test_http_channel_removeowner_v1_not_an_owner(clear_user, owner_user, member_user1, channel_create):
    assert requests.post(config.url + 'channel/removeowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    }).status_code == 400

def test_http_channel_removeowner_v1_not_valid_channel_id(clear_user,  owner_user, member_user1, channel_create):
    
    requests.post(config.url + 'channel/addowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    }) 
    assert requests.post(config.url + 'channel/removeowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : 234234234,
        'u_id' : member_user1["auth_user_id"]
    }).status_code == 400
    
def test_http_channel_removeowner_v1_only_owner_in_channel(clear_user, owner_user, channel_create):
    assert requests.post(config.url + 'channel/removeowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : owner_user["auth_user_id"]
    }).status_code == 400
    
    
def test_http_channel_removeowner_v1_not_owner_of_dreams(clear_user, owner_user, member_user1, member_user2, channel_create):
    requests.post(config.url + 'channel/invite/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    }) 
    requests.post(config.url + 'channel/addowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user2["auth_user_id"]
    }) 
    
    assert requests.post(config.url + 'channel/removeowner/v1', json={
        'token' :  member_user1["auth_user_id"],
        'channel_id' : channel_create["channel_id"],
        'u_id' :  member_user2["auth_user_id"]
    }).status_code == 403


def test_http_channel_removeowner_v1_removing_an_owner(clear_user, owner_user, member_user1, member_user2, channel_create, channel_details):
    members_count = len(channel_details["owner_members"])
    assert members_count == 1
    resp = requests.post(config.url + 'channel/addowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    }) 
    resp = requests.post(config.url + 'channel/addowner/v1', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user2["auth_user_id"]
    }) 
    resp = requests.post(config.url + 'channel/removeowner/v1', json={
        'token' :  owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' :  member_user1["auth_user_id"]
    })
    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    })
    
    members_count = len(json.loads(resp.text)["owner_members"])
    print(json.loads(resp.text)["owner_members"])
    assert members_count == 2
    
