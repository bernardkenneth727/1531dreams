import pytest
import requests
import json
from src import config
import time

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser3():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello3@gmail.com',
        'password': '12345678!',
        'name_first': 'Jacky',
        'name_last': 'Harlow'
    })

    return json.loads(resp.text)

@pytest.fixture
def channelsCreate(registerUser):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': registerUser['token'], 
        'name': 'Teams1',
        'is_public': True   
    })

    return json.loads(resp.text)

@pytest.fixture
def channelJoin(registerUser2, channelsCreate):
    requests.post(config.url + 'channel/join/v2', json={
        'token': registerUser2['token'], 
        'channel_id': channelsCreate['channel_id'],
    })


@pytest.fixture
def standupStart(registerUser, channelsCreate):
    resp = requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    })

    return json.loads(resp.text)


@pytest.fixture
def standupActive(registerUser, channelsCreate):
    resp = requests.get(config.url + 'standup/active/v1', params={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id']
    })

    return json.loads(resp.text)



def test_http_standup_start_normal(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart, standupActive): 

    assert standupStart["time_finish"] == standupActive["time_finish"]

    time.sleep(2.1)

def test_http_standup_start_invalidToken(clear_users, registerUser, registerUser2, channelsCreate, channelJoin): 
    assert requests.post(config.url + 'standup/start/v1', json={
        'token': 'invalid',
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    }).status_code == 403

def test_http_standup_start_invalidChannel(clear_users, registerUser, registerUser2, channelsCreate, channelJoin): 
    assert requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser['token'],
        'channel_id': channelsCreate['channel_id'] + 111,
        'length': 2
    }).status_code == 400

def test_http_standup_start_activeStandupAlready(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart): 
    assert requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser['token'],
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    }).status_code == 400

    time.sleep(2.1)

def test_http_standup_start_UserNotInChannel(clear_users, registerUser, registerUser2, registerUser3, channelsCreate, channelJoin): 
    assert requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser3['token'],
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    }).status_code == 403