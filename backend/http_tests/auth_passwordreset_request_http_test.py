import pytest
import requests
import json
from src import config
from src.auth_code import reset

def test_http_auth_passwordreset_request_return():
    resp = requests.post(config.url + 'auth/passwordreset/request/v1', json = {
        'email': 'cactuscomp1531@gmail.com'
    })

    assert resp.status_code == 200

    assert json.loads(resp.text) == {}