import pytest
import requests 
import json
from src import config
# 400 for input error 
# 403 for access error
@pytest.fixture
def clear_user():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def owner_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def dup_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'he232323o@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'david.so@gmail.com',
        'password': 'GeniusBrain',
        'name_first': 'David',
        'name_last': 'So'
    })
    return json.loads(resp.text)

@pytest.fixture
def channel_create(owner_user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token' : owner_user["token"],
        'name' : "iAmironman",
        'is_public' : False
    })

    return json.loads(resp.text)


########################## LAWRENCE LEUNG ##############################
def test_http_channel_details_v2_invalid_channel_id(clear_user, owner_user):
    assert requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : 22323232
    }).status_code == 400

def test_http_channel_details_v2_not_member(clear_user, member_user1, channel_create):
    assert requests.get(config.url + 'channel/details/v2', params={
        'token' : member_user1["token"],
        'channel_id' : channel_create["channel_id"]
    }).status_code == 403

def test_http_channel_details_v2_duplicate_handle(clear_user, owner_user, dup_user, channel_create):
    resp = requests.post(config.url + 'channel/invite/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : dup_user["auth_user_id"]
    })

    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    })

    details = json.loads(resp.text)

    assert details["name"] == "iAmironman"
    assert details["is_public"] == "False"
    assert details["owner_members"][0]["handle_str"] == "helloworld" 
    assert details["all_members"][1]["handle_str"] == "helloworld0"


def test_http_channel_details(clear_user, owner_user, member_user1, channel_create):

    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    })

    details = json.loads(resp.text)

    assert details["name"] == "iAmironman"
    assert details["is_public"] == "False"
    assert details["owner_members"][0]["u_id"] == owner_user["auth_user_id"]
    assert details["all_members"][0]["u_id"] == owner_user["auth_user_id"]
    
    
    

