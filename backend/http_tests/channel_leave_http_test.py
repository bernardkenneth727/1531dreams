import pytest
import requests 
import json
from src import config
# 400 for input error 
# 403 for access error 
@pytest.fixture
def clear_user():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def owner_user():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Hello',
        'name_last': 'World'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user1():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'doctor@unsw.com',
        'password': 'D0ct0r',
        'name_first': 'Doctor',
        'name_last': 'UNSW'
    })
    return json.loads(resp.text)

@pytest.fixture 
def member_user2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'david.so@gmail.com',
        'password': 'GeniusBrain',
        'name_first': 'David',
        'name_last': 'So'
    })
    return json.loads(resp.text)

@pytest.fixture
def channel_create(owner_user):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token' : owner_user["token"],
        'name' : "iAmironman",
        'is_public' : False
    })

    return json.loads(resp.text)

@pytest.fixture
def channel_details(owner_user, channel_create):
    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    }) 
    return json.loads(resp.text)


def test_http_channel_leave_v1_not_valid_channel_id(clear_user, owner_user):
    assert requests.post(config.url + 'channel/leave/v1', json={
        'token' : owner_user["token"],
        'channel_id' : 2342342
    }).status_code == 400

def test_http_channel_leave_v1_not_valid_token(clear_user, owner_user, channel_create):
    assert requests.post(config.url + 'channel/leave/v1', json={
        'token' : 2323232323,
        'channel_id' : channel_create["channel_id"]
    }).status_code == 403

def test_http_channel_leave_v1_not_auth_user_of_channel(clear_user, member_user1, channel_create):
    assert requests.post(config.url + 'channel/leave/v1', json={
        'token' : member_user1["token"],
        'channel_id' : channel_create["channel_id"]
    }).status_code == 403

def test_http_channel_leave_v1_member_leaves_channel(clear_user, owner_user, member_user1, channel_create, channel_details):
    member_count = len(channel_details["all_members"])
    assert member_count == 1
    resp = requests.post(config.url + 'channel/invite/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"],
        'u_id' : member_user1["auth_user_id"]
    })
    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    })
    member_count = len(json.loads(resp.text)["all_members"])
    assert member_count == 2

    requests.post(config.url + 'channel/leave/v1', json={
        'token' : member_user1["token"],
        'channel_id' : channel_create["channel_id"]
    })
    resp = requests.get(config.url + 'channel/details/v2', params={
        'token' : owner_user["token"],
        'channel_id' : channel_create["channel_id"]
    })
    member_count = len(json.loads(resp.text)["all_members"])
    assert member_count == 1
