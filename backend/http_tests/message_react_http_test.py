import pytest
import requests
import json
from src import config

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture
def setup_routes(clear_users):
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Hello'
    })
    owner_user = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'member@gmail.com',
        'password': '123456',
        'name_first': 'John',
        'name_last': 'Member'
    })
    member_user = json.loads(resp.text)

    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'Johnwalker@gmail.com',
        'password': 'Capisback',
        'name_first': 'John',
        'name_last': 'Walker'
    })
    member_user2 = json.loads(resp.text)

    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': owner_user["token"],
        'name': "My Channel",
        'is_public': True
    })
    channel0 = json.loads(resp.text)

    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': owner_user["token"],
        'u_ids' : [member_user["auth_user_id"]]
    })
    dm0 = json.loads(resp.text)

    resp = requests.post(config.url + 'message/send/v2', json={
        'token' : owner_user["token"],
        'channel_id' : channel0["channel_id"],
        'message' : "helloworld"
    })
    message0 = json.loads(resp.text)

    resp = requests.post(config.url + 'message/senddm/v1', json={
        'token' : member_user["token"],
        'dm_id' : dm0["dm_id"],
        'message' : "alldayyes"
    })
    message1 = json.loads(resp.text)

    return {
        'owner': owner_user,
        'member1': member_user,
        'member2' : member_user2,
        'channel': channel0,
        'message_ch': message0,
        'message_dm' : message1
    }

@pytest.fixture
def setup_message_react(setup_routes):
    thumbs_up = 1
    requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : thumbs_up
    })

# 400 for input error 
# 403 for access error 
# Test1 message/react/v1 The person reacting has already reacted in the channel or dm
def test_message_react_v1_invalid_user_access_http(clear_users, setup_routes):
    thumbs_up = 1
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["member1"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : thumbs_up
    }).status_code == 403
    
    requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["member1"]["token"],
        'message_id' : setup_routes["message_dm"]["message_id"],
        'react_id' : thumbs_up
    })

    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["member2"]["token"],
        'message_id' : setup_routes["message_dm"]["message_id"],
        'react_id' : thumbs_up
    }).status_code == 403

# Test 2 The message_id is valid
def test_message_react_v1_invalid_message_id_http(clear_users, setup_routes):
    thumbs_up = 1
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : 23231421,
        'react_id' : thumbs_up
    }).status_code == 400

# Test 3 React_id is one   
def test_message_react_v1_invalid_react_id_http(clear_users, setup_routes):
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : 92
    }).status_code == 400

    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : -232
    }).status_code == 400

# Test 4 Reacting the same message raises input error 
def test_message_react_v1_contains_react_http(clear_users, setup_routes, setup_message_react):
    thumbs_up = 1
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : thumbs_up
    }).status_code == 400

# Test 5 Reacting to a dm message 
def test_message_react_v1_dm_http(clear_users, setup_routes):
    thumbs_up = 1
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_dm"]["message_id"],
        'react_id' : thumbs_up
    }).status_code == 200

# Test 6 Reacting to a channel message 
def test_message_react_v1_channel_http(clear_users, setup_routes):
    thumbs_up = 1
    assert requests.post(config.url + 'message/react/v1', json={
        'token' : setup_routes["owner"]["token"],
        'message_id' : setup_routes["message_ch"]["message_id"],
        'react_id' : thumbs_up
    }).status_code == 200

