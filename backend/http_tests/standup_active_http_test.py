import pytest
import requests
import json
from src import config
import time

@pytest.fixture
def clear_users():
    requests.delete(config.url + 'clear/v1')

@pytest.fixture 
def registerUser():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello@gmail.com',
        'password': '123456!',
        'name_first': 'Jack',
        'name_last': 'Smith'
    })

    return json.loads(resp.text)

@pytest.fixture 
def registerUser2():
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'hello2@gmail.com',
        'password': '1234567!',
        'name_first': 'John',
        'name_last': 'Swish'
    })

    return json.loads(resp.text)

@pytest.fixture
def channelsCreate(registerUser):
    resp = requests.post(config.url + 'channels/create/v2', json={
        'token': registerUser['token'], 
        'name': 'Teams1',
        'is_public': True   
    })

    return json.loads(resp.text)

@pytest.fixture
def channelJoin(registerUser2, channelsCreate):
    requests.post(config.url + 'channel/join/v2', json={
        'token': registerUser2['token'], 
        'channel_id': channelsCreate['channel_id'],
    })


@pytest.fixture
def standupStart(registerUser, channelsCreate):
    resp = requests.post(config.url + 'standup/start/v1', json={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id'],
        'length': 2
    })

    return json.loads(resp.text)

@pytest.fixture
def standupActive(registerUser, channelsCreate):
    resp = requests.get(config.url + 'standup/active/v1', params={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id']
    })

    return json.loads(resp.text)



def test_http_standup_active_normal(clear_users, registerUser, registerUser2, channelsCreate, channelJoin, standupStart, standupActive): 

    assert standupActive["time_finish"] == standupStart["time_finish"]
    assert standupActive["is_active"] == True 

    time.sleep(2.1)

    resp = requests.get(config.url + 'standup/active/v1', params={
        'token': registerUser['token'], 
        'channel_id': channelsCreate['channel_id']
    })

    standupActiveAfter = json.loads(resp.text)

    assert standupActiveAfter["is_active"] == False
    assert standupActiveAfter["time_finish"] == None

def test_http_standup_active_invalidToken(clear_users, registerUser, registerUser2, channelsCreate, channelJoin): 
    assert requests.get(config.url + 'standup/active/v1', params={
        'token': 'invalid',
        'channel_id': channelsCreate['channel_id']
    }).status_code == 403

def test_http_standup_active_invalidChannel(clear_users, registerUser, registerUser2, channelsCreate, channelJoin): 
    assert requests.get(config.url + 'standup/active/v1', params={
        'token': registerUser['token'],
        'channel_id': channelsCreate['channel_id'] + 111
    }).status_code == 400
