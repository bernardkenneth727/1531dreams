import pytest
import requests
import json
from src import config

@pytest.fixture
def intro_dm():
    requests.delete(config.url + 'clear/v1')
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'johnsmith@gmail.com', 
        'password': '123456', 
        'name_first': 'John', 
        'name_last': 'Smith'
    })
    result1 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'michaelscott@dundermifflin.com', 
        'password': 'PASSWORD',
        'name_first': 'Michael',
        'name_last': 'Scott'
    })
    result2 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'leslieknope@pawnee.gov', 
        'password': 'JoeBiden1',
        'name_first': 'Leslie',
        'name_last': 'Knope'
    })
    result3 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'jake.peralta@nypdbrooklyn.gov', 
        'password': 'nine-nine',
        'name_first': 'Jake',
        'name_last': 'Peralta'
    })
    result4 = json.loads(resp.text)
    resp = requests.post(config.url + 'auth/register/v2', json={
        'email': 'eleanorshellstrop@yahoo.com', 
        'password': 'TheBadPlace',
        'name_first': 'Eleanor',
        'name_last': 'Shellstrop'
    })
    result5 = json.loads(resp.text)
    
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': result1["token"],
        'u_ids': [result2["auth_user_id"], result3["auth_user_id"]]
    })
    new_dm = json.loads(resp.text)

    return {
        "john": result1,
        "michael": result2,
        "leslie": result3,
        "jake": result4,
        "eleanor": result5,
        "dm": new_dm
    }

# Testing dm/list/v1.
# Testing a normal valid circumstance with a singular dm.
def test_dm_list_valid_singular(intro_dm):
    resp = requests.get(config.url + 'dm/list/v1', params={
        'token': intro_dm["john"]["token"]
    })
    assert resp.status_code == 200

    result = json.loads(resp.text)
    assert result == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }
        ]
    }

# Testing a normal valid circumstance with multiple dms.
def test_dm_list_valid_multiple(intro_dm):
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': intro_dm["john"]["token"],
        'u_ids': []
    })
    result1 = json.loads(resp.text)
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': intro_dm["john"]["token"],
        'u_ids': []
    })
    result2 = json.loads(resp.text)
    resp = requests.post(config.url + 'dm/create/v1', json={
        'token': intro_dm["john"]["token"],
        'u_ids': []
    })
    result3 = json.loads(resp.text)

    resp = requests.get(config.url + 'dm/list/v1', params={
        'token': intro_dm["john"]["token"]
    })
    result = json.loads(resp.text)

    assert result == {
        "dms": [
            {
                "dm_id": intro_dm["dm"]["dm_id"],
                "name": intro_dm["dm"]["dm_name"]
            }, {
                "dm_id": result1["dm_id"],
                "name": "johnsmith"
            }, {
                "dm_id": result2["dm_id"],
                "name": "johnsmith"
            }, {
                "dm_id": result3["dm_id"],
                "name": "johnsmith"
            }
        ]
    }

# Testing an invalid circumstance with the token not being valid.
def test_dm_list_invalid_token(intro_dm):
    assert requests.get(config.url + 'dm/list/v1', params={
        'token': '123.123.123'
    }).status_code == 403